# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#
# Provide a set of function to work around path.
#

# return the absolute path of $1
#
# Ex: _zb_get_absolute_path ../../a/b /a -> error.
#
# If path does not begin by /, then cwd (which default to $PWD) is prepend making the path absolute. Else, the function
# will remove any '.' or empty entries (like /c/./ or /c//b), and fold any '..' (like /a/../b which is /b).
#
# This works by splitting the path using IFS=/. Empty components and . are first removed, then we use a stack to
# remove '..'.
#
# @param path
# @param cwd current directory, default to $PWD. Must be absolute.
# @return return an absolute path.
_zb_get_absolute_path() { # {{{
  local path="$1"

  # the path begins by /, so we ignore CWD.
  if [[ -z "${path}" || "${path:0:1}" != '/' ]]; then
    path="${2:-$PWD}/${path}"
  fi


  local IFS='/'
  # shellcheck disable=SC2206 # this is wanted
  local pc=( ${path} )
  unset IFS
  local val

  local -r n=${#pc[@]}
  # first pass, remove '.' and empty path (they occur like that: /a//b
  # before /a there is an empty path
  # before /b too.
  local pc2=()
  local p=0
  for (( i = 0 ; i < n ; i++ )); do
    val=${pc[$i]}
    if [[ -n "$val" && '.' != "$val" ]]; then
      pc2[p]="${pc[$i]}"
      (( ++p ))
    fi
  done
  # second pass, remove '..'
  # we use a stack, the index being the marker to the top
  local stack=()

  local stack_idx=0
  for (( i = 0 ; i < p ; i++ )); do
    val=${pc2[$i]}
    if [[ "$val" != '..' ]]; then
      #push
      stack[stack_idx]=$val
      (( ++stack_idx ))
    elif [[ $stack_idx -gt 0 ]]; then
      # we remove a value from stack
      # pop
      (( --stack_idx ))
    else
      # this is an error, but we return '/'. In fact, if you have path (CWD) :
      # /e/git/zeta-bash and you do 'cd ../../../../../../', then bash
      # will go to /.
      echo -n '/'
      return 1
    fi
  done

  # for debug purpose, display the stack
  # local -r x=${#stack[@]}
  # for (( i = 0 ; i < $x ; i++ )); do
  #   echo -n "stack[$i]: ${stack[$i]}"
  #   if [[ $i -eq $stack_idx ]]; then
  #     echo -e ' \e[1;31m*\e[0m'
  #   else
  #     echo ''
  #   fi
  # done

  if [[ $stack_idx -eq 0 ]]; then
    echo -n '/'
  else
    val=''
    for (( i = 0 ; i < stack_idx ; i++ )); do
      val+="/${stack[$i]}"
    done
    echo -n "$val"
  fi
  return 0
} # }}} [_zb_get_absolute_path]

#
# Rewrite a path into a Windows compliant path.
#
# If the path matches '/?/', return a "C:\" style path (pretty much like pwd -W), otherwise simply replace
# slashes by backslashes (that's the official filename separator on Windows, and some programs does not
# understand the slashes path).
#
# The path will be transformed into an absolute one using _zb_get_absolute_path first.
#
# @param path a path, required
# @return returns an absolute windows path.
#
_zb_win_path0() {
  local path=${1}
  if [[ -z "$path" ]]; then
    return 1
  fi
  path=$(_zb_get_absolute_path "$path")
  local win_path=''
  if [[ "${path:0:1}" == '/' && "${path:2:1}" == '/' ]]; then
    local -u drive="${path:1:1}"
    local remaining="${path:3}"
    win_path="${drive}:\\${remaining//\//\\}"
  else
    win_path="${path//\//\\}"
  fi
  if [[ -t 1 ]]; then
    echo "$win_path"
  else
    echo -n "$win_path"
  fi
}

#
# Rewrite a path into a Windows compliant path.
#
# If the path matches '/?/', return a "C:\" style path (pretty much like pwd -W), otherwise simply replace
# slashes by backslashes (that's the official filename separator on Windows, and some programs does not
# understand the slashes path).
#
# The path will always be an absolute one.
#
# @param paths... if empty, use $PWD.
#
_zb_win_path() {
  if [[ $# -eq 0 ]]; then
    _zb_win_path0 "$PWD"
  else
    for path; do
      _zb_win_path0 "${path}"
    done
  fi
}

_zb_path_handler_init() {
  local tool=""
  if tool="$(command -v cygpath)" ; then
    export ZB_PATH_HANDLER_MODE="cygpath"
    export ZB_PATH_HANDLER_EXEC="$tool"
    export ZB_PATH_HANDLER_RUNONCE="yes"
  elif tool="$(command -v wslpath)" ; then
    export ZB_PATH_HANDLER_MODE="wslpath"
    export ZB_PATH_HANDLER_EXEC="$tool"
    export ZB_PATH_HANDLER_RUNONCE="no"
  else
    return 1
  fi
}

#
# Rewrite a path using cygpath or wslpath
#
# Note: wslpath don't accept multiple path.
#
# @param path...
# @return 0 if all ok;
#
_zb_win_path2() {
  if [[ -z "${ZB_PATH_HANDLER_MODE}" && -z "${ZB_PATH_HANDLER_EXEC}" ]] && ! _zb_path_handler_init ; then
    eecho "could not determine path tool to be used."
    return 1
  fi
  # for now, both cygpath and realpath use the same -w/-m options:
  if [[ "$#" == 0 ]]; then
    "${ZB_PATH_HANDLER_EXEC}" -w "$PWD"
  elif [[ "$ZB_PATH_HANDLER_RUNONCE" == yes ]]; then
    "${ZB_PATH_HANDLER_EXEC}" -w "$@"
  else
    for path; do
      "${ZB_PATH_HANDLER_EXEC}" -w "$path" || return 1
    done
  fi
}

#
# Prefix a set of path from windows by '/mnt' if WSL.
#
# @param name where to store entries
# @param path... absolute path entries
# @output ZB_PATH_WSL_DIRECTORIES
#
_zb_wsl_prefix() {
  local __var_name="$1"
  if [[ -z "$1" ]]; then
    return 1
  fi
  shift
  if [[ -n "$WSL_DISTRO_NAME" ]]; then
    eval "${__var_name}=(); for arg; do ${__var_name}+=( \"/mnt\${arg}\" ) ; done"
  else
    eval "${__var_name}=(); for arg; do ${__var_name}+=( \"\${arg}\" ) ; done"
  fi
}
