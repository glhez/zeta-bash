# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# This contains utility function to temper the PATH variable.
#

#
# Initialize the PATH variable.
#
# initial stack is empty
declare ZETA_BASH_PATH_ORIG=

#
# Save current PATH (protection against empty stack)
#
_zb_path_backup() {
  if [[ -z "$ZETA_BASH_PATH_ORIG" ]]; then
    ZETA_BASH_PATH_ORIG="${PATH}"
  fi
}

_zb_path_list_var_source() { # {{{ [_zb_path_list_var_source]
  local file="$1"
  [[ !  -f "$file" ]] && return 0
  local line=''
  while read -r line; do
    line="${line%%'#'*}"
    # rtrim: https://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
    line="${line%"${line##*[![:space:]]}"}"
    [[ -z "$line" ]] && continue

    local negate=no
    if [[ "${line:0:1}" == '!' ]]; then
      negate=yes
      line="${line:1}"
    fi

    local os=''
    if [[ "$line" == *:* ]]; then
      os="${line%%:*}"
      line="${line#*:}"
    fi

    local prepend=no
    if [[ "${line:0:1}" == '+' ]]; then
      prepend=yes
      line="${line:1}"
    fi
    local name="${line%%=*}"
    local value="${line#*=}"

    #echo "::: $line"
    #declare -p negate os prepend name value
    if [[ -z "$os" || "$negate" == yes && "$os" != "${ZETA_BASH_OS}" || "${os}" == "${ZETA_BASH_OS}" ]]; then
      if [[ "$prepend" == yes ]]; then
        pre_entries_names+=( "$name" )
        pre_entries_values+=( "$value" )
      else
        post_entries_names+=( "$name" )
        post_entries_values+=( "$value" )
      fi
    fi
  done < "$file"
} # }}}

_zb_path_ref() {
  local -n target="$1"
  local name="$2"
  local value="$3"
  local v="${!name:-}"
  # no value for env => ignored it
  [[ -z "$v" ]] && return 1
  # specific to windows, we translate variables once in case var is
  if [[ "${ZETA_BASH_OS}" == windows && "$v" =~ ^[a-zA-Z]: ]]; then
    local cgw=''
    if [[ "${env_cache_set["$name"]:-no}" == no ]]; then
      if cgw=$(cygpath -u "$v") && [[ -n "$cgw" ]]; then
        v="$cgw"
      fi
      env_cache_set["$name"]="yes"
      env_cache_values["$name"]="$v"
    else
      v="${env_cache_values["$name"]}"
    fi
  fi
  [[ "$value" != '/' ]] && v+="$value"

  #shellcheck disable=SC2034
  target="$v"
  return 0
}

_zb_path_append() {
    local -n __entries="$1"
  local -n __entries_hash="$2"
  local    value="$3"
  local    clean="$4"
  local    remove_duplicates="$5"

  # value can be empty (the current dir)
  if [[ "${clean}" == yes && -n "$value" && ! -d "$value" ]]; then
    return 0
fi
  if [[ "${remove_duplicates}" == yes ]]; then
    # we use x before for empty path
    [[ "${__entries_hash["x${value}"]:-no}" == yes ]] && return 0
    __entries_hash["x${value}"]=yes
  fi
  __entries+=( "$value" )
}

_zb_path_export() {
  local -n __new_entries="$1"
  local new_path=""
  for (( i = 0, n = ${#__new_entries[@]}; i < n; ++i )) ; do
    [[ "$i" != 0 ]] && new_path+=":"
    new_path+="${__new_entries[$i]}"
  done
  export PATH="$new_path"
}

#
# Initialize the enviroment.
#
# @param --clean: remove directory entry that are invalid
# @param --remove-duplicates remove duplicates entries
#
_zb_path_init() { # {{{
  local clean=no
  local remove_duplicates=no

  #
  #
  #
  for arg; do
    case "$arg" in
      --clean)             clean=yes ;;
      --remove-duplicates) remove_duplicates=yes ;;
    esac
  done

  local -a pre_entries_names=()
  local -a pre_entries_values=()
  local -a post_entries_names=()
  local -a post_entries_values=()

  _zb_path_list_var_source "$ZETA_BASH_HOME/lib/path.list"
  _zb_path_list_var_source "$ZETA_BASH_USER_HOME/lib/path.list"

  local -A env_cache_values=()
  local -A env_cache_set=()

  local -a new_entries=()
  # shellcheck disable=SC2034
  local -A new_entries_hash=()

  for (( i = 0, n = ${#pre_entries_names[@]}; i < n; ++i )); do
    local name="${pre_entries_names[i]}"
    local value="${pre_entries_values[i]}"
    path_value=''
    _zb_path_ref path_value "${name}" "${value}" && _zb_path_append new_entries new_entries_hash "$path_value" "${clean}" "${remove_duplicates}"
  done

  # somehow, mapfile or <<< will add a new line delimiter at the end. IFS + read won't.
  #mapfile -d ':' -t entries <<<"$PATH"
  IFS=: read -r -a entries <<<"$PATH"
  for entry in "${entries[@]}"; do
    _zb_path_append new_entries new_entries_hash "$entry" "${clean}" "${remove_duplicates}"
  done

  for (( i = 0, n = ${#post_entries_names[@]}; i < n; ++i )); do
    local name="${post_entries_names[i]}"
    local value="${post_entries_values[i]}"
    path_value=''
    _zb_path_ref path_value "${name}" "${value}" && _zb_path_append new_entries new_entries_hash "$path_value" "${clean}" "${remove_duplicates}"
  done

  _zb_path_export new_entries
  return 0
} # }}} [_zb_path_init]

#
# pretty print the current PATH
#
_zb_path_print() { # {{{
  #
  # we do some work before, like cleaning duty (we want to display them if found)
  #
  local env_list_names=()  # name of variables found
  local env_list_values=() # value of variables found
  local env_list_inpath=() # number of times found in path (exact matchs) ?
  local env_list_index=0

  local name value
  while read -r name; do
    # shellcheck disable=SC2155
    local value="${!name:-}"
    [[ -z "$value" ]] && continue

    # under windows, normalize the path entry to bash
    if [[ "$ZETA_BASH_OS" == windows && "$value" =~ ^[[:alpha:]]:[\\/] ]]; then
      local letter=${value:0:1}
      local path=${value:3}
      value="/${letter,,}/${path//\\//}"
    fi

     env_list_names[env_list_index]=$name
    env_list_values[env_list_index]="$value"
    env_list_inpath[env_list_index]=0

    (( ++env_list_index ))
  done < <(compgen -e)

  local i j
  local env_list_length=${env_list_index}

  local -a path_entries=()
  IFS=':' read -r -a path_entries <<< "$PATH"

  for (( i = 0; i < ${#path_entries[@]}; ++i )); do
    local entry="${path_entries[$i]}"
    local index=-1 #best index
    for (( j = 0 ; j < env_list_length ; j++ )); do
      local val=${env_list_values[$j]}
      local len=${#val}
      local t=${entry:0:$len}

      # on windows, do insensitive search:
      if [[ "$ZETA_BASH_OS" == windows ]]; then
        t=${t,,}
        val=${val,,}
      fi

      if [[ "$t" == "$val" ]]; then
        if [[ $index -eq -1 ]]; then
          index=$j
        elif [[ $len -gt ${#env_list_values[$index]} ]]; then
          index=$j
        fi
      fi
    done

    [[ $index -ne -1 ]] && env_list_inpath[index]=$(( ${env_list_inpath[$index]} + 1 ))

    printf "%03d: " "${i}"

    if [[ -n "${entry}" && ! -d "${entry}" ]]; then
      echo -en "${ZETA_BASH_COLOR_RED}"
      echo -n "(missing) "
    fi
    if [[ $index -ne -1 ]]; then
      local len=${#env_list_values[$index]}
      local suffix=${entry:$len}
      echo -en "${ZETA_BASH_COLOR_BLUE}"
      echo -n  "\${${env_list_names[$index]}}"
      echo -en "${ZETA_BASH_COLOR_NONE}"
      echo -n "$suffix"
    else
      echo -n "$entry"
    fi
    echo -e "${ZETA_BASH_COLOR_NONE}"
  done

  for (( j = 0, i = 0 ; j < env_list_length ; j++ )); do
    if [[ ${env_list_inpath[$j]} -gt 0 ]]; then
      echo -en "${ZETA_BASH_COLOR_ORANGE}E"
      printf "%02d: " "${i}"
      echo -en "${ZETA_BASH_COLOR_NONE}${ZETA_BASH_COLOR_BLUE}"
      echo -n "${env_list_names[$j]}"
      echo -en "${ZETA_BASH_COLOR_NONE}="
      echo -n "${env_list_values[$j]}"
      [[ ! -d "${env_list_values[$j]}" ]] && echo -en "${ZETA_BASH_COLOR_RED} (missing) "
      echo -e "${ZETA_BASH_COLOR_NONE}"

      (( ++i ))
    fi
  done
} # }}} [_zb_path_print]

#
# Restore the path to some previous value.
#
_zb_path_restore() { # {{{
  export PATH="${ZETA_BASH_PATH_ORIG}"
} # }}} [_zb_path_restore]

#
# Position path for an application, such as mvn, java or node
#
# @param --app-name=path         name of applicaton, if quiet = no
# @param --quiet=yes|no          print stuff if quiet = no
# @param --app-home=path         selected home
# @param --relative-app-bin=path directory in which executable is found in app_home
# @param --executable=filename   executable to be tested against PATH using command -v
# @param --directories=function  a function producing path_entries, to remove known directories from path (cleanup)
#
_zb_path_replace_app_home() {
  local options_app_name=''
  local options_app_home=''
  local options_quiet=no
  local options_rel_app_bin_windows='/bin'
  local options_rel_app_bin_linux='/bin'
  local options_executable=''
  local options_resolve_symlink=no
  local options_directories=''

  for arg; do
    case "$arg" in
      --app-name=*)         options_app_name="${arg##--app-name=}"            ;;
      --app-home=*)         options_app_home="${arg##--app-home=}"            ;;
      --quiet=*)            options_quiet="${arg##--quiet=}"                  ;;
      --relative-app-bin=*)
        options_rel_app_bin_windows="${arg##--relative-app-bin=}"
        options_rel_app_bin_linux="${options_rel_app_bin_windows}"
      ;;
      --relative-app-bin-windows=*) options_rel_app_bin_windows="${arg##--relative-app-bin-windows=}" ;;
      --relative-app-bin-linux=*)   options_rel_app_bin_linux="${arg##--relative-app-bin-linux=}" ;;
      --executable=*)       options_executable="${arg##--executable=}"        ;;
      --directories=*)      options_directories="${arg##--directories=}"      ;;

      --no-quiet|--verbose) options_quiet=no            ;;
      --quiet)              options_quiet=yes           ;;
      --no-resolve-symlink) options_resolve_symlink=yes ;;
      --resolve-symlink)    options_resolve_symlink=yes ;;
      *)
        eecho "unsupported option $arg"
        return 1
      ;;
    esac
  done

  if [[ "${ZETA_BASH_OS:-linux}" == linux ]]; then
    options_rel_app_bin="${options_rel_app_bin_linux}"
  else
    options_rel_app_bin="${options_rel_app_bin_windows}"
  fi

  if [[ -z "${options_app_name}"   ]] ; then eecho "missing --app-name"   ; return 1 ; fi
  if [[ -z "${options_app_home}"   ]] ; then eecho "missing --app-home"   ; return 1 ; fi
  if [[ -z "${options_executable}" ]] ; then eecho "missing --executable" ; return 1 ; fi
  [[ -z "${options_rel_app_bin}"   ]] && options_rel_app_bin='/'

  options_quiet="${options_quiet,,}"
  options_resolve_symlink="${options_resolve_symlink,,}"

  [[ "$options_quiet"           != no && "$options_quiet"           != yes ]] && options_quiet=no
  [[ "$options_resolve_symlink" != no && "$options_resolve_symlink" != yes ]] && options_resolve_symlink=no

  if [[ "$options_resolve_symlink" == yes && -L "${options_app_home}" ]]; then
    local options_app_home_re=''
    if options_app_home_re=$(realpath "$options_app_home") ; then
      options_app_home="${options_app_home_re}"
    else
      wecho "could not resolve $options_app_home link"
    fi
  fi

  local app_bin="${options_app_home}${options_rel_app_bin}"
  if [[ ! -d "$app_bin" ]] ; then
    eecho "${ZETA_BASH_COLOR_PURPLE}${app_bin}${ZETA_BASH_COLOR_NONE} was not found."
    return 1
  fi

  local known_directory=''
  local known_directories=()
  options_directories=''
  if [[ -n "${options_directories}" ]]; then
    _zb_bt_resolve_find --provider "${options_directories}"
    _zb_bt_expand_sort --path

    for known_directory in "${ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY[@]}"; do
      #
      # we remove any entries that are our own, the rest are kept (we can't really know)
      #
      [[ -z "${known_directory}" ]] && continue
      case "${known_directory,,}" in
        # linux
        /usr/local/sbin) : ;;
        /usr/local/bin)  : ;;
        /usr/sbin)       : ;;
        /usr/bin)        : ;;
        /sbin)           : ;;
        /bin)            : ;;
        # windows
        /c/windows/system32) : ;;
        /c/windows)          : ;;
        # gfw
        /mingw64/bin)        : ;;
        # user
        "${HOME,,}/bin")        : ;;
        "${USERPROFILE,,}/bin") : ;;
        *) known_directories+=( "${known_directory}" ) ;;
      esac
    done
    _zb_bt_resolve_destroy
  fi

  [[ "$options_quiet" == no ]] && iecho "prepending ${options_app_name} ${ZETA_BASH_COLOR_PURPLE}${app_bin}${ZETA_BASH_COLOR_NONE} to PATH."
  local new_path="${app_bin}"

  # shellcheck disable=SC2034
  local -a new_entries=( "${app_bin}" )
  # shellcheck disable=SC2034
  local -A new_entries_hash=()

  # mapfile + <<< will have a new line at end of entries
  IFS=: read -r -a entries <<<"$PATH"

  for entry in "${entries[@]}"; do
    # clean right most '//' (// is valid in Windows UNC path)
    local is_unc="${entry:0:2}"
    local remaining="${entry:2}"
    while [[ "$remaining" == *//* ]]; do remaining="${remaining//'//'/'/'}" ; done
    entry="${is_unc}${remaining}"

    if [[ ! -x "$entry/${options_executable}" ]]; then
      _zb_path_append new_entries new_entries_hash "$entry" no yes
    else
      local found=no
      #decho "tring to clean $entry from known"
      for known_directory in "${known_directories[@]}"; do
        local filename="${known_directory##*/}"
        local dirname="${known_directory%/*}"
        local ver_before="${filename%%'@VER'*}"
        local ver_after="${filename##*'@VER'}"
        #decho "match? dirname: ${dirname} before: ${ver_before} after: ${ver_after} relbin: ${options_rel_app_bin}"
        if [[ "$entry" == "${dirname}/${ver_before}"*"${ver_after}${options_rel_app_bin}" ]]; then
          found=yes
          break
        fi
      done
      if [[ "$found" == no ]] ; then
        _zb_path_append new_entries new_entries_hash "$entry" no yes
      else
        [[ "$options_quiet" == no ]] && iecho "removing known entry: ${entry}"
      fi
    fi
  done
  _zb_path_export new_entries
}
