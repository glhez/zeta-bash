# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# @param command bash completion $1
# @param word    bash completion $2
# @param pword   bash completion $3
_zb_gitex_completion_gitex_main() { # {{{
  local cmd="$1"
  local cur=''
  local prev=''
  local -a words=()
  local cword=''

  _get_comp_words_by_ref -n '=' cur prev words cword


  #
  # handle completion of _zb_gitex_ function as well
  # in this case, the --refresh is disabled.
  #
  local action=''
  local -A global_options=()

  # --no-colors --no-color --colors --color

  if [[ "$cmd" == _zb_gitex_* ]]; then
    action="$cmd"
  elif [[ "$cmd" == gitex-gc ]];  then
    action="gitex-gc"
  else
    global_options[--refresh]=no
    global_options[--no-colors]=no
    global_options[--no-color]=no
    global_options[--colors]=no
    global_options[--color]=no

    # first, flag global options as present or not
    for (( i = 1, n = ${#words[@]}; i < n; ++i )); do
      local word="${words[$i]}"
      if [[ -n "$word" && -n "${global_options[$word]}" ]]; then
        global_options[$word]=yes
      fi
    done

    if [[ "$cmd" == bun ]]; then
      action=bundle
    else
      # then, determine the action
      for (( i = 1, n = ${#words[@]}; i < n; ++i )); do
        local word="${words[$i]}"
        if [[ -z "$word" || -z "${global_options[$word]}" ]]; then
          case "$word" in
            synchronize|sync|fetch|--synchronize|--sync|--fetch) action=fetch ;;
            goto|--goto)                           action=goto        ;;
            status|--status)                       action=status      ;;
            rebase|--rebase)                       action=rebase      ;;
            list|--list)                           action=list        ;;
            print|--print)                         action=print       ;;
            bundle|bun|--bun|--import)             action=bundle      ;;
            *)                                     action=default     ;;
          esac
          break
        fi
      done
    fi
  fi

  # we always help!
  local -a completions=( '-h' '--help' )
  if [[ "$action" == default ]]; then
    completions+=( synchronize sync fetch --synchronize --sync --fetch )
    completions+=( goto --goto )
    completions+=( status --status )
    completions+=( rebase --rebase )
    completions+=( list --list )
    completions+=( print --print )
    completions+=( bundle bun --bundle --bun --import )
  elif [[ "$action" == gitex-gc ]]; then
    completions+=(  --no-reflog --no-gc --reflog --gc --empty )
  fi

  for option in "${!global_options[@]}"; do
    if [[ "${global_options[$option]}" == no ]]; then
      completions+=( "${option}" )
    fi
  done

  # configure what's next
  local complete_remote=no
  local restrict_bare=no
  case "$action" in
    fetch)
      complete_remote=yes
      completions+=( --remote -r '--remote=' --prune -p --prune-tags -t  )
      completions+=( --mark-offline --mark-offline-by '--mark-offline-by=' -m )
      completions+=( --ignore-offline -i )
      completions+=( --no-mark-offline -N )
      completions+=( --status --no-status -s -S )
      # inherit from status
      completions+=( -A --show-all --remote-status -R )
    ;;
    goto)
      :
    ;;
    status)
      restrict_bare=yes
      completions+=( -A --show-all --remote-status -R --truncate-long-name -tln )
      for suggestion in automatic disabled 15 30 45 60 ; do
        completions+=( "--truncate-long-name=${suggestion}" )
      done
    ;;
    rebase)
      restrict_bare=yes
      complete_remote=yes
      completions+=( --remote -r '--remote=' --pull --no-confirm --stash --no-stash )
      completions+=( --status --no-status -s -S )
      # inherit from status
      completions+=( -A --show-all --remote-status -R )
    ;;
    list)
      completions+=( --live --cached )
    ;;
    print)
      completions+=( --git-dir )
    ;;
    bundle)
      completions+=( --import '--history=' --history '-h' '--full' )
      completions+=( '-T' '--target-host-name=' '--target-host-name' )
      completions+=( '--source-host-name=' '--source-host-name' )
      completions+=( '--ignore-mode' '--no-ignore-mode' )
    ;;
  esac

  # add the repo selector in the list
  completions+=( --offline --online --all )
  completions+=( --importable --exportable )
  if [[ "${restrict_bare}" != yes ]]; then
    completions+=( --bare --no-bare --non-bare --any )
  fi

  #
  # this will limit repo starting with '-', but it is most likely there will
  # be close to none
  #

  local config_loaded=no

  # shellcheck disable=SC1090,SC2154
  if [[ ( "$cur" != -? && "$cur" != --* || "$cur" == '-@' || "$complete_remote" == yes ) && -f "${ZETA_BASH_GITEX_CONF}" ]]; then
    # may fail, due to shell error, etc... can't do much.
    . "${ZETA_BASH_GITEX_CONF}"
    config_loaded=yes
  fi

  # consider existing repo
  local -A already_completed_words=()
  # shellcheck disable=SC2154
  for (( i = 1, n = ${#words[@]}; i < n; ++i )); do
    local word="${words[$i]}"
    if [[ -n "$word" ]]; then already_completed_words[$word]=yes ; fi
  done

  # rewrite completions
  local -a possible_completions=()
  for completion in "${completions[@]}" ; do
    if [[ -z "${already_completed_words[$completion]}" ]]; then
      possible_completions+=( "$completion" )
    fi
  done

  # shellcheck disable=SC2154
  if [[ "${config_loaded}" == yes ]]; then
    for group in "${!gitex_group_indexes[@]}"; do
      local w="@${group}"
      if [[ -z "$w" ]]; then continue ; fi
      if [[ -z "${already_completed_words[$w]}"  ]]; then possible_completions+=(  "$w" ) ; fi
      if [[ -z "${already_completed_words[-$w]}" ]]; then possible_completions+=( "-$w" ) ; fi
      # for group like ext/, add a *
      if [[ "${group}" == */* ]]; then
        w="${w%%/*}/*"
        if [[ -z "${already_completed_words[$w]}"  ]]; then possible_completions+=(  "$w" ) ; fi
        if [[ -z "${already_completed_words[-$w]}" ]]; then possible_completions+=( "-$w" ) ; fi
      fi
    done
    for (( index = 0, n = ${#gitex_repo_names[@]}; index < n; ++index )); do
      local is_bare="${gitex_repo_is_bare[$index]}"
      if [[ "$is_bare" == no || "${restrict_bare}" == no ]]; then
        local w="${gitex_repo_names[$index]}"
        if [[ -n "$w" && -z "${already_completed_words[$w]}"  ]]; then possible_completions+=(  "$w" ) ; fi
        if [[ -n "$w" && -z "${already_completed_words[-$w]}" ]]; then possible_completions+=( "-$w" ) ; fi
      fi
    done
  fi

  _zb_complete_match_prefix "$cur" "${possible_completions[@]}"

  # let PWD and such be completed
  if [[ "$action" == gitex-gc ]]; then
    local -a dirs=()
    if [[ "$cur" != '--' ]] && mapfile -t dirs < <(compgen -o bashdefault -o dirnames "$cur") ; then
      for probable_git_dir in "${dirs[@]}"; do
        if git -C "${probable_git_dir}" rev-parse --is-bare-repository 1> /dev/null 2>&1 ; then
          COMPREPLY+=( "$(printf "%q" "${probable_git_dir}" )" )
        fi
      done
    fi
  fi

} # }}} [_zb_gitex_completion_gitex_main]

# don't add filenames: this make names with '/' fails
# https://stackoverflow.com/questions/55476845/escape-slashes-in-bash-complete
complete -o bashdefault -o nospace -F _zb_gitex_completion_gitex_main \
  gitex-main            \
  g                     \
  bun                   \
  gitex-gc
complete -o bashdefault -o default -o dirnames -o filenames -W '-f'                    gitex-clean



