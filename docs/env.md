# Environment Switching made easy

## Description

**Zeta Bash** use a local git repository serving the purpose of storing any file, like settings for
various environment. It is also able to generate on the fly the `PATH` environment variable based
other variable such as `JAVA_HOME`, `M2_HOME`.

You can easily switch from one environment to another: you can use some JDK in one, use another maven
implementation or you might simply want to share your settings across several PC, some at work, some at
home, some on Windows, other on Linux, etc.

## Usage

The `init.bash` file contained in **Zeta Bash** will load the environment specified by `ZETA_BASH_ENV` (if empty, `default`
is used) and by the current OS (today, that's only `windows` or `linux`).

The user environment file is simply a bash file named `${ZETA_BASH_OS}.${ZETA_BASH_ENV}`, stored in `${ZETA_BASH_USER_HOME}/env`
and containing such lines: on Windows, the default filename would be `windows.default.bash`, on Linux it would be `linux.default.bash`.

```bash
export LANG=en_US.utf8
export APPS_ROOT="/e/apps"
export ZETA_BASH_PROMPT_TOKENS='time pwd git java zb_env'

export ZETA_BASH_GITEX_REPOPATH="/e/git=2"
export ZETA_BASH_GITEX_BARE_REPOPATH='/i/personnel/git-bare=2'

export ZETA_BASH_JAVA_CONTAINER_PATH="${APPS_HOME}/java"
export JAVA_DEFAULT=17

export ZETA_BASH_NODEJS_CONTAINER_PATH="${APPS_HOME}/nodejs"*
export NODE_DEFAULT=18

export ZETA_BASH_MAVEN_CONTAINER_PATH="${APPS_HOME}/apache-maven"
export MAVEN_DEFAULT=3.9.4
```

After this file is sourced during initialization or by `env-reload`:

- during initizaliation, a `${ZETA_BASH_USER_HOME}/.cache/.session` is sourced is available: it contains (some) variables
  from previous session to avoid checking java/maven/node current version (because this produce slow shell startup on Windows)
- if `JAVA_DEFAULT`, `NODE_DEFAULT` or `MAVEN_DEFAULT` is changed, the appropriate JAVA_HOME, NODEJS_HOME and M2_HOME are
  updated.
- if `ZETA_BASH_PROMPT_TOKENS` is changed, the default prompt (`PS1`) is updated.
- the PATH is rebuilt using specification found in path.list `${ZETA_BASH_HOME}/lib` and `${ZETA_BASH_USER_HOME}/lib`.

The specification describe what variable need to be found and where to look at binaries:

```text
ZETA_BASH_HOME=/bin
+JAVA_HOME=/bin
```

The `+` before `JAVA_HOME` tells the builder to prepend the new path before.

**Note:** `PATH` leading to non existent directories are not removed.

To display the `PATH`, the `path-print` alias (see [aliases](#aliases) section below) can be used.

On Windows, this can produce this (with fancy colors):

```text
$ path-print
000: ${NODEJS_HOME}/
001: ${M2_HOME}/bin
002: ${JAVA_HOME}/bin
003: ${ZETA_BASH_HOME}/bin
004: ${MINGW_PREFIX}/bin
005: /usr/bin
006: /bin
007: ${SYSTEMROOT}/system32
008: ${SYSTEMROOT}
009: ${SYSTEMROOT}/System32/Wbem
010: ${SYSTEMROOT}/System32/WindowsPowerShell/v1.0
011: ${SYSTEMROOT}/System32/OpenSSH
012: ${PROGRAMFILES} (x86)/NVIDIA Corporation/PhysX/Common
013: ${PROGRAMFILES}/dotnet
014: ${PROGRAMFILES}/PuTTY
015: ${PROGRAMFILES}/TortoiseGit/bin
016: /cmd
017: ${PROGRAMFILES}/Docker/Docker/resources/bin
018: ${LOCALAPPDATA}/Microsoft/WindowsApps
019: ${PROGRAMFILES}/smartmontools/bin
020: ${LOCALAPPDATA}/Programs/Microsoft VS Code/bin
021: ${LOCALAPPDATA}/Microsoft/WinGet/Packages/jqlang.jq_Microsoft.Winget.Source_8wekyb3d8bbwe/
022: /usr/bin/vendor_perl
023: /usr/bin/core_perl
E00: JAVA_HOME=/e/apps/portable/java/latest/openjdk-17-hotspot
E01: LOCALAPPDATA=/c/Users/Foobar/AppData/Local
E02: M2_HOME=/e/apps/portable/apache-maven/apache-maven-3.9.4
E03: MINGW_PREFIX=/mingw64
E04: NODEJS_HOME=/e/apps/portable/nodejs/latest/node-v18
E05: PROGRAMFILES=/c/Program Files
E06: SYSTEMROOT=/c/Windows
E07: ZETA_BASH_HOME=/e/git/zb/zeta-bash
```

If now you are working with another JDK, you can create a new file in the `${ZETA_BASH_USER_HOME}/env` folder, say `windows.java7.env`:

```bash
_zb_env_source 'default'
export JAVA_DEFAULT=7
```

The `_zb_env_source` is particular and works like `source` bash built-in: it will load another environment file.

It is used to allow inheritance between configurations.

Now, setting `ZETA_BASH_ENV` to java7 in `.bashrc` and reloading the console will effectively use the JDK7.

And if you don't want to do that, you can simply call `env-reload java7` which will load the `windows.java7.bash` file and
then create a new `PATH` making Java 7 the default JDK.

Or, if you simply want to test with JDK7, you can also set up your `PATH` on the fly:

```text
[23:00:41] /e/git [1.8.0_92]
$ JAVA_HOME=".../jdk1.7.0_80" path-init ; path-print
```

**Notes:**

- Changing `MAVEN_DEFAULT` in a env file is faster than changing the path to it.
- If all that you need is changing maven version, use `switch-maven`. See [buildtools](buildtools.md).

## Aliases

- `env-reload`: invoke the function `_zb_env_reload` which reload the current environment.
- `env-load-aliases`: invoke the function `_zb_env_load_aliases` to load new aliases.
- `env-load-completions`: invoke the function `_zb_env_load_completions` to load new completions file
- `path-restore`: invoke the function `_zb_path_pop` which restore the previous `PATH` variable.
- `path-init`: initialize the current `PATH` using `_zb_path_init`
- `path-print`: display the current `PATH` in a readable format (invoke `_zb_path_print`).

## Environment

`ZETA_BASH_HOME`

  Points to where Zeta Bash is.

`ZETA_BASH_USER_HOME`

  Points to where user configuration are.

`ZETA_BASH_OS`

  Your current operating system. Two known values are `windows` and `linux`.

`ZETA_BASH_PATH_ORIG`

  The previous PATH, before Zeta Bash tempered with it.

`ZETA_BASH_ENV`

  Name of the environment as desired by the user; must be set **before** loading the `init.bash` script.

`ZETA_BASH_ENV_ACTUAL`

  The current loaded environment name.

`ZETA_BASH_ENV_LOAD_ALIASES`

  A space separated list of aliases file to load; default to a list matching aliases file found in `aliases/*.aliases.bash`.
  This directory can be either in `ZETA_BASH_HOME`, either `ZETA_BASH_USER_HOME`.

`ZETA_BASH_ENV_LOAD_COMPLETION`

  A space separated list of completion file to load; default to a list matching completions file found
  in `completions/*.completion.bash`.
  This directory can be either in `ZETA_BASH_HOME`, either `ZETA_BASH_USER_HOME`.
