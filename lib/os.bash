# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

#
# Return a simple string describing the OS at best.
#
# The string can be used to perform OS specific lookup, like <os>.default.env.
#
# This  should  only target "major" OS, and not specific versions (like distinguish between Windows XP, Vista,
# 7, 8, or Arch Linux, Gentoo, Debian or Ubuntu, ...).
#
_zb_determine_os() { # {{{
  if [[ -z "${ZETA_BASH_OS:-}" ]]; then
    local os="${OS:-}"
    case "${os,,}" in
      *windows*) export ZETA_BASH_OS=windows ;;
      *)         export ZETA_BASH_OS=linux ;;
    esac
  fi
} # }}} [_zb_determine_os]

#
# Return a simple string describing the arch at best.
#
# The string can be used to perform arch specific lookup, like <os>.default.env.
#
# The arch is normalized across system (and is lower cased).
#
_zb_determine_arch() { # {{{
  if [[ -z "${ZETA_BASH_ARCH:-}" ]]; then
    if [[ -n "${MSYSTEM_CARCH:-}" ]]; then
      export ZETA_BASH_ARCH="${MSYSTEM_CARCH,,}"
    elif command -v arch > /dev/null 2>&1 ; then
      ZETA_BASH_ARCH="$(arch)"
      export ZETA_BASH_ARCH="${ZETA_BASH_ARCH,,}"
    else
      eecho "could not determine arch for ${ZETA_BASH_COLOR_HL}${ZETA_BASH_OS:-}${ZETA_BASH_COLOR_NONE}"
      return 1
    fi
  fi
} # }}} [_zb_determine_arch]

