# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Append to COMPREPLY the possible completions
#
# @param -p prefix if present, and if prefix==$cur, then prefix each case with $prefix$sep
# @param -s sep separator to add to the prefix, if match (default is '=')
# @param cur current word
# @param opt... additional options
# @return 0 if prefix was found, 1 otherwise
#
# @usage
#  local opts=( a b )
#  _zb_complete_match_prefix "$cur" ${opts[@]} # cur='' => a b
#  _zb_complete_match_prefix -p '--application' "$cur" ${opts[@]} # cur='--aplication' => --aplication=a --aplication=b
#
_zb_complete_match_prefix() {
  _zb_complete_match_prefix2 -- "$@"
}

_zb_complete_match_prefix2() {
  local flag
  local OPTIND=1
  local prefix='' prefix_set=no
  local sep='='
  while getopts 'p:s:' flag "$@" ; do
    case "$flag" in
      p) prefix="$OPTARG" prefix_set=yes ;;
      *) eecho "unsupported flag: ${flag} ${OPTIND}" ;;
    esac
  done
  shift $((OPTIND-1))

  local cur="$1"
  if [[ "$prefix_set" == yes ]]; then
    prefix+="$sep"
    if [[ "${cur:0:${#prefix}}" != "${prefix}" ]]; then
      return 1
    fi
    cur="${cur:${#prefix}}"
  fi
  shift
  local found=1
  for opt; do
    if [[ ( -z "$cur" || "$opt" == "$cur"* ) && "$cur" != "$opt" ]]; then
      # this produce a better quote than @Q
      # printf ... : Two String -> Two\ String
      # printf ... : OneString  -> OneString
      # @Q ....... : Two String -> 'Two String'
      # @Q ....... : OneString  -> 'OneString'
      COMPREPLY+=( "$(printf "%q" "${opt}" )" )
      found=0
    fi
  done
  return $found
}

#
# Complete a file list (Zeta Bash oriented)
#
# @param cur current word (optional)
# @param prefix file prefix
# @param suffix file suffix
_zb_complete_filelist() {
  local cur
  if [[ "$#" == 3 ]]; then
    cur="$1"
    shift
  else
    # shellcheck disable=SC2034
    local prev item
    _get_comp_words_by_ref -n ':' cur prev
  fi

  local prefix="$1"
  local suffix="$2"

  local -a possible_completions=()
  for item in "$prefix"*"$suffix" ; do
    local item="${item:${#prefix}}"
    item="${item:0:-${#suffix}}"
    # there is probably no file containing '*' in most fs (even if some allow the
    # * to be used).
    if [[ "$item" != '*' ]]; then
      possible_completions+=( "$item" )
    fi
  done
  if [[ ${#possible_completions[@]} -gt 0 ]]; then
    _zb_complete_match_prefix "$cur" "${possible_completions[@]}"
  fi
}

_zb_completion_loader0() {
  local cmd="$1"
  local file=''
  local dynamic=no

  if [[ "$ZETA_BASH_OS" == 'windows' ]]; then
    case "$cmd" in
      find|grep)            file=findgrep ;;
      ls|ll|xargs|du)       file=binutils ;;
      tsvn|tgit|tortoise|t) file=tortoise-scm ;;
    esac
  fi
  if [[ -z "$file" ]]; then
    case "$cmd" in
      vscode)                   file=editor   ;;
      find-and-grep)            file=findgrep ;;
      g|_zb_gitex_main|gitex-*) file=gitex    ;;
      mvn|mvnDebug|mvnw)        file=maven    ;;
      ./mvnDebug|./mvnw)        file=maven    ;;
      nexus)                    file=nexus    ;;
      switch-java)              file=sdk ;;
      switch-maven)             file=sdk ;;
      switch-nodejs)            file=sdk ;;
      switch-node)              file=sdk ;;
      _zb_node_switch)          file=sdk ;;
      _zb_mvn_switch)           file=sdk ;;
      _zb_java_switch)          file=sdk ;;
      env-reload|path-init|_zb_env_reload|_zb_lib) file=zeta ;;
    esac
  fi

  if [[ -z "$file" ]]; then
    file="$cmd"
    dynamic=yes
  fi

  export ZETA_BASH_COMPLETION_LAST_CMDSPEC="$cmd"
  export ZETA_BASH_COMPLETION_LAST_FILE="$file"
  export ZETA_BASH_COMPLETION_LAST_DYNAMIC="$dynamic"

  if [[ -n "$file" ]]; then
    local -a files=()
    files+=( "${ZETA_BASH_HOME}/completions/${file}.completion.bash" )
    if [[ "$dynamic" == yes ]]; then
      files+=( "${ZETA_BASH_USER_HOME}/completions/${file}.completion.bash" )
      files+=( "${ZETA_BASH_USER_HOME}/completions/default.completion.bash" )
    fi

    # see bash documentation: 124 is special here!

    for completion_file in "${files[@]}" ; do
      if [[ -f "${completion_file}" ]]; then
        # shellcheck disable=SC1090
        source "${completion_file}" "$cmd" && return 0
      fi
    done

    if [[ $dynamic == no ]]; then
      eecho "completion mapping is broken: ${ZETA_BASH_COLOR_HL}${cmd}${ZETA_BASH_COLOR_NONE} -> ${ZETA_BASH_COLOR_RED}${file}${ZETA_BASH_COLOR_NONE}"
      return 2
    fi
  fi
  return 1
}

#
# ensure that some basic are found:
#
# - remove '@' from COMP_WORDBREAKS
#
_zb_completion_init() {
  #
  # when bash-completion is present, overrides it to use a
  #
  if [[ -n "${BASH_COMPLETION_VERSINFO}" ]] && declare -f __load_completion > /dev/null ; then
    _zb_completion_loader() {
      local cmd="$1"
      __load_completion "$cmd" && return 124
      _zb_completion_loader0 "$cmd" && return 124
      complete -F _minimal -- "$cmd" && return 124
    } && complete -D -F _zb_completion_loader
  else
    #
    # the '@' makes gitex group completion fails, and on Windows, since bash-completion is not present, the
    # _get_comp_words_by_ref fails at resplitting correctly the in @.
    #
    # see https://github.com/git-for-windows/build-extra/pull/293
    #
    #
    if [[ "$COMP_WORDBREAKS" == *@* ]]; then
      #
      # this add the '@', so we disable it.
      #
      shopt -u hostcomplete
    fi
    _zb_completion_loader() {
      local cmd="$1"
      #
      # git for windows have some, to some extend, completion for some commands:
      #
      # vim: fails because of missing _filedir
      #
      if [[ "$ZETA_BASH_OS" == 'windows' && "$cmd" != vim  ]]; then
        local path="/usr/share/bash-completion/completions/$cmd"
        if [[ -f "$path" ]]; then
          # shellcheck disable=SC1090
          source "$path" && return 124
        fi
      fi
      _zb_completion_loader0 "$cmd"             && return 124
      complete -o bashdefault -o default "$cmd" && return 124
    } && complete -D -F _zb_completion_loader
  fi
}


