#!/bin/bash

declare SCRIPT_FILE
if ! SCRIPT_FILE=$(realpath "$0"); then
  echo "realpath is skrewed !"
  exit 1
fi
declare SCRIPT_DIR="${SCRIPT_FILE%/*}"

export ZETA_BASH_HOME="${SCRIPT_DIR%/*}"
export ZETA_BASH_USER_HOME="${ZETA_BASH_HOME%/*}/zeta-bash-user"


# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/path.bash
source "${ZETA_BASH_HOME}/lib/path.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=tests/assertions.bash
source "${ZETA_BASH_HOME}/tests/assertions.bash"

export JAVA_HOME='/c/Program Files/Java/jdk8/'

_zb_path_init
_zb_path_print
_zb_path_init --clean
_zb_path_print
export PATH="$PATH:$PATH:$PATH"
_zb_path_init --remove-duplicates
_zb_path_print
