#!/bin/bash

# color constants to be used
declare -r C_RED="\e[1;31m"    #red
declare -r C_GREEN="\e[1;32m"  #green
declare -r C_NONE="\e[0m"      #none
declare -r C_YELLOW="\e[1;33m" #yellow
declare -r C_HL="\e[1m"        #highlight

declare DEBUG_FAILED=0
if [[ "$1" == "debug" ]]; then
  DEBUG_FAILED=1
fi

header() {
  echo -e "${C_YELLOW}${*}${C_NONE}"
}

item() {
  echo -e ":: ${*}"
}

run_checks() {
  local title="$1"
  local handler="$2"

  if ! $handler ; then
    item "[${C_RED}KO${C_NONE}] $title (${C_HL}${handler}${C_NONE})"
    if [[ "$DEBUG_FAILED" == 1 ]]; then
      set -x
      $handler
      set +x
    fi
    return 1
  else
    item "[${C_GREEN}OK${C_NONE}] $title (${C_HL}${handler}${C_NONE})"
    return 0
  fi
}

check_mapfile() {
  local  -a array=()
  # check mapfile with string
  mapfile -t array <<-XYZ
line1
line2
XYZ

  [[ "${array[0]}" == 'line1' && "${array[1]}" == 'line2' ]]
}

check_reference() {
  local a='value'
  local -n b=a
  [[ "$a" == "$b" ]]
}

check_quote_expansion() {
  local quotable="this is ' quoted"
  [[ "${quotable@Q}" == "'this is '\'' quoted'" ]]
}

check_mapfile_with_nul_delimiter() {
  local  -a array=()
  local delim=$'\0'
  if ! mapfile -d "${delim}" -t array < <(echo -n 'A;B;C;' | tr ';' '\0' ); then
    return 1
  fi
  [[ "${array[0]}" == 'A' && "${array[1]}" == 'B' && "${array[2]}" == 'C' ]]
}

check_negative_index() {
  local -a array=( a b c d )
  [[ "${array[-2]}" == c ]]
  [[ "${array[-1]}" == d ]]
}

# ensure we exit at first error, rather than never
set -e

header "Checking bash compatibility"
run_checks "mapfile" check_mapfile
run_checks "reference using local -n" check_reference
run_checks "quote expansion" check_quote_expansion
run_checks 'mapfile -d NUL' check_mapfile_with_nul_delimiter
run_checks 'negative index in array' check_negative_index

