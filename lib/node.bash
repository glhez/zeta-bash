# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Determine the maven version
#
# @param storage where to store the value (a local variable from calling function)
# @return 0 if executable found; 1 otherwise.
_zb_node_version() { # {{{
  local -gn storage="$1"
  local actual_executable=''

  local executable=node
  #[[ "${ZETA_BASH_OS}" == windows ]] && executable=node.exe

  # not found
  if ! actual_executable=$(command -v "${executable}" 2> /dev/null) || [[ -z "${actual_executable}" ]]; then
    storage=''
    return 1
  fi

  local refresh_indicator=''
  if _zb_prompt_updated ZETA_BASH_PS_NODE "${actual_executable}" ; then
    refresh_indicator='#'
  fi

  local version=""
  if [[ -z "${refresh_indicator}" ]]; then
    version="${ZETA_BASH_PS_NODE_VER:-}"
  else
    version=$("${actual_executable}" --version 2>&1 | sed -E -r -e 's@^v@@g' )
    if _zb_prompt_save -u ZETA_BASH_PS_NODE "${actual_executable}" "${version}" ; then
      _zb_env_session_save
    fi
    version="${refresh_indicator} ${version}"
  fi

  # shellcheck disable=SC2034  # this is a reference
  storage="${version}"
  return 0
} # }}} [_zb_node_version]

_zt_node_validate() {
  local node_home="$1"
  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    [[ -x "${node_home}/npm" ]]
  else
    [[ -x "${node_home}/bin/node" ]]
  fi
}

#
# produce a set of path containing NodeJS
# each entries may end with a filename itself referenceing @VER for version
#
_zb_node_path_entries() { # {{{
  _zb_bt_analyze_entry environment 'NODE@_HOME' 'NODEJS@_HOME'
  # nodejs-downloader
  for dir in "${ZETA_BASH_NODEJS_CONTAINER_PATH:-}" "${NODEJS_INSTALL_HOME:-}" ; do
    [[ -n "${dir}" && -d "${dir}" ]] && _zb_bt_analyze_entry directory "${dir}/latest/node-v@VER"
  done
  # use https://github.com/nvm-sh/nvm
  [[ -d "$HOME/.nvm/versions/node" ]] && _zb_bt_analyze_entry directory "$HOME/.nvm/versions/node/v@VER"
  # winget install nodejs
  # apt install nodejs
  for file in '/c/Program Files/nodejs/node.exe' '/usr/bin/node' ; do
    [[ -x "$file" && -f "${file}" ]] && _zb_bt_analyze_entry version 'v([0-9]+)([.]([0-9]+)([.]([0-9]+))?)?' '1 3 5' "-" "$file" --version
  done
} # }}}

_zb_node_switch() { # {{{
  local quiet=no
  local version=''
  for arg; do
    if [[ "${arg}" == --quiet ]]; then
      quiet=yes
    elif [[ -z "$version" ]]; then
      version="$arg"
    fi
  done

  [[ -z "${version}" ]] && version="${NODE_DEFAULT}"
  [[ -z "${version}" ]] && return 1

  if ! _zt_utils_parse_semver "$version" ; then
    eecho "invalid nodejs version ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE} (not a semver)"
    return 1
  fi

  local node_home=''
  if ! _zb_bt_resolve_container node_home _zb_node_path_entries _zt_node_validate ; then
    eecho "could not resolve a nodejs home for version ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE}."
    return 1
  fi

  if ! _zb_env_update_var "NODE_HOME" "${node_home}" "${quiet}" ; then
    return 0
  fi

  export ZETA_BASH_NODE_EXECUTABLE="${node_home}/node"

  _zb_path_replace_app_home --app-name=nodejs \
     "--quiet=$quiet" \
     "--app-home=$node_home" \
     "--relative-app-bin-windows=/" \
     "--relative-app-bin-linux=/bin" \
     "--executable=node" \
     --directories=_zb_node_path_entries


  # refresh the prompt, but don't reevaluate the version if empty
  if actual_executable=$(command -v node 2> /dev/null) ; then
    _zb_prompt_save ZETA_BASH_PS_NODE "${actual_executable}" '' || :
  fi
  _zb_env_session_save
  return 0
} # }}} [_zb_node_switch]
