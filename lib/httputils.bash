
# @param file file to validate
# @param checksum checksum file, should contains only the checksum
_zb_httputils_validate_checksum() {
  local file="$1"
  local checksum="$2"
  local tool=''
  case "$checksum" in
    *'.sha1')   tool=sha1sum   ;;
    *'.sha256') tool=sha256sum ;;
    *'.sha512') tool=sha512sum ;;
    *) return 1 ;;
  esac
  local mask=''
  if ! mask="$(<"${checksum}") *" ; then
    return 1
  fi
  if "${tool}" --binary "${file}" | grep -i -F "$mask" --quiet ; then
    return 0
  fi
  return 1
}

#
# Mimic wget, with checksum validation
#
# Create intermedidate directory
# Delete target_file is download failed or if checksum mismatch
#
#
# @param source_url source url
# @param target_file target file
# @param checksum_url a possible checksum url
# @param checksum_file possible checksum file; if empty use the url as base
#
_zb_httputils_wget() {
  local source_url="$1"
  local target_file="$2"
  local checksum_url="${3:-}"
  local checksum_file="${4:-}"

  local filename="${target_file##*/}"

  if [[ -n "${checksum_url}" ]]; then
    if [[ -z "${checksum_file}" ]]; then
      case "${checksum_url}" in
        *.sha1)   checksum_file="${target_file}.sha1" ;;
        *.sha256) checksum_file="${target_file}.sha256" ;;
        *.sha512) checksum_file="${target_file}.sha512" ;;
        *)
          eecho "checksum_file is required when url does not contains a valid extension."
          return 2
        ;;
      esac
    fi

    if [[ ! -f "${checksum_file}" ]]; then
      local checksum_dir="${checksum_file%/*}"
      iwrap "downloading ${ZETA_BASH_COLOR_HL}${checksum_url}${ZETA_BASH_COLOR_NONE}"
      if [[ ! -d "${checksum_dir}" ]] && ! mkdir -p "${checksum_dir}" ; then
        eecho "download ${ZETA_BASH_COLOR_HL}${checksum_url}${ZETA_BASH_COLOR_NONE} failed: could not create intermediate directories ${ZETA_BASH_COLOR_HL}${checksum_dir}${ZETA_BASH_COLOR_NONE}."
        return 1
      fi
      if ! curl -s --fail -L "${checksum_url}" -o "${checksum_file}" ; then
        eecho "download ${ZETA_BASH_COLOR_HL}${checksum_url}${ZETA_BASH_COLOR_NONE} failed: curl failed."
        rm -f "${checksum_file}"
        return 1
      fi
    else
      wwrap "using previous checksum found in ${ZETA_BASH_COLOR_HL}${checksum_file}${ZETA_BASH_COLOR_NONE}"
    fi

    if [[ -f "${target_file}" ]]; then
      iwrap "validating checksum of ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}"
      if ! _zb_httputils_validate_checksum "${target_file}" "${checksum_file}" ; then
        wecho "checksum mismatch for ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}: will redownload file."
        rm -f "${target_file}"
      else
        iecho "validated checksum of ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}"
      fi
    fi
  fi

  if [[ ! -f "${target_file}" ]]; then
    local target_dir="${target_file%/*}"
    iwrap "downloading ${ZETA_BASH_COLOR_HL}${source_url}${ZETA_BASH_COLOR_NONE}"
    if [[ ! -d "${target_dir}" ]] && ! mkdir -p "${target_dir}" ; then
      eecho "download ${ZETA_BASH_COLOR_HL}${source_url}${ZETA_BASH_COLOR_NONE} failed: could not create intermediate directories ${ZETA_BASH_COLOR_HL}${target_dir}${ZETA_BASH_COLOR_NONE}."
      return 1
    fi
    if ! curl -s --fail -L "${source_url}" -o "${target_file}" ; then
      eecho "download ${ZETA_BASH_COLOR_HL}${source_url}${ZETA_BASH_COLOR_NONE} failed: curl failed."
      rm -f "${target_file}"
      return 1
    fi
    iecho "downloaded ${ZETA_BASH_COLOR_HL}${source_url}${ZETA_BASH_COLOR_NONE}"

    if [[ -n "${checksum_url}" ]]; then
      iwrap "validating checksum of ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}"
      if ! _zb_httputils_validate_checksum "${target_file}" "${checksum_file}" ; then
        eecho "checksum mismatch for ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}: keeping file at your own discretion."
        return 2
      else
        iecho "validated checksum of ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}"
      fi
    fi
  fi

  return 0
}

_zb_httputils_extract0() {
  local source_file="$1"
  local target_dir="$2"
  case "$source_file" in
    *.zip)                       unzip -n -qq "${source_file}" -d "${target_dir}" || return 1 ;;
    *.tar.gz|*.tar.bz2|*.tar.xz) tar axf "${target_file}" -C "${target_dir}"      || return 1 ;;
    *) eecho "unsupported archive type." ; return 1 ;;
  esac
  return 0
}

#
# Extract result of _zb_httputils_wget
#
# @param source_file source file to extract
# @param target_dir target dir to cd into
# @param validate a validate handler targing source_file and target_dir
# @param other... other parameters for handler
#
_zb_httputils_extract() {
  local source_file="$1"
  local target_dir="$2"
  shift 2
  local validate=''
  if [[ $# != 0 ]]; then
    validate="${1:-}"
    shift 1
  fi

  if [[ ! -f "${source_file}" ]]; then
    eecho "nothing to extract: ${ZETA_BASH_COLOR_HL}${source_file}${ZETA_BASH_COLOR_NONE} does not exists."
    return 1
  fi
  iwrap "extracting ${ZETA_BASH_COLOR_HL}${source_file}${ZETA_BASH_COLOR_NONE}"
  if ! _zb_httputils_extract0 "${source_file}" "${target_dir}" ; then
    eecho "extraction of ${ZETA_BASH_COLOR_HL}${source_file}${ZETA_BASH_COLOR_NONE} in ${ZETA_BASH_COLOR_HL}${target_dir}${ZETA_BASH_COLOR_NONE} failed"
    return 1
  fi
  if [[ -n "${validate}" ]] && ! "${validate}" "${source_file}" "${target_dir}" "$@" ; then
    eecho "extraction of ${ZETA_BASH_COLOR_HL}${source_file}${ZETA_BASH_COLOR_NONE} in ${ZETA_BASH_COLOR_HL}${target_dir}${ZETA_BASH_COLOR_NONE} is not valid (see ${validate})"
    return 1
  fi
  iecho "extracted ${ZETA_BASH_COLOR_HL}${source_file}${ZETA_BASH_COLOR_NONE}"
  return 9
}
