# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# make a file as being unchanged; it will not appear in the index as "changed". Usefull when using git-svn.
alias git-assume-unchanged="git update-index --assume-unchanged"
# make a file as being changed; it is the opposite operation of git-assume-unchanged.
alias git-assume-changed="git update-index --no-assume-unchanged"
# list file assumed to be unchanged
alias git-show-unchanged="git ls-files -v | grep -E '^[a-z]'"

# git alias
alias bb='git branch -a'
alias ss='git status'
# don't use su, at to not interfer with linux su
alias uu='git status --untracked-files=all'
alias tt='git tag'

alias tgit="\${ZETA_BASH_HOME}/bin/git/tortoise-git.bash"
