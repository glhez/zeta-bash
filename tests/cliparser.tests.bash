#!/bin/bash -eu

declare SCRIPT_FILE
if ! SCRIPT_FILE=$(realpath "$0"); then
  echo "realpath is skrewed !"
  exit 1
fi
declare SCRIPT_DIR="${SCRIPT_FILE%/*}"

export ZETA_BASH_HOME="${SCRIPT_DIR%/*}"
export ZETA_BASH_USER_HOME="${ZETA_BASH_HOME%/*}/zeta-bash-user"

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/path.bash
source "${ZETA_BASH_HOME}/lib/path.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/cliparser.bash
source "${ZETA_BASH_HOME}/lib/cliparser.bash"
# shellcheck source=tests/assertions.bash
source "${ZETA_BASH_HOME}/tests/assertions.bash"

# options {{{
declare -a  OPTIONS_USER_VERSIONS
declare -x  OPTIONS_SAVE_CONFIG
declare -xA OPTIONS_RELEASES
declare -x  OPTIONS_REFRESH
declare -x  OPTIONS_CLEAN
declare -x  OPTIONS_MAVEN_TOOLCHAIN
declare -xA OPTIONS_IMPL
# }}} [options]

beforeAll() {
  local ojdk_share="${ZETA_BASH_HOME}/share/openjdk"
  assert_execute _zb_cliparser_write_config_header_footer "${ojdk_share}/openjdk-conf-header.bash" ''
  assert_execute _zb_cliparser_set_usage_file "${ojdk_share}/usage.txt"

  assert_execute _zb_cliparser_set_user_args                OPTIONS_USER_VERSIONS
  assert_execute _zb_cliparser_describe                     OPTIONS_USER_VERSIONS   "jdk major version (use - to remove)"

  assert_execute _zb_cliparser_register_option_variable     OPTIONS_SAVE_CONFIG     bool
  assert_execute _zb_cliparser_register_option_map_singular OPTIONS_SAVE_CONFIG     ':save-config' ':save'

  assert_execute _zb_cliparser_register_option_variable     OPTIONS_MAVEN_TOOLCHAIN bool
  assert_execute _zb_cliparser_describe                     OPTIONS_MAVEN_TOOLCHAIN "generate maven toolchain"
  assert_execute _zb_cliparser_register_option_map_singular OPTIONS_MAVEN_TOOLCHAIN ':toolchain'

  assert_execute _zb_cliparser_register_option_variable     OPTIONS_IMPL            bool[enum] hotspot openj9
  assert_execute _zb_cliparser_describe                     OPTIONS_IMPL            "jdk implementation"
  assert_execute _zb_cliparser_register_option_map          OPTIONS_IMPL            --impl required
  assert_execute _zb_cliparser_register_option_map_singular OPTIONS_IMPL            ':hotspot' ':openj9'
  assert_execute _zb_cliparser_register_option_variable     OPTIONS_RELEASES        bool[enum] lts latest all

  assert_execute _zb_cliparser_register_option_variable     OPTIONS_CLEAN           enum archive dir all no
  assert_execute _zb_cliparser_describe                     OPTIONS_CLEAN           "clean directory, archive, all or none"
  assert_execute _zb_cliparser_register_option_map_singular OPTIONS_CLEAN           ':clean-archive:archive' ':clean-dir:dir' ':clean:all' ':no-clean:no'
  assert_execute _zb_cliparser_register_option_map          OPTIONS_CLEAN           --clean optional all

  assert_execute _zb_cliparser_register_option_map_singular OPTIONS_RELEASES        ':lts' ':all' ':latest'
  assert_execute _zb_cliparser_describe                     OPTIONS_RELEASES        "jdk release to pickup (map with keys: lts, all, latest and value yes/no)"

  CLI_PARSER_VARIABLES_SET=()
  for variable in "${!CLI_PARSER_VARIABLES_DESCRIPTION[@]}"; do
    unset "${CLI_PARSER_CONFIG_VARPREFIX}${variable}"
  done

}

setUp() {
  OPTIONS_USER_VERSIONS=()
  OPTIONS_SAVE_CONFIG=no
  OPTIONS_RELEASES=( [lts]=yes [latest]=yes [all]=no )
  OPTIONS_REFRESH=no
  OPTIONS_CLEAN=no
  OPTIONS_MAVEN_TOOLCHAIN=no
  OPTIONS_IMPL=( [hotspot]=yes [openj9]=no )

  return 0
}

test_display_usage() {
  assert_should_fail "display usage" _zb_cliparser_parse_options --help
}

test_single_boolean_value() {
  OPTIONS_SAVE_CONFIG=no
  assert_should_success "--save-config" _zb_cliparser_parse_options --save-config
  assert_true "OPTIONS_SAVE_CONFIG should be true" "$OPTIONS_SAVE_CONFIG"
}

test_boolean_emum_array() {
  OPTIONS_RELEASES=( [lts]=no [latest]=no [all]=no )

  assert_should_success "changing releases to lts and latest" _zb_cliparser_parse_options --lts --latest
  assert_map_equals "OPTIONS_RELEASES" lts=yes latest=yes all=no

  assert_should_success "changing releases to all" _zb_cliparser_parse_options --no-lts --no-latest --all
  assert_map_equals "OPTIONS_RELEASES" lts=no latest=no all=yes
}

test_boolean_enum_and_special_values() {
  OPTIONS_IMPL=()

  assert_should_success "changing impl to all" _zb_cliparser_parse_options --impl=all
  assert_map_equals "OPTIONS_IMPL" hotspot=yes openj9=yes

  assert_should_success "changing impl to none with hotspot" _zb_cliparser_parse_options --impl=none --hotspot
  assert_map_equals "OPTIONS_IMPL" hotspot=yes openj9=no

  assert_should_success "changing impl to all with no hotspot" _zb_cliparser_parse_options --impl=all --no-hotspot
  assert_map_equals "OPTIONS_IMPL" hotspot=no openj9=yes

  assert_should_success "changing impl to none with --hotspot and --openj9" _zb_cliparser_parse_options --impl=none --hotspot --openj9
  assert_map_equals "OPTIONS_IMPL" hotspot=yes openj9=yes
}

test_user_options() {
  assert_should_success "should store the user options" _zb_cliparser_parse_options a b c
  assert_array_equals "OPTIONS_USER_VERSIONS" a b c
}

test_write_config() {
  local config_file="$PWD/config/openjdk.conf"

  CLI_PARSER_VARIABLES_SET[OPTIONS_CLEAN]=yes
  CLI_PARSER_VARIABLES_SET[OPTIONS_MAVEN_TOOLCHAIN]=yes
  CLI_PARSER_VARIABLES_SET[OPTIONS_RELEASES]=yes
  CLI_PARSER_VARIABLES_SET[OPTIONS_USER_VERSIONS]=yes
  CLI_PARSER_VARIABLES_SET[OPTIONS_IMPL]=yes

  assert_execute _zb_cliparser_write_config "${config_file}"

  assert_file_equals "${config_file}" "${SCRIPT_DIR}/cliparser/test_write_config/save_config_01"
}

test_read_config_file_not_exists() {
  local config_file="${SCRIPT_DIR}/cliparser/test_read_config/read_config_01"

  assert_execute _zb_cliparser_read_config "${config_file}"

  for variable in "${!CLI_PARSER_VARIABLES_DESCRIPTION[@]}"; do
    assert_not_set "${CLI_PARSER_CONFIG_VARPREFIX}${variable}"
  done
}

test_read_config_file_exists() {
  local config_file="${SCRIPT_DIR}/cliparser/test_read_config/read_config_02.conf"

  assert_execute _zb_cliparser_read_config "${config_file}"

  assert_map_equals   CLI_PARSER_VARIABLES_SET OPTIONS_CLEAN=yes \
                                               OPTIONS_MAVEN_TOOLCHAIN=yes \
                                               OPTIONS_IMPL=yes \
                                               OPTIONS_RELEASES=yes \
                                               OPTIONS_USER_VERSIONS=yes

  assert_equals       "OPTIONS_CLEAN should be archive" archive "${OPTIONS_CLEAN}"
  assert_equals       OPTIONS_MAVEN_TOOLCHAIN yes "${OPTIONS_MAVEN_TOOLCHAIN}"
  assert_map_equals   OPTIONS_IMPL            openj9=yes hotspot=no
  assert_map_equals   OPTIONS_RELEASES        lts=yes all=no
  assert_array_equals OPTIONS_USER_VERSIONS   17 -18 19 -20
}

run_test_suite "${SCRIPT_FILE##*/}" "$@"



