# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#
_zb_bt_complete_container() { # {{{
  local cur="$1"
  local entry_provider="$2"
  local test_command="$3"


  mapfile -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "--quiet" -- "${cur}" )
  _zb_bt_resolve_find --provider "${entry_provider}" --validator "${test_command}"
  _zb_bt_expand_sort --completion
  _zb_bt_resolve_destroy
  [[ "${#ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY[@]}" != 0 ]] && mapfile -O "${#COMPREPLY[@]}" -t COMPREPLY < <(compgen -W "${ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY[*]}" -- "${cur}" )
  return 0
} # }}} [_zb_bt_complete_container]


_zb_java_switch_completion() { _zb_bt_complete_container "$2" _zb_java_path_entries _zb_java_validate ; }
_zb_mvn_switch_completion()  { _zb_bt_complete_container "$2" _zb_mvn_path_entries  _zt_mvn_validate   ; }
_zb_node_switch_completion() { _zb_bt_complete_container "$2" _zb_node_path_entries _zt_node_validate   ; }

complete -o bashdefault -o nospace             -F _zb_mvn_switch_completion  switch-maven              _zb_mvn_switch
complete -o bashdefault -o nospace             -F _zb_node_switch_completion switch-nodejs switch-node _zb_node_switch
complete -o bashdefault -o nospace -o dirnames -F _zb_java_switch_completion switch-java               _zb_java_switch

