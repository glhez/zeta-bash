# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

_zb_gitex_main() {
  local exchange_file="${ZETA_BASH_USER_CACHE}/zb-gitex-fifo.$$"
  rm -f "${exchange_file}"

  [[ ! -d "${ZETA_BASH_USER_CACHE}" ]] && mkdir -p "${ZETA_BASH_USER_CACHE}"

  export ZETA_BASH_GITEX_FIFO="${exchange_file}"
  if ! "${ZETA_BASH_HOME}/bin/gitex-main" "$@" ; then
    return 1
  fi

  #
  # this is "kind of a hack" to let parent shell be aware that
  # we want to cd (or use a hook)
  #
  if [[ -f "${ZETA_BASH_GITEX_FIFO}" ]]; then
    local src=''
    src="$(<"${ZETA_BASH_GITEX_FIFO}")"
    rm -f "${ZETA_BASH_GITEX_FIFO}"
    #shellcheck disable=SC1090
    eval "${src}"
  fi
}
