# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# provide dumb completion for find and grep
#

_zb_complete_find() {
  _zb_complete_find_and_grep0 yes no no "$@"
}

_zb_complete_grep() {
  _zb_complete_find_and_grep0 no yes no "$@"
}

_zb_complete_find_and_grep() {
  _zb_complete_find_and_grep0 yes yes yes "$@"
}

#
# completion for find-and-grep
#
_zb_complete_find_and_grep0() {
  local complete_find="$1"
  local complete_grep="$2"
  local complete_find_and_grep="$3"
  shift 3

  local cur0="$2"
  local prev0="$3"

  local cur prev #item
  _get_comp_words_by_ref -n "=:" cur prev

  COMPREPLY=()

  #
  if [[ "${complete_find_and_grep}" == yes ]]; then
    local -a find_and_grep_opts=(
      -jedit  --code  -vscode  -vscodium  -vsc  -n++  -npp  -notepadplusplus  -notepad++
      --jedit --code --vscode --vscodium --vsc --n++ --npp --notepadplusplus --notepad++
      -ext -extension -maven -java -properties -xml -bash '--'
      --disable-default-exclude -dde -sql
      --editor-limit=
    )

    _zb_complete_match_prefix "$cur" "${find_and_grep_opts[@]}"
  fi

  if [[ "${complete_find}" == yes ]]; then
    local -a find_opts=()
    if [[ "$prev" == -type ]]; then
      find_opts=( b c d p f l s D )
    elif [[ "$prev" == -xtype ]]; then
      find_opts=( b c d p f l s )
    else
      find_opts=(
        -depth    --help    -maxdepth    -mindepth    -mount    -noleaf   --version   -xdev   -ignore_readdir_race
        -noignore_readdir_race  -amin  -anewer  -atime  -cmin  -cnewer  -ctime  -empty  -false -fstype -gid -group
        -ilname  -iname  -inum  -iwholename -iregex -links -lname -mmin -mtime -name -newer -nouser -nogroup -path
        -perm  -regex  -readable  -writable  -executable  -wholename  -size  -true  -type  -uid -used -user -xtype
        -context  -delete  -print0  -printf  -fprintf  -print  -fprint0  -fprint  -ls  -fls -prune -quit -exec -ok
        -execdir -okdir
      )
    fi

    _zb_complete_match_prefix "$cur" "${find_opts[@]}"
  fi

  if [[ "${complete_grep}" == yes ]]; then

    #
    # special cases
    #
    local -a special_opts=( --binary-file --color --colour --directories -d --devices -D )

    local word="$cur"
    local special_opt=''
    local curnoeq="${cur%=*}"

    for (( i = 0; i < ${#special_opts[@]}; ++i )); do
      local opt="${special_opts[$i]}"
      if [[ "${opt:0:2}" != '--' ]]; then
        if [[ "$prev" == "$opt" ]]; then
          special_opt="$opt"
        fi
      else
        if [[ "$prev" == "$opt" ]]; then
          special_opt="$opt"
        elif [[ "$opt" == "${curnoeq}" ]]; then
          # --color=always
          # -------^        CUR0='--color'    PREV0=''
          # --------^       CUR0='a'          PREV0='='
          if [[ "$prev0" == '=' ]]; then
            word="$cur0"
          else
            word=''
          fi
          special_opt="$opt"
        fi
      fi
      if [[ -n "$special_opt" ]]; then
        break
      fi
    done

    local -a grep_opts=()
    if [[ -n "$special_opt" ]]; then
      case "$special_opt" in
        --binary-file)     grep_opts=( "binary" "text"  "without-match" ) ;;
        --color|--colour)  grep_opts=( "always" "never" "auto"         ) ;;
        --directories|-d)  grep_opts=( "read" "recurse" "skip"         ) ;;
        --devices|-D)      grep_opts=( "read" "skip"                            ) ;;
      esac
    else
      grep_opts=(
        -E  --extended-regexp  -F  --fixed-strings  -G  --basic-regexp  -P  --perl-regexp -e --regexp -f --file -i
        --ignore-case  -w  --word-regexp  -x  --line-regexp  -z  --null-data -s --no-messages -v --invert-match -V
        --version  --help  -m  --max-count -b --byte-offset -n --line-number --line-buffered -H --with-filename -h
        --no-filename  --label -o --only-matching -q --quiet --silent --binary-files -a --text -I -d --directories
        -D  --devices  -r  --recursive -R --dereference-recursive --include --exclude --exclude-from --exclude-dir
        -L   --files-without-match   -l   --files-with-matches   -c   --count   -T   --initial-tab  -Z  --null  -B
        --before-context -A --after-context -C --context -NUM --color --colour -U --binary
        '--after-context=' '--before-context=' '--binary-files=' '--color=' '--colour=' '--context=' '--devices='
        '--directories='   '--exclude-dir='    '--exclude-from=' '--exclude=' '--file=' '--include='  '--label='  '--max-count='
        '--regexp='
      )
    fi
    _zb_complete_match_prefix "$word" "${grep_opts[@]}"
  fi
}

# linux have probably better completion for those
if [[ "$ZETA_BASH_OS" == 'windows' ]]; then
  complete -o default -o bashdefault -o dirnames -o nospace -o filenames -F _zb_complete_find find
  complete -o default -o bashdefault -o dirnames -o nospace -o filenames -F _zb_complete_grep grep
fi
complete -o default -o bashdefault -o nospace -o dirnames -o filenames -F _zb_complete_find_and_grep find-and-grep

