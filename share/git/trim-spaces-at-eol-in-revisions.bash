#!/bin/bash
# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"

if [[ -z "${FIND_EXPRESSION}" ]]; then
  echo -e "${ZETA_BASH_COLOR_RED}error:${ZETA_BASH_COLOR_NONE} missing ${ZETA_BASH_COLOR_HL}FIND_EXPRESSION${ZETA_BASH_COLOR_NONE} enviromnent variable"
  exit 2
fi

# shellcheck disable=SC2086
find ${FIND_EXPRESSION} > /dev/null

if [[ $? -ne 0 ]]; then
  echo -e "${ZETA_BASH_COLOR_RED}error:${ZETA_BASH_COLOR_NONE} find ${ZETA_BASH_COLOR_HL}${FIND_EXPRESSION}${ZETA_BASH_COLOR_NONE} failed."
  exit 2
fi

if [[ $# -eq 0 ]]; then
  echo -e "${ZETA_BASH_COLOR_RED}error:${ZETA_BASH_COLOR_NONE} please select some <rev-list> to use."
  exit 2
fi

git filter-branch --prune-empty --tree-filter "find ${FIND_EXPRESSION} -print0 | xargs -r0 sed -i -r -e 's/[ \t]+\$//g'" -- "$@"

