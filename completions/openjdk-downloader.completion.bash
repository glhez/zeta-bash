# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# @param command bash completion $1
# @param word    bash completion $2
# @param pword   bash completion $3
_zb_openjdk_downloader_completion_() { # {{{
  # shellcheck disable=SC2034
  local cmd="$1"
  local cur=''
  # shellcheck disable=SC2034
  local prev=''
  local -a words=()
  # shellcheck disable=SC2034
  local cword=''

  _get_comp_words_by_ref -n '=' cur prev words cword

  local -A completed_options=()
  for (( i = 1, n = ${#words[@]}; i < n; ++i )); do
    local word="${words[$i]}"
    if [[ -n "$word" ]]; then
      # handle alias
      case "$word" in
        -p|--lts)        completed_options[-p]=yes completed_options[--lts]=yes       ;;
        -A|--all)        completed_options[-A]=yes completed_options[--all]=yes       ;;
        -x|--latest)     completed_options[-x]=yes completed_options[--latest]=yes    ;;
        -J|--junction)   completed_options[-J]=yes completed_options[--junction]=yes  ;;
        -L|--link)       completed_options[-L]=yes completed_options[--link]=yes      ;;
        -e|--elevated)   completed_options[-e]=yes completed_options[--elevated]=yes  ;;
        -r|--refresh)    completed_options[-r]=yes completed_options[--refresh]=yes   ;;
        -W|--wait)       completed_options[-W]=yes completed_options[--wait]=yes      ;;
        -t|--toolchain)  completed_options[-t]=yes completed_options[--toolchain]=yes ;;
        -c|--clean)
          completed_options[-c]=yes
          completed_options[--clean]=yes
          completed_options[--clean-archive]=yes
          completed_options[--clean=archive]=yes
          completed_options[--clean-dir]=yes
          completed_options[--clean=dir]=yes
        ;;
        --clean-archive|--clean=archive)
          completed_options[--clean-archive]=yes
          completed_options[--clean=archive]=yes
        ;;
        --clean-dir|--clean=dir)
          completed_options[--clean-dir]=yes
          completed_options[--clean=dir]=yes
        ;;
        -s|--save-config|--save)
          completed_options[-s]=yes
          completed_options[--save]=yes
          completed_options[--save-config]=yes
        ;;

        *) completed_options[$word]=yes
      esac
    fi
  done

  local -a completions=()
  # release selection
  completions+=( --lts    --all    --latest    )
  completions+=( --no-lts --no-all --no-latest )
  completions+=( -p       -A        -x         )
  # impl selection
  completions+=( --impl=none --impl=all --impl=hotspot --impl=openj9 )
  # link generation
  completions+=( --link    --junction --elevated )
  completions+=( -L        -J         -e )
  completions+=( --no-link            --no-elevated )
  # misc
  completions+=( --refresh --wait --toolchain --save-config --save )
  completions+=( -r        -W     -t          -s )
  completions+=( --no-refresh --no-wait --no-toolchain  )
  # clean up
  completions+=( --clean         --no-clean -c )
  completions+=( --clean-archive --clean=archive )
  completions+=( --clean-dir     --clean=dir )

  # populate the possible releases if we can
  local var_file=''
  for dir in ZETA_BASH_JAVA_CONTAINER_PATH OPENJDK_HOME ; do
    local path
    eval "path=\${$dir}"
    if [[ -n "${path}" && -d "${path}/.cache" ]]; then
      # note: + seems to be a meta for find.
      if var_file=$(find "${path}/.cache" -regextype sed -iregex '^.\+/releases-[0-9]\+-[0-9]\+\.bash' \
                        | sort --reverse \
                        | head -n1) && [[ -f "$var_file" ]]; then
        break
      fi
      var_file=''
    fi
  done

  if [[ -n "$var_file" ]]; then
    local -a all_releases=()
    # shellcheck disable=SC1090
    source "$var_file"
    for release in "${all_releases[@]}"; do
      completions+=( "$release" "-$release" )
    done
  fi

  # clean up already completed
  local -a not_completed_options=()
  for completion in "${completions[@]}" ; do
    if [[ -z "${completed_options[${completion}]}" ]]; then
      not_completed_options+=( "$completion" )
    fi
  done
  _zb_complete_match_prefix "$cur" "${not_completed_options[@]}"
} # }}} [_zb_openjdk_downloader_completion_]

# don't add filenames: this make names with '/' fails
# https://stackoverflow.com/questions/55476845/escape-slashes-in-bash-complete
complete -o bashdefault -o nospace -F _zb_openjdk_downloader_completion_ openjdk-downloader



