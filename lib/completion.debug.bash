# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# completion debug facilities (must be loaded prior to use)
#

_zb_completion_array_tracer() {
  local array="$1"
  shift
  local i=0
  printf 'string[] %s:%b' "$array" '\n'
  for arg; do
    printf '>[%d]: [%s]%b' "$i" "$arg" '\n'
    (( ++i ))
  done
}

_zb_completion_variables_tracer() {
  printf 'int    COMP_CWORD=%q index(COMP_WORDS)%b'    "${COMP_CWORD}"       '\n'
  printf 'string COMP_LINE=[%s]%b'                     "${COMP_LINE}"        '\n'
  printf 'int    COMP_POINT=[%s]: offset(COMP_LINE)%b' "${COMP_POINT}"       '\n'
  printf 'enum   COMP_TYPE=%q%b'                       "${COMP_TYPE}"        '\n'
  printf 'char[] COMP_WORDBREAKS=%q%b'                 "${COMP_WORDBREAKS}"  '\n'
  _zb_completion_array_tracer COMP_WORDS "${COMP_WORDS[@]}"

}

_zb_completion_reply_tracer() {
  _zb_completion_array_tracer COMPREPLY "${COMPREPLY[@]}"
}

_zb_completion_args_tracer() {
  _zb_completion_array_tracer '$@' "$@"
}

_zb_completion_tracer() {
  _zb_completion_args_tracer "$@"
  _zb_completion_variables_tracer
  _zb_completion_reply_tracer
}

_zb_complete_trace() {
  local tracer="${1:-_zb_completion_tracer}"
  shift
  local col
  local maxlen=80
  (( col=COLUMNS - maxlen - 4 ))

  if [[ $col -le 0 ]]; then
    return 0
  fi

  echo -e "\e[s"
  local lineno=1
  while read -r line; do
    echo -ne "\e[${lineno};${col}H\e[K\e[0m${ZETA_BASH_COLOR_RED}$(printf "%02d: " "${lineno}")\e[0m\e[7m"
    echo -n "${line:0:$maxlen}"
    (( ++lineno ))
    if [[ $lineno -gt $LINES ]]; then
      break
    fi
  done < <("$tracer" "$@")
  while [[ $lineno -le $LINES ]]; do
    echo -ne "\e[0m\e[${lineno};${col}H\e[K\e[0m${ZETA_BASH_COLOR_PURPLE}$(printf "%02d: " "${lineno}")\e[0m"
    echo -n "${line:0:$maxlen}"
    (( ++lineno ))
  done
  echo -e "\e[u\e[0m"
}

