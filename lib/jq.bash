# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Validate that JQ_EXECUTABLE is found.
#
_zb_jq_init() { # {{{
  if [[ -z "${JQ_EXECUTABLE:-}" ]] ; then
    JQ_EXECUTABLE=''
    if [[ "${ZETA_BASH_OS}" != windows ]]; then
      if ! JQ_EXECUTABLE=$(command -v jq) ; then
        eecho "missing ${ZETA_BASH_COLOR_HL}JQ_EXECUTABLE${ZETA_BASH_COLOR_NONE} env"
        wecho "please download JQ from ${ZETA_BASH_COLOR_HL}https://jqlang.github.io/jq/download/${ZETA_BASH_COLOR_NONE}"
        wecho "or install it: "
        wecho "  ${ZETA_BASH_COLOR_HL}sudo apt install jq${ZETA_BASH_COLOR_NONE}"
        wecho "  ${ZETA_BASH_COLOR_HL}sudo emerge jq${ZETA_BASH_COLOR_NONE}"
        return 1
      fi
    else
      if ! JQ_EXECUTABLE=$(command -v jq.exe) ; then
        # let's try winget installation folder, in case.
        # not sure if the identifier can vary
        JQ_EXECUTABLE="$USERPROFILE/AppData/Local/Microsoft/WinGet/Packages/jqlang.jq_Microsoft.Winget.Source_8wekyb3d8bbwe/jq.exe"
        if [[ ! -f "${JQ_EXECUTABLE}" ]]; then
          eecho "missing ${ZETA_BASH_COLOR_HL}JQ_EXECUTABLE${ZETA_BASH_COLOR_NONE} env"
          wecho "please download JQ from ${ZETA_BASH_COLOR_HL}https://jqlang.github.io/jq/download/${ZETA_BASH_COLOR_NONE}"
          wecho "or install it: "
          wecho "  ${ZETA_BASH_COLOR_HL}winget install jqlang.jq${ZETA_BASH_COLOR_NONE}"
          return 1
        fi
      fi
    fi
  fi

  if [[ ! -x "$JQ_EXECUTABLE" ]]; then
    eecho "${ZETA_BASH_COLOR_HL}JQ_EXECUTABLE${ZETA_BASH_COLOR_NONE}=${ZETA_BASH_COLOR_HL}${JQ_EXECUTABLE}${ZETA_BASH_COLOR_NONE} is not executable."
    return 1
  fi
  export JQ_EXECUTABLE
  return 0
} # }}} [_zb_jq_init]

_zb_jq() {
  _zb_jq_init || return 1
  "${JQ_EXECUTABLE}" "$@"
}
