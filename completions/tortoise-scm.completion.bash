# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

complete -o default -o nospace -W 'logs log commit status update'           tsvn
complete -o default -o nospace -W 'logs log commit status settings setting' tgit tortoise t
