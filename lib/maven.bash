# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Determine the maven version
#
# @param storage where to store the value (a local variable from calling function)
# @return 0 if executable found; 1 otherwise.
_zb_mvn_version() { # {{{
  local -gn storage="$1"
  local actual_executable=''

  # not found
  if ! actual_executable=$(command -v mvn 2> /dev/null) || [[ -z "${actual_executable}" ]]; then
    storage=''
    return 1
  fi

  local refresh_indicator=''
  if _zb_prompt_updated ZETA_BASH_PS_MVN "${actual_executable}" ; then
    refresh_indicator='#'
  fi

  local version=''
  if [[ -z "${refresh_indicator}" ]]; then
    version="${ZETA_BASH_PS_MVN_VER:-}"
  else
    version=$("${actual_executable}" --batch-mode --version 2>&1 | head -n 1 | sed -E -r -e 's/^.*Apache Maven ([^ ]+) .+$/\1/g')
    if _zb_prompt_save -u ZETA_BASH_PS_MVN "${actual_executable}" "${version}" ; then
      _zb_env_session_save
    fi
    version="${refresh_indicator} ${version}"
  fi

  # shellcheck disable=SC2034  # this is a reference
  storage="${version}"
  return 0
} # }}} [_zb_mvn_version]

_zt_mvn_validate() {
  local mvn_home="$1"
  [[ -x "${mvn_home}/bin/mvn" ]]
}

#
# produce a set of path containing MVN
# each entries may end with a filename itself referenceing @VER for version
#
_zb_mvn_path_entries() {
  _zb_bt_analyze_entry environment 'MVN@_HOME' 'MAVEN@_HOME'
  for dir in "${ZETA_BASH_MAVEN_CONTAINER_PATH:-}" "${MAVEN_INSTALL_HOME:-}" ; do
    [[ -n "${dir}" && -d "${dir}" ]] && _zb_bt_analyze_entry directory "${dir}/apache-maven-@VER"
  done
}

_zb_mvn_switch() { # {{{
  local quiet=no
  local version=''
  for arg; do
    if [[ "${arg}" == --quiet ]]; then
      quiet=yes
    elif [[ -z "$version" ]]; then
      version="$arg"
    fi
  done

  if [[ -z "${version}" ]]; then
    version="${MAVEN_DEFAULT}"
    # don't issue an error if the default is not set
    [[ -z "${version}" ]] && return 1
  fi

  if ! _zt_utils_parse_semver "$version" ; then
    eecho "invalid maven version ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE} (not a semver)"
    return 1
  fi

  local mvn_home=''
  if ! _zb_bt_resolve_container mvn_home _zb_mvn_path_entries _zt_mvn_validate ; then
    eecho "could not resolve a maven home for version ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE}."
    return 1
  fi

  local mvn_opts=''
  local mvn_settings=''
  local mvn_profiles=''

  _zb_bt_expand_variable mvn_opts     'MAVEN@_OPTS'
  _zb_bt_expand_variable mvn_settings 'MAVEN@_SETTINGS'
  _zb_bt_expand_variable mvn_profiles 'MAVEN@_PROFILES'

  [[ -n "${MAVEN_GLOBAL_OPTS}" ]] && mvn_opts="${MAVEN_GLOBAL_OPTS} ${mvn_opts}"

  _zb_env_update_var MAVEN_OPTS  "${mvn_opts}"     #"${quiet}"
  _zb_env_update_var M2_SETTINGS "${mvn_settings}" #"${quiet}"
  _zb_env_update_var M2_PROFILES "${mvn_profiles}" #"${quiet}"
  if ! _zb_env_update_var "M2_HOME" "${mvn_home}" "${quiet}" ; then
    return 0
  fi

  export ZETA_BASH_MVN_EXECUTABLE="${mvn_home}/mvn"
  export ZETA_BASH_MVND_EXECUTABLE="${mvn_home}/mvnDebug"

  _zb_path_replace_app_home --app-name=maven "--quiet=$quiet" "--app-home=${mvn_home}" "--executable=mvn" --directories=_zb_mvn_path_entries

  # refresh the prompt, but don't reevaluate the version if empty
  if actual_executable=$(command -v mvn 2> /dev/null) ; then
    _zb_prompt_save ZETA_BASH_PS_MVN "${actual_executable}" '' || :
  fi
  _zb_env_session_save
  return 0
} # }}} [_zb_mvn_switch]
