. | first
  | . as $release
  | .binaries | first
  | {
    release_name:   @sh "\($release.release_name)",
    release_semver: @sh "\($release.version_data.semver)",
    file_name:      @sh "\(.package.name)",
    file_url:       @sh "\(.package.link)",
    file_checksum:  @sh "\(.package.checksum)"
  }
  | "#!/bin/bash
\($jq_release_name   )=\(.release_name)
\($jq_release_semver )=\(.release_semver)
\($jq_file_name      )=\(.file_name)
\($jq_file_url       )=\(.file_url)
\($jq_file_checksum  )=\(.file_checksum)
"
