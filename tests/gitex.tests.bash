#!/bin/bash


declare SCRIPT_FILE
if ! SCRIPT_FILE=$(realpath "$0"); then
  echo "realpath is skrewed !"
  exit 1
fi
declare SCRIPT_DIR="${SCRIPT_FILE%/*}"

export ZETA_BASH_HOME="${SCRIPT_DIR%/*}"
export ZETA_BASH_USER_HOME="${ZETA_BASH_HOME%/*}/zeta-bash-user"

# shellcheck source=tests/assertions.bash
source "${ZETA_BASH_HOME}/tests/assertions.bash"

# shellcheck source=bin/gitex-main
source "${ZETA_BASH_HOME}/bin/gitex-main"

# shellcheck disable=SC1090
if ! source "${ZETA_BASH_GITEX_CONF}" ; then
  eecho "something failed while sourcing ${ZETA_BASH_COLOR_HL}${ZETA_BASH_GITEX_CONF}${ZETA_BASH_COLOR_NONE}"
  return 1
fi

export ZETA_BASH_LOG_TIMED=0
export ZETA_BASH_LOG_LEVEL=debug

declare    expected_repo_last_index=0
declare -a expected_repo_names=()
declare -a expected_repo_git_dirs=()
declare -a expected_repo_working_trees=()
declare -a expected_repo_primary_groups=()
declare -a expected_repo_is_bare=()
declare -a expected_repo_remotes=()

declare -A expected_group_indexes=()
declare -A expected_repo_indexes=()

add_expected_repository() {
  local is_bare="$1"
  local name="$2"
  local group="$3"
  local working_tree="$4"

  # this is  to avoid shellcheck SC2178

  local git_dir="${working_tree}"
  if [[ "$is_bare" == 0 ]]; then
    git_dir+="/.git"
  fi

  # we cheat a little here to avoid problem with remote ordering
  local -a remotes=()
  mapfile -t remotes < <(git "--git-dir=${git_dir}" remote)

  local n=$expected_repo_last_index

  expected_repo_names[$n]="${name}"
  expected_repo_primary_groups[$n]="${group}"
  expected_repo_is_bare[$n]="$is_bare"
  expected_repo_working_trees[$n]="$working_tree"
  expected_repo_git_dirs[$n]="${git_dir}"
  expected_repo_remotes[$n]="${remotes[*]}"

  if [[ -z "${expected_group_indexes[$group]}" ]]; then
    expected_group_indexes[$group]="$n"
  else
    expected_group_indexes[$group]+=" $n"
  fi
  if [[ -z "${expected_repo_indexes[$name]}" ]]; then
    expected_repo_indexes[$name]="$n"
  else
    expected_repo_indexes[$name]+=" $n"
  fi
  (( ++expected_repo_last_index ))
}

cleanup() {
  if [[ -n "$TEST_TEMP_DIR" && -d "$TEST_TEMP_DIR" ]]; then
    iecho "cleanup temp directory ${TEST_TEMP_DIR}"
    rm -Rf "$TEST_TEMP_DIR"
  fi
}

setup() {
  local temp_dir=''
  if ! temp_dir=$(mktemp -d --suffix=_gitex ); then
    report_failure "could not create a temporary directory"
  fi

  export TEST_TEMP_DIR="$temp_dir"

  {
    local -a git_opts=( --quiet )
    pushd "$temp_dir" > /dev/null
      git init "${git_opts[@]}" --bare source.git
      git init  "${git_opts[@]}" "maker"
      pushd maker > /dev/null
        git remote add origin '../source.git'
        git checkout "${git_opts[@]}" -b master
        echo -n "FILE" > file
        git add .
        git commit "${git_opts[@]}" -m 'initial commit'
        git branch "${git_opts[@]}" develop master
        git push "${git_opts[@]}" origin develop master
      popd > /dev/null
      git clone "${git_opts[@]}" "source.git" "bare/bare-repo1.git"     --bare
      git clone "${git_opts[@]}" "source.git" "bare/bare-repo2.git"     --bare --config zb.group=g2 --config zb.name=g2-r1
      git clone "${git_opts[@]}" "source.git" "non-bare/non-bare-repo1"
      git --git-dir "non-bare/non-bare-repo1/.git" remote add mirror "bare/bare-repo2.git"
      git --git-dir "non-bare/non-bare-repo1/.git" remote add httperr "http://127.0.0.1:9647"
      pushd 'non-bare/non-bare-repo1'
        touch file_in_index file_untracked file_moved
        git add file_in_index file_moved
        git commit -m 'do something'
        echo 'test' > file_in_index
        git add file_in_index
        git mv file_moved file_moved_newname
      popd
      git clone "${git_opts[@]}" "source.git" "non-bare/non-bare-repo2" -b develop -o mirror --config zb.group=g2 --config zb.name=g2-r2
    popd > /dev/null
  } || report_failure "could not create git directories"

  export ZETA_BASH_GITEX_REPOPATH="${temp_dir}/bare=1:${temp_dir}/non-bare=1:"

  add_expected_repository no non-bare-repo1 non-bare   "${temp_dir}/non-bare/non-bare-repo1"
  add_expected_repository no g2-r2          g2         "${temp_dir}/non-bare/non-bare-repo2"
  add_expected_repository yes bare-repo1.git bare       "${temp_dir}/bare/bare-repo1.git"
  add_expected_repository yes g2-r1          g2         "${temp_dir}/bare/bare-repo2.git"
}

test_gitex_conf() {
  _zb_gitex_conf_save "${ZETA_BASH_GITEX_CONF}" || report_failure "_zb_gitex_conf_save failed"

  assert_array_equals gitex_repo_names         "${expected_repo_names[@]}"
  assert_array_equals gitex_repo_git_dirs      "${expected_repo_git_dirs[@]}"
  assert_array_equals gitex_repo_working_trees "${expected_repo_working_trees[@]}"
  assert_array_equals gitex_repo_primary_group "${expected_repo_primary_groups[@]}"
  assert_array_equals gitex_repo_is_bare       "${expected_repo_is_bare[@]}"
  assert_array_equals gitex_repo_remotes       "${expected_repo_remotes[@]}"


  local -a expected_gitex_group_indexes
  local -a expected_gitex_repo_indexes

  hash_to_array expected_group_indexes expected_gitex_group_indexes
  hash_to_array expected_repo_indexes  expected_gitex_repo_indexes

  assert_map_equals gitex_group_indexes "${expected_gitex_group_indexes[@]}"
  assert_map_equals gitex_repo_indexes  "${expected_gitex_repo_indexes[@]}"
}

test_gitex_goto() {
  local repo1_name="${expected_repo_names[0]}"
  local repo1_dir="${expected_repo_working_trees[0]}"
  local repo2_name="${expected_repo_names[1]}"
  local repo2_dir="${expected_repo_working_trees[1]}"


  _zb_gitex_goto "${repo1_name}" || report_failure "_zb_gitex_goto failed"
  assert_equals "PWD should be %expected" "$PWD" "${repo1_dir}"

  _zb_gitex_goto "${repo2_name}" || report_failure "_zb_gitex_goto failed"
  assert_equals "PWD should be %expected" "$PWD" "${repo2_dir}"

  ! _zb_gitex_goto "${repo1_name}" "${repo2_name}" || report_failure "_zb_gitex_goto must fail"
  assert_equals "PWD should be %expected" "$PWD" "${repo2_dir}"

  # this case is when you TAB
  _zb_gitex_goto "${repo2_name}/" || report_failure "_zb_gitex_goto failed"
  assert_equals "PWD should be %expected" "$PWD" "${repo2_dir}"

  hook() { return 1 ; }
  ZETA_BASH_GITEX_HOOK_GOTO=hook
  _zb_gitex_goto "${repo1_name}" && report_failure "_zb_gitex_goto should have failed"
  assert_not_equals "PWD should still be %expected" "$PWD" "${repo2_dir}"

  hook() {
    assert_equals "bare"          "${expected_repo_is_bare[0]}"       "$1"
    assert_equals "repo name"     "${expected_repo_names[0]}"         "$2"
    assert_equals "git_dir"       "${expected_repo_git_dirs[0]}"      "$3"
    assert_equals "working_tree"  "${expected_repo_working_trees[0]}" "$4"
    return 0 ;
  }
  _zb_gitex_goto "${repo1_name}" || report_failure "_zb_gitex_goto have failed"
  assert_equals "PWD should be %expected" "$PWD" "${repo1_dir}"
}

test_gitex_print() {
  declare -a gitex_print_result=()

  # shellcheck disable=SC2034
  mapfile -t gitex_print_result < <(_zb_gitex_print --git-dir "${expected_repo_names[@]}")  || \
    report_failure "_zb_gitex_print have failed"
  assert_array_equals gitex_print_result "${expected_repo_git_dirs[@]}"
}

test_gitex_list() {
  local -a expected=()
  expected+=( 'group bare' )
  expected+=( '  01. bare-repo1.git      ♪ (origin)' )
  expected+=( 'group g2' )
  expected+=( '  01. g2-r1               ♪ (origin)' )
  expected+=( '  02. g2-r2               ⁜ (mirror)' )
  expected+=( 'group non-bare' )
  expected+=( '  01. non-bare-repo1      ⁜ (httperr, mirror, origin)' )

  assert_command_output _zb_gitex_list --no-color --all -- "${expected[@]}"

  local -a expected=()
  expected+=( 'group g2' )
  expected+=( '  01. g2-r1               ♪ (origin)' )
  expected+=( '  02. g2-r2               ⁜ (mirror)' )
  assert_command_output _zb_gitex_list --no-color @g2 -- "${expected[@]}"

  local -a expected=()
  expected+=( 'group g2' )
  expected+=( '  01. g2-r2                (mirror)' )
  expected+=( 'group non-bare' )
  expected+=( '  01. non-bare-repo1       (httperr, mirror, origin)' )
  assert_command_output _zb_gitex_list --no-color g2-r2 non-bare-repo1 -- "${expected[@]}"
}

test_gitex_status() {
  # try adding this repo
  _zb_gitex_repo_add "${ZETA_BASH_HOME}/.git" 0
  _zb_gitex_repo_add "/e/git/external/github/formatter-maven-plugin/.git" 0
  cecho "_zb_gitex_status -R -A" && _zb_gitex_status -R -A
  cecho "_zb_gitex_status -R"    && _zb_gitex_status -R
  cecho "_zb_gitex_status"       && _zb_gitex_status
  # try with
  #export ZETA_BASH_GITEX_REPOPATH=/e/git=3:/e/apps/data/jedit=0
  #_zb_gitex_main --refresh status
}

test_gitex_fecth() {
  _zb_gitex_fetch @g2 --prune
  _zb_gitex_fetch non-bare-repo1 --remote=invalid --remote=a
  #
  _zb_gitex_fetch --all --remote=httperr
}

test_gitex_rebase() {
  _zb_gitex_rebase --no-confirm non-bare-repo1 --pull
  _zb_gitex_rebase --no-confirm non-bare-repo1 --remote=invalid
}

declare -a tests=()
tests+=( 'test_gitex_conf'  )
#tests+=( 'test_gitex_goto'  )
#tests+=( 'test_gitex_print' )
#tests+=( 'test_gitex_list'  )
tests+=( 'test_gitex_status'  )
# tests+=( 'test_gitex_help'  )
# tests+=( 'test_gitex_rebase'  )


trap cleanup EXIT
setup && run_tests "$SCRIPT_DIR" "${tests[@]}"


#_zb_gitex_main --refresh goto zeta-bash




