# Git Extension

## Description

**Git Extension** is a set of script to works with git repositories, mainly to switch from/to one repository to another
and to display status as an overview (eg: instead of going into each repository working tree and checking status for
each).

## Usage

The alias `g` is normally set to the `_zb_gitex_main` function, which in turn invoke `${ZETA_BASH_HOME}/bin/gitex-main`.

The alias `bun` should normally invoke `_zb_gitex_main` with `bundle` as command.

See `g --help` for up to date usage.

## Environment

`ZETA_BASH_GITEX_REPOPATH`

List of path separated by `:`, each pointing to a directory containing git non-bare repositories.

The `find` command is used to look up for `.git` directories. It can be further optimized (or restricted)
by appending a `=N` (max depth) at the path. It will be the same as invoking `find` like this:

```shell
find "${start}" -type d -name .git -maxdepth N
```

`ZETA_BASH_GITEX_BARE_REPOPATH`

List of bare repositories path. Same format than `ZETA_BASH_GITEX_REPOPATH`.

Like `ZETA_BASH_GITEX_REPOPATH`, `find` is used to look for git bare repositories; however, because there are
no clear marker as to _what is a git bare repositories_ (except for invoking `git`), the extension will
use `find` like this:

```shell
find "${start}" -type f -path '*/config' -maxdepth N
```

Bare repositories should be restricted to a specific path.

`ZETA_BASH_GITEX_CONF`

Where to store the _git extension_ configuration.

The default value is: `${ZETA_BASH_USER_CONFIG}/.gitex.bash`

`ZETA_BASH_BUNDLE_HOME`

From where bundle are imported or exported.

`ZETA_BASH_BUNDLE_DEFAULT_TARGET`

Default target to use for bundles. Always upper cased.

`ZETA_BASH_BUNDLE_DEFAULT_SOURCE`

Default source to use for bundles. Always upper cased.

You generally won't have to set this one: it default to `HOSTNAME`.

`ZETA_BASH_GITEX_LOADED`

Indicate that the configuration file `${ZETA_BASH_GITEX_CONF}` was loaded.

It can be unset to force reload the configuration (for example, after an `g --refresh`).

`ZETA_BASH_GITEX_HOOK_GOTO`

A command to be invoked right after entering a repository directory; the command should take the following
arg (in the given order):

- `bare`: a boolean which indicate if the repository is a bare repository (value != 0) or not (value=0);
- `repo`: the name of repository, for example, `zeta-bash`
- `git_dir`: the path to the git directory (where the `.git` is for non bare repositories)
- `working_tree`: the path to the content; for non bare directory, it is the directory containing the
  `.git` in the standard layout.

With the following function, we may use the relevant information to perform a set env: `_zb_env_reload` is
used instead of `env-reload` because it is alias.

```bash
_zbu_select_env() {
  local bare="$1"
  local repo="$2"
  local git_dir="$3"
  local working_tree="$4"

  local env='default'
  case "$working_tree" in
    */zeta-bash) env='zeta-bash' ;;
    */jdk6/*)    env=jdk6        ;;
    */jdk7/*)    env=jdk7        ;;
    */jdk8/*)    env=jdk8        ;;
    */jdk11/*)   env=jdk11      ;;
  esac

  # env-reload is an alias
  _zb_env_reload "$env"
}
export ZETA_BASH_GITEX_HOOK_GOTO=_zbu_select_env
```

For example:

```text
[23:25:14] /e/git [1.8.0_192]
$ env-reload
[23:26:33] /e/git [1.8.0_192]
$ g zeta-bash
[23:26:37] /e/git/zb/zeta-bash (develop) [1.8.0_192]
$ g jdk11-example
[23:26:45] /e/git/jdk11/jdk11-example (master) [# 11.0.1] [jdk11]
$ ...
```

As we can see, we switched to `jdk11` environment when switching to `jdk11-example`.

## Example of configuration

```shell
export ZETA_BASH_GITEX_REPOPATH="/e/git=2:/f/git"
export ZETA_BASH_GITEX_BARE_REPOPATH='/g/git-bare=2'
```

This will search repositories in `/e/git` up to two level and in `/f/git` to all tree.
Bare repositories will be scanned from `/g/git-bare`.

## Configuration file (version 2)

The configuration file is generated using bash `declare -p`. It defaults located is `${ZETA_BASH_USER_HOME}/.config/gitex.v2.bash`

You should have to edit it, but in case you need to: a Java like syntax is used for type, where `string[]` simply means
`declare -a`, and `string[string]` means `declare -A`.

- `int` `gitex_repo_index_seq` : Sequence of the next repository to be created.
- `string[]` `gitex_repo_names` : Indexed array of repo names; may contains duplicates names.
- `string[]` `gitex_repo_git_dirs` : Indexed array of path to git_dir
- `string[]` `gitex_repo_working_trees` : Indexed array of path to working tree;
- `string[]` `gitex_repo_git_dirs` : Indexed array of path to git_dir
- `bool[]` `gitex_repo_is_bare` : Indexed array of bare indicator
- `string[]` `gitex_repo_primary_group` : Primary group for a repository (used in `g --list` to group repository)
- `string[][]` `gitex_repo_remotes` : Space (or IFS) separated list of remotes contained in the repository
- `string[int:string]` `gitex_repo_disabled_remotes` : Indicate disabled remotes for a repository. The 'int:string'
   means `"$index:$remote"`
- `int[][string]` `gitex_group_indexes` : List of repository index for a group
- `int[][string]` `gitex_repo_indexes` : List of repository index for a repo name
- `string[int]` `gitex_bun_names` : Indicate the expected bundle name of a repository
- `ModeEnum[int]` `gitex_bun_modes` : Indicate the bundle mode (may be `both`, `import` or `export`)
- `int[int]` `gitex_bun_histories` : Indicate depth in history, when selected commits.

## Git Configuration

The user may optionally set this two configurations property in GIT:

- `zb.name`: name to be used for a repository (this change how you access the repository).
- `zb.group`: list of group to be used for a repository (this change how you reference the group).
- `zb-bun.name`: prefix of bundle generated for a given repository (see Git Bundle)
- `zb-bun.history`: default value for history of git bundle
- `zb-bun.mode`: bundle mode (both, import, export)
- `zb-remote.<remote>.offline`: indicate a remote that was disabled because `git fetch $remote` failed

## Git Bundle

The `g bundle --export` generate a bundle that can be used by `g bundle --import` after copying the necessary files to
the target location:

### Exporting

Assuming `ZETA_BASH_BUNDLE_HOME` exists and is defined as `/e/git/bundles`:

On one end, generate one or several bundle:

```bash
# from current directory (it will be added if absent from configuration)
g bundle
bun
# generate bundles for all exportable repositories, if there are some and forcing the source to be Foobar
g bundle --source-host-name "Foobar"  --exportable
bun      --source-host-name "Foobar"  --exportable

```

Due to how repositories selector flags (`--exportable`, `@expr`, etc) are processed, and default value of the command,
if `--exportable` repo selector does not select anything, the command will try to generate a bundle for the current
directory - eventually adding the parent directory.

The default value for `--source-host-name` is `${ZETA_BASH_BUNDLE_DEFAULT_SOURCE^^}`, then `${HOSTNAME^^}`, which should
produce the current `HOSTNAME` in upper case.

A new bundle will be generated into `/e/git/bundles/FOOBAR/glhez_zeta-bash-FOOBAR.bundle`.

The string `glhez_zeta-bash` correspond to the `zb-bun.name`: if this was not configured, a default will be generated
based on the remotes, `origin` being selected before any other remote.

When computing a default name, the following transformation are done:

- removal of user part (eg: `ssh://git@bitbucket.org` or `git@bitbucket.org`)
- scheme (`http://`, `https://`, `ssh://`) and `host:port` are removed
- `~` is removed
- ending `.git` are removed
- `:` and `/` are replaced by `_`

The transformation may not generate a name valid for the OS (eg: Windows).

In most case, it should work fine, however it would be best to enforce it.

### Importing

Assuming `ZETA_BASH_BUNDLE_HOME` exists and is defined as `/d/bundles`:

When the bundle is exported, and copied using any means (USB Device, SSH, FTP, Cloud Storage, ...), simply import them
like that (for this to work, you would typically copy or move the `FOOBAR` folder from the previous step
to `/d/bundles`):

```bash
# using default ZETA_BASH_BUNDLE_DEFAULT_TARGET if found
g bundle --import
# or
bun --import
# forcing the target
g bundle --import --target-host-name "FOOBAR"
# or
bun --import --target-host-name "FOOBAR"
```

The command will list all repositories where bundle are enabled (eg: `zb-bun.mode` is configured as `import` or `both`)
and for which a `zb-bun.name` was previously generated. In the example above, this means that we would look
for `/d/bundles/FOOBAR/glhez_zeta-bash-FOOBAR.bundle`.

At the end, you can then fetch all bundle using `g --fetch @bun`.

### Content of the bundle

For the moment, the `--all` is option is passed to [`git bundle create`][GIT-BUNDLE].

[GIT-BUNDLE]: https://git-scm.com/docs/git-bundle
