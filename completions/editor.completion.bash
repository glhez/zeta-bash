# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

declare vscode_options='
  -h --help -v --version -n --new-window -r --reuse-window -g --goto -d --diff -w --wait --locale
  --install-extension --uninstall-extension --disable-extensions --list-extensions --show-versions --enable-proposed-api --force
  --extensions-dir --user-data-dir -s --status -p --performance --disable-gpu --verbose --prof-startup --upload-logs
  --add
'

complete -o filenames -o default -o nospace -W "${vscode_options}" vscode codium code



