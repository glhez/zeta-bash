#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

declare previous_arg=

declare left_label=''
declare left_file=''

declare right_label=''
declare right_file=''

declare is_merge=0
declare use_kdiff3=0

echo $@ > /tmp/svn-diff-helper

for arg; do
  if [[ -n "${previous_arg}" ]]; then
    arg="${previous_arg}${arg}"
    previous_arg=''
  fi
  case "$arg" in
    -m)
      is_merge=1
    ;;
    -u)  : ;;
    -w)  : ;;
    -k)  use_kdiff3=1 ;;
    -L)  previous_arg="${arg}" ;;
    -L*)
      if   [[ -z "${left_label}"  ]]; then left_label="${arg:2}"  ;
      elif [[ -z "${right_label}" ]]; then right_label="${arg:2}" ;
      fi
    ;;
    *)
      if   [[ -z "${left_file}"  ]]; then left_file="${arg}"  ;
      elif [[ -z "${right_file}" ]]; then right_file="${arg}" ;
      fi
    ;;
  esac
done

if [[ $is_merge -eq 1 ]]; then
  echo $@
elif [[ $use_kdiff3 -eq 1 ]]; then
  "/c/Program Files/KDiff3/kdiff3.exe" "${left_file}" "${right_file}" --L1 "${left_label}" --L2 "${right_label}"
else
  git diff -w "${left_file}" "${right_file}"
fi
