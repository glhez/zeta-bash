#!/bin/bash
# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/os.bash
source "${ZETA_BASH_HOME}/lib/os.bash" && _zb_determine_os

if [[ "${ZETA_BASH_OS}" != windows && -z "$WSL_DISTRO_NAME" ]]; then
  eecho "this script does not work on ${ZETA_BASH_COLOR_HL}${ZETA_BASH_OS}${ZETA_BASH_COLOR_NONE}"
  eecho "and distrib was not seen as WSL."
  exit 1
fi

main() {
  local scm_type="$1"
  shift

  if [[ "$scm_type" != "git" && "$scm_type" != "svn" && "$scm_type" != auto ]]; then
    eecho "wrong SCM type [${ZETA_BASH_COLOR_HL}${scm_type}${ZETA_BASH_COLOR_NONE}], use [${ZETA_BASH_COLOR_HL}git${ZETA_BASH_COLOR_NONE}], [${ZETA_BASH_COLOR_HL}svn${ZETA_BASH_COLOR_NONE}] or [${ZETA_BASH_COLOR_HL}auto${ZETA_BASH_COLOR_NONE}]"
    return 3
  fi

  local -a args=()
  for arg; do
    case "$arg" in
      -[a-zA-Z0-9]|--*)
        eecho "invalid option ${ZETA_BASH_COLOR_HL}${arg}${ZETA_BASH_COLOR_NONE}"
        return 2
      ;;
      *) args+=( "$arg" ) ;;
    esac
  done

  # lookup for real repo path
  local relpath="${args[1]:-$PWD}"
  local path=""

  # don't bother trying to call realpath
  if ! path=$(realpath "$relpath") ; then
    eecho "could not resolve realpath of ${ZETA_BASH_COLOR_HL}${relpath}${ZETA_BASH_COLOR_NONE}."
    return 2
  fi

  local scm_repository_path="${path}"
  local found=no
  if [[ "$scm_type" = auto ]]; then
    while [[ -n "${scm_repository_path}" ]]; do
      for scm_known_type in git svn ; do
        local scm_dir_path="${scm_repository_path}/.${scm_known_type}"
        if [[ "${scm_known_type}" == git && -f "${scm_dir_path}" || -d "${scm_dir_path}" ]]; then
          iecho "found ${ZETA_BASH_COLOR_HL}${scm_known_type}${ZETA_BASH_COLOR_NONE} SCM file/directory in ${ZETA_BASH_COLOR_HL}${scm_repository_path}${ZETA_BASH_COLOR_NONE}"
          scm_type="${scm_known_type}"
          found=yes
          break
        fi
      done
      scm_repository_path="${scm_repository_path%/*}"
    done
  else
    while [[ -n "${scm_repository_path}" ]]; do
      local scm_dir_path="${scm_repository_path}/.${scm_type}"
      if [[ "${scm_type}" == git && -f "${scm_dir_path}" || -d "${scm_dir_path}" ]]; then
        found=yes
        break
      fi
      scm_repository_path="${scm_repository_path%/*}"
    done
  fi

  if [[ "${found}" == no ]]; then
    eecho "the path ${ZETA_BASH_COLOR_HL}${relpath}${ZETA_BASH_COLOR_NONE} does not appear to be a ${ZETA_BASH_COLOR_HL}${scm_type}${ZETA_BASH_COLOR_NONE} repository."
    iecho "tried from absolute path: ${ZETA_BASH_COLOR_HL}${path}${ZETA_BASH_COLOR_NONE}."
    return 2
  fi

  local command="${args[0]}"
  if [[ -z "$command" ]]; then
    command=status
  fi

  # commands:
  #   https://tortoisesvn.net/docs/release/TortoiseSVN_en/tsvn-automation.html
  #   https://tortoisegit.org/docs/tortoisegit/tgit-automation.html#tgit-automation-basics
  # a '-' means: use the command.
  local -A aliases
  aliases[git:setting]=settings
  aliases[git:settings]=-
  aliases[svn:update]=-
  aliases[commit]=-
  aliases[log]=-
  aliases[logs]=log
  aliases[status]=repostatus

  # get the proc command.
  local tortoise_proc_command=''
  for alias in "$scm_type:$command" "$command"; do
    tortoise_proc_command="${aliases[$alias]}"
    if [[ -n "$tortoise_proc_command" ]]; then
      if [[ "$tortoise_proc_command" == '-' ]]; then
        tortoise_proc_command="$command"
      fi
      break
    fi
  done
  if [[ -z "$tortoise_proc_command" ]]; then
    eecho "command not supported: ${ZETA_BASH_COLOR_HL}${command}${ZETA_BASH_COLOR_NONE}."
    return 1
  fi

  # localize tortoise based on current scm
  local tortoise_name=-
  local tortoise_proc_name=-
  local tortoise_proc_path=-

  case "$scm_type" in
    git)
      tortoise_name=TortoiseGit
      tortoise_proc_name=TortoiseGitProc
    ;;
    svn)
      tortoise_name=TortoiseSVN
      tortoise_proc_name=TortoiseProc
    ;;
  esac
  tortoise_proc_name+=.exe

  # lookup for the Tortoise*Proc
  local -a possible_directories=()
  _zb_wsl_prefix possible_directories "/c/Program Files/${tortoise_name}/bin" \
                                      "/c/Program Files (x86)/${tortoise_name}/bin"
  for possible_directory in "${possible_directories[@]}" ; do
    tortoise_proc_path="${possible_directory}/${tortoise_proc_name}"
    if [[ -f "${tortoise_proc_path}" ]]; then
      break
    fi
    tortoise_proc_path=''
  done

  if [[ -z "${tortoise_proc_path}" ]]; then
    eecho "unable to find ${ZETA_BASH_COLOR_HL}${tortoise_proc_name}${ZETA_BASH_COLOR_NONE}."
    return 3
  fi

  _zb_utils_gui_exec "${tortoise_proc_path}" "/command:${tortoise_proc_command}" "/path:$(_zb_win_path2 "${path}")"
}

main "$@"
