#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"

#
# Test unicode font.
#
iecho "Zeta Bash unicode tester."
iecho ""
iecho "  This tools will print unicode character from a range."
iecho ""

declare -i ustart uend

declare -i errors=0
if [[ $# -eq 2 ]]; then
  iecho "reading options from args..."
  ustart="$1" 1> /dev/null 2>&1;
  uend="$2"   1> /dev/null 2>&1;
  if [[ "${ustart}" -eq 0 ]]; then eecho "invalid start range (${ZETA_BASH_COLOR_HL}$1${ZETA_BASH_COLOR_NONE}) !"; (( ++errors )); fi
  if [[ "${uend}"   -eq 0 ]]; then eecho "invalid end range (${ZETA_BASH_COLOR_HL}$2${ZETA_BASH_COLOR_NONE}) !"  ; (( ++errors ));  fi
else
  read -r -e -p 'Please enter start range (0x...) : ' ustart
  read -r -e -p 'Please enter end range (0x...)   : ' uend

  if [[ "${ustart}" -eq 0 ]]; then eecho "invalid start range !"; (( ++errors )); fi
  if [[ "${uend}"   -eq 0 ]]; then eecho "invalid end range !"  ; (( ++errors )); fi
fi

if [[ "${errors}" -eq 0 && "${ustart}" -gt "${uend}" ]]; then
  eecho "invalid range: start (${ZETA_BASH_COLOR_HL}${ustart}${ZETA_BASH_COLOR_NONE}) > end (${ZETA_BASH_COLOR_HL}${uend}${ZETA_BASH_COLOR_NONE})."
  (( ++errors ))
fi

if [[ $errors -gt 0 ]]; then
  exit 1
fi

declare -i start=${ustart}
declare -i end=${uend}
declare -i limit=8

iecho "$(printf "displaying ${ZETA_BASH_COLOR_HL}0x%04X${ZETA_BASH_COLOR_NONE} to ${ZETA_BASH_COLOR_HL}0x%04X${ZETA_BASH_COLOR_NONE}" "$start" "$end")"
for (( i = start, j = limit - 1; i < end ; ++i, --j )); do
  printf "0x%04X: \e[43;30m\\u$(printf '%04X' "$i")${ZETA_BASH_COLOR_NONE} " "$i"
  if [[ $j -eq 0 ]]; then
    echo
    j=$limit
  fi
done
if [[ $j -ge 0 ]]; then
  echo
fi
echo '-'
