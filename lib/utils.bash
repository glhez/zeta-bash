# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#


#
# utilities
#
declare ZETA_BASH_WRAP_ANSI_EC="\r\033[K"

# in case set -u is under effect.
[[ ! -v ZETA_BASH_LOG_LEVEL   ]] && declare ZETA_BASH_LOG_LEVEL='info'
[[ ! -v ZETA_BASH_LOG_TIMED   ]] && declare ZETA_BASH_LOG_TIMED=0
[[ ! -v ZETA_BASH_LOG_PREFIX  ]] && declare ZETA_BASH_LOG_PREFIX=''
[[ ! -v ZETA_BASH_LOG_ESCAPE  ]] && declare ZETA_BASH_LOG_ESCAPE=0

# color constants to be used
_zb_color_on() {
  export ZETA_BASH_COLORS_ENABLED=yes

  export ZETA_BASH_COLOR_RED="\e[1;31m"              # red
  export ZETA_BASH_COLOR_GREEN="\e[1;32m"            # green
  export ZETA_BASH_COLOR_NONE="\e[0m"                # none
  export ZETA_BASH_COLOR_CYAN="\e[1;36m"             # cyan
  export ZETA_BASH_COLOR_BLUE="\e[1;34m"             # some kind of blue
  export ZETA_BASH_COLOR_YELLOW="\e[1;33m"           # yellow
  export ZETA_BASH_COLOR_PURPLE="\e[1;35m"           # purple
  export ZETA_BASH_COLOR_ORANGE="\e[38;2;255;128;0m" # orange (require xterm 256 colored shell)
  export ZETA_BASH_COLOR_HL="\e[1m"                  # highlight
}

_zb_color_off() {
  export ZETA_BASH_COLORS_ENABLED=no
  export ZETA_BASH_COLOR_RED=''       # red
  export ZETA_BASH_COLOR_GREEN=''     # green
  export ZETA_BASH_COLOR_NONE=''      # none
  export ZETA_BASH_COLOR_CYAN=''      # cyan
  export ZETA_BASH_COLOR_BLUE=''      # some kind of blue
  export ZETA_BASH_COLOR_YELLOW=''    # yellow
  export ZETA_BASH_COLOR_PURPLE=''    # purple
  export ZETA_BASH_COLOR_ORANGE=''    # orange (require xterm 256 colored shell)
  export ZETA_BASH_COLOR_HL=''        # highlight
}

export -a ZETA_BASH_COLORS_PARSED_ARGS=()

_zb_color_init() {
  local color_mode=auto
  local prev_arg=''
  local parse_error=0
  ZETA_BASH_COLORS_PARSED_ARGS=()
  for arg; do
    if [[ -n "$prev_arg" ]]; then
      arg="${prev_arg}=${arg}"
      prev_arg=''
    fi
    local token=''
    local read_token=no
    case "$arg" in
      --color|--colour)   prev_arg="$arg" ;;
      --color=*)          token="${arg##'--color='}"  ; read_token=yes ;;
      --colour=*)         token="${arg##'--colour='}" ; read_token=yes ;;
      *) ZETA_BASH_COLORS_PARSED_ARGS+=( "$arg" ) ;;
    esac

    if [[ "${read_token}" == yes ]]; then
      case "${token}" in
        auto)               color_mode=auto   ;;
        always|on|true|1)   color_mode=always ;;
        never|off|false|0)  color_mode=never  ;;
        *)
          echo "invalid color token ${token}: always, never or auto expected." 1>&2
          parse_error=1
        ;;
      esac
    fi
  done

  case "${color_mode}" in
    always) _zb_color_on ;;
    never)  _zb_color_off ;;
    auto)
      # https://stackoverflow.com/questions/911168/how-can-i-detect-if-my-shell-script-is-running-through-a-pipe
      if [[ -t 1 ]]; then
        _zb_color_on
      else
        _zb_color_off
      fi
    ;;
  esac
  return $parse_error
}


_zb_log_enabled() {
  local level="$1"
  case "$level:${ZETA_BASH_LOG_LEVEL:-info}" in
    debug:debug)          return 0 ;;
    warn:error)           return 1 ;;
    info:warning|info:warn|info:error) return 1 ;;
    debug:*)              return 1 ;;
    *)                    return 0 ;;
  esac
}

_zb_log() {
  local wrap=$1
  local level=$2
  local level_str=$3
  local color=$4
  ! _zb_log_enabled "$level" && return 0

  shift 4

  [[ "$wrap" == yes && "${ZETA_BASH_WRAP:-on}" == off ]] && wrap=no

  # note: this work only in the context of the same shell
  if [[ "$wrap" == no && "${ZETA_BASH_WRAPPED:-0}" == 1 ]]; then
    echo -en "${ZETA_BASH_WRAP_ANSI_EC}" 1>&2
    export ZETA_BASH_WRAPPED=0
  fi

  local prefix=''
  [[ "$wrap" == yes              ]] && prefix+="${ZETA_BASH_WRAP_ANSI_EC}"
  if [[ "${ZETA_BASH_LOG_TIMED:-0}" == 1 ]] ; then
    date_prefix=''
    local epoch_real_time="${EPOCHREALTIME:-}"
    local seconds=-1
    local micros=0
    if [[ "${epoch_real_time}" =~ ^0*(0|[1-9][0-9]*)[.,]0*(0|[1-9][0-9]*)$ ]]; then
      seconds="${BASH_REMATCH[1]}"
      micros="${BASH_REMATCH[2]}"
      micros=$(( micros / 1000 )) # 10^6 -> 10^3
      printf -v date_prefix '%(%T)T.%03d' "${seconds}" "${micros}"
    else
      printf -v date_prefix '%(%T)T'
    fi
    prefix+="${ZETA_BASH_COLOR_ORANGE}${date_prefix}${ZETA_BASH_COLOR_NONE} | "
  fi
  [[ -n "${ZETA_BASH_LOG_PREFIX:-}"  ]] && prefix+="++ [${ZETA_BASH_COLOR_PURPLE}${ZETA_BASH_LOG_PREFIX}${ZETA_BASH_COLOR_NONE}] "
  prefix+="${color}${level_str}|${ZETA_BASH_COLOR_NONE} "

  if [[ "${ZETA_BASH_LOG_ESCAPE:-0}" == 1 ]]; then
    local -a eopts=()
    echo -en "${prefix}" 1>&2
    [[ "$wrap" == yes ]] && eopts+=( -n )
    echo "${eopts[@]}" "$*" 1>&2
  elif [[ "$wrap" == yes ]]; then
    echo -en "${prefix}$*" 1>&2
  else
    echo -e "${prefix}$*" 1>&2
  fi

  if [[ "$wrap" == yes ]]; then
    if [[ "${ZETA_BASH_LOG_FORCE_WRAP:-0}" == 1 ]] ; then
      echo '' 1>&2
      export ZETA_BASH_WRAPPED=0
    else
      export ZETA_BASH_WRAPPED=1
    fi
  fi
  return 0
}

# do the same as echo, but on stderr
eecho() { _zb_log no   error 'error' "${ZETA_BASH_COLOR_RED}"    "$@"; }
wecho() { _zb_log no   warn  'warn ' "${ZETA_BASH_COLOR_YELLOW}" "$@"; }
iecho() { _zb_log no   info  'info ' "${ZETA_BASH_COLOR_CYAN}"   "$@"; }
cecho() { _zb_log no   info  'cmd  ' "${ZETA_BASH_COLOR_PURPLE}" "$@"; }
decho() { _zb_log no   debug 'debug' "${ZETA_BASH_COLOR_GREEN}"  "$@"; }

ewrap() { _zb_log yes error 'error' "${ZETA_BASH_COLOR_RED}"    "$@"; }
wwrap() { _zb_log yes warn  'warn ' "${ZETA_BASH_COLOR_YELLOW}" "$@"; }
iwrap() { _zb_log yes info  'info ' "${ZETA_BASH_COLOR_CYAN}"   "$@"; }
cwrap() { _zb_log yes info  'cmd  ' "${ZETA_BASH_COLOR_PURPLE}" "$@"; }
dwrap() { _zb_log yes debug 'debug' "${ZETA_BASH_COLOR_GREEN}"  "$@"; }

#
# Print the stacktrace of the current executing shell.
#
# @param message a message to print (in case of a natural error)
_zb_utils_print_stacktrace() {
  _zb_utils_print_stacktrace2 eecho "$@"
}

_zb_utils_print_stacktrace2() {
  local handler="$1"
  local message="$2"

  local cs="${ZETA_BASH_COLOR_YELLOW}"
  local cl="${ZETA_BASH_COLOR_YELLOW}"
  local cf="${ZETA_BASH_COLOR_HL}"
  local cn="${ZETA_BASH_COLOR_NONE}"
  # we start at one, to ignore this function

  local start=1
  local function="${FUNCNAME[1]}"
  if [[ "${function}" == _zb_utils_print_stacktrace ]]; then
    start=2
    function="${FUNCNAME[2]}"
  fi

  if [[ -z "${message}" ]]; then
    message="stacktrace for function ${ZETA_BASH_COLOR_HL}${function}${ZETA_BASH_COLOR_NONE}"
  fi

  "$handler" "${message}"
  for (( i = start, j = i - 1, n = ${#FUNCNAME[@]}; i < n; ++i,++j )); do
    local function="${FUNCNAME[$i]}"
    local file=${BASH_SOURCE[$i]}
    local line="${BASH_LINENO[$j]}"
    if [[ "${ZETA_BASH_OS}" == windows ]]; then
      file=$(cygpath -m "$file")
    fi
    "$handler" "  at ${cf}${function}${cn} (${cs}${file}${cn}:${cl}${line}${cn})"
  done
  return 0
}

#
# Leftpad a value with padwith until length is reached.
#
# @param varname variable to be padded
# @param length length to pad
# @param padwith a string to pad with; default to space ' '
#
_zb_utils_pad_left_in_place() {
  local varname=$1
  local length=$2
  local padwith="${3:- }"

  local -n value="${varname}"
  local padded="${value}"
  while (( "${#padded}" < length )); do
    padded="${padwith}${padded}"
  done
  value="${padded}"
}

#
# Rightpad a value with padwith until length is reached.
#
# @param varname variable to be padded
# @param length length to pad
# @param padwith a string to pad with; default to space ' '
# @param initial_length length of value, if it contains some invisible characters such as escape code
#
_zb_utils_pad_right_in_place() {
  local varname=$1
  local length=$2
  local padwith="${3:- }"
  local padwith_length="${#padwith}"
  local initial_length="${4}"

  local -n value="${varname}"
  local padded="${value}"

  [[ -z "${initial_length}" ]] && initial_length="${#padded}"

  while (( initial_length < length )); do
    padded+="${padwith}"
    (( initial_length+=padwith_length ))
  done
  value="${padded}"
}

#
# Check that one function exists.
#
# @param fn function name
# @return 0 if function exists.
#
_zb_utils_function_exists() {
  declare -F "$1" > /dev/null
  return $?
}

#
# Execute a gui program by detaching said program from the shell
#
# This ensure that jedit and vscode opens in detached mode.
#
# @param program... program and args
#
_zb_utils_gui_exec() {
  if [[ "$ZETA_BASH_OS" = 'windows' ]]; then
    # will support ZETA_BASH_EDITOR_FILELIST for us
    # will also support ZETA_BASH_EDITOR_FILELIST_SEPARATOR
    "${ZETA_BASH_BIN_WINVOKE}" "$@"
  else
    if [[ -n "${ZETA_BASH_EDITOR_FILELIST}" ]]; then
      xargs --arg-file "${ZETA_BASH_EDITOR_FILELIST}" -0 "$@" "${ZETA_BASH_EDITOR_FILELIST_SEPARATOR}" </dev/null >/dev/null 2>&1 &
    else
      "$@" </dev/null >/dev/null 2>&1 &
    fi
    disown $!
  fi
}

#
# Change the current terminal title
#
# We use ANSI Escape code or a ConEmu macro if ConEmu is here.
#
# @see https://www.tldp.org/HOWTO/Xterm-Title-3.html
# @see https://conemu.github.io/en/AnsiEscapeCodes.html
#
_zb_utils_set_term_title() {
  local title="$1"
  # shellcheck disable=SC2154
  if [[ -n "$ConEmuANSI" ]] && command -v ConEmuC.exe > /dev/null ; then
    MSYS_NO_PATHCONV=1 cmd /C "ConEmuC.exe /GUIMACRO Rename(0,\"${title}\")" > /dev/null
  else
    echo  -e "\e]0;${title}\x07"
  fi
}

#
# Return max length of args
#
# @param -h hash    arg are keys and use an indirection with a hash
# @param arg... value to check
#
_zb_utils_maxlen() {
  local flag
  local OPTIND=1
  while getopts 'h:' flag "$@" ; do
    case "$flag" in
      h) hash_name="$OPTARG" ;;
      *) _zb_utils_print_stacktrace "unsupported flag: ${flag} ${OPTIND}" && return 1 ;;
    esac
  done
  shift $((OPTIND-1))

  local arg=''
  local max_length=0
  if [[ -z "$hash_name" ]]; then
    for arg; do
      [[ "${#arg}" -gt $max_length ]] && max_length="${#arg}"
    done
  else
    for arg; do
      local value=''
      eval "value=\${${hash_name}[\"\${arg}\"]}"
      [[ "${#value}" -gt $max_length ]] && max_length="${#value}"
    done
  fi
  echo "${max_length}"
}

_zt_utils_parse_semver() {
  local version="$1"
  declare -gA ZETA_BASH_SEMVER=()
  if [[ ! "${version}" =~ ^([0-9]+)(\.[0-9]+)?(\.[0-9]+)?(\.[0-9]+)?$ ]]; then
    return 1
  fi
  local semver_major="${BASH_REMATCH[1]}"
  local semver_minor="${BASH_REMATCH[2]}"
  local semver_patch="${BASH_REMATCH[3]}"
  if [[ -n "${semver_minor}" ]]; then semver_minor="${semver_minor:1}" ; else semver_minor='' ; fi
  if [[ -n "${semver_patch}" ]]; then semver_patch="${semver_patch:1}" ; else semver_patch='' ; fi

  # invalid semver
  if [[ -n "${semver_patch}" && -z "${semver_minor}" ]]; then return 1 ; fi

  declare -xgA ZETA_BASH_SEMVER=( [major]="$semver_major" [minor]="$semver_minor" [patch]="$semver_patch" )
  return 0
}

#
# on windows, we don't have uuid, we declare one suitable here
#
if [[ "${ZETA_BASH_OS}" == windows ]] && ! declare -f uuid > /dev/null ; then
  uuid() {
    powershell -Command "[guid]::NewGuid().ToString()"
  }
fi

_zb_utils_is_debug_enabled() {
  [[ "${ZETA_BASH_DEBUG:-0}" == 1 || "${ZETA_BASH_LOG_LEVEL:-info}" == debug ]]
}

_zb_utils_epoch_seconds() {
  local epoch_seconds=''
  local epoch_seconds_supported=no
  # because we use '-n', this
  case "${BASH_VERSION}" in
    0.*|1.*|2.*|3.*|4.0.*|4.1.*)  eecho "version ${BASH_VERSION} of bash not supported." ; return 1 ;;
    4.*)
      printf -v epoch_seconds '%(%s)T' -1
    ;;
    5.*)
      epoch_seconds_supported=yes
      epoch_seconds="${EPOCHSECONDS}"
    ;;
  esac
  if [[ -n "$1" ]]; then
    local -n _____varname="$1"
    _____varname="${epoch_seconds}"
  elif [[ "${epoch_seconds_supported}" == no ]]; then
    EPOCHSECONDS="${epoch_seconds}"
  fi
}

_zb_utils_is_not_empty() {
  local what="$1"
  local exit_code="${2:-}"
  local -n value_of_what="$what"
  if [[ -z "${value_of_what:-}" ]]; then
    if [[ -n "${exit_code}" ]]; then
      eecho "missing ${what}"
      exit "$exit_code"
    fi
    return 1
  fi
  return 0
}

_zb_utils_is_executable() {
  local what="$1"
  local path_to_executable="$2"
  local exit_code="${3:-}"
  local -n value_of_what="$what"
  if [[ ! -x "${value_of_what}/${path_to_executable}" ]]; then
    if [[ -n "${exit_code}" ]] ; then
      eecho "invalid ${what} (${path_to_executable} is not executable)"
      exit "$exit_code"
    fi
    return 1
  fi
  return 0
}

#
# Create symlink, eventually using elevated process on Windows
#
#
# linux:
#   ln -s TARGET LINKNAME
# windows
#   use powershell
#
# @param target link target
# @param linkname link name
#
_zb_utils_ln() {
  local target="$1"
  local linkname="$2"
  if [[ "${ZETA_BASH_OS}" != windows ]]; then
    if ! ln -s "${target}" "${linkname}" ; then
      return 2
    fi
    return 0
  fi

  local symlink_file=''
  if ! symlink_file="$(mktemp mklink.XXXXXXXXXX --suffix=.cmd)" ; then
    eecho "could not create temp file to avoid PowerShell reading too much."
    rm -f "${symlink_file}"
    return 2
  fi

  local symlink_share_dir="${ZETA_BASH_HOME}/share/create-link"
  local symlink_header_file="${symlink_share_dir}/create-link.header.cmd"
  local symlink_footer_file="${symlink_share_dir}/create-link.footer.cmd"

  local cygpath_link='' cygpath_target='' cygpath_symlink_file=''
  if ! cygpath_symlink_file="$(cygpath -w "${symlink_file}")" ; then cygpath_symlink_file="${symlink_file}" ; fi
  if !         cygpath_link="$(cygpath -w "${linkname}"    )" ; then cygpath_link="${linkname}"             ; fi
  if !       cygpath_target="$(cygpath -w "${target}"      )" ; then cygpath_target="${target}"             ; fi
  {
    cat "${symlink_header_file}"
    echo "  call :symbolic \"${cygpath_link}\" \"${cygpath_target}\""
    cat "${symlink_footer_file}"
  } | unix2dos -q > "${symlink_file}"

  local -a command_opts=( "Start-Process -Wait -Verb RunAs '${cygpath_symlink_file}'" )
  local failed=no failed_cause=''
  if ! powershell "${command_opts[@]}" ; then
    failed=yes
    failed_cause=powershell
  fi
  if [[ "${failed}" == no && ! -L "${linkname}" ]]; then
    failed=yes
    failed_cause='not a symlink'
  fi
  if [[ "${failed}" == no && ! "${linkname}" -ef "${target}" ]]; then
    failed=yes
    failed_cause='link does not point to target'
  fi
  if [[ "${failed}" == yes ]]; then
    eecho "could not create link using PowerShell+cmd; cause: ${failed_cause}."
    eecho "You could retry:"
    eecho "  powershell ${command_opts[*]}"
    eecho ""
    eecho "you may retry manually using the command below, with appropriate privilege/permissions:"
    eecho ""
    eecho "  ${cygpath_symlink_file//\\/\\\\}"
    eecho ""
    eecho "NOTE the file was not deleted for you to retry."
    return 2
  fi
  rm -f "${symlink_file}"
  return 0
}
