# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Validate that XMLSTARLET_EXECUTABLE is found.
#
_zb_xmlstarlet_init() { # {{{
  if [[ -z "${XMLSTARLET_EXECUTABLE:-}" ]] ; then
    XMLSTARLET_EXECUTABLE=''
    if [[ "${ZETA_BASH_OS}" != windows ]]; then
      if ! XMLSTARLET_EXECUTABLE=$(command -v xmlstarlet) ; then
        eecho "missing ${ZETA_BASH_COLOR_HL}XMLSTARLET_EXECUTABLE${ZETA_BASH_COLOR_NONE} env"
        wecho "please download xmlstarlet from ${ZETA_BASH_COLOR_HL}https://xmlstar.sourceforge.net/download.php${ZETA_BASH_COLOR_NONE}"
        wecho "or install it: "
        wecho "  ${ZETA_BASH_COLOR_HL}sudo apt install xmlstarlet${ZETA_BASH_COLOR_NONE}"
        wecho "  ${ZETA_BASH_COLOR_HL}sudo emerge xmlstarlet${ZETA_BASH_COLOR_NONE}"
        return 1
      fi
    else
      if ! XMLSTARLET_EXECUTABLE=$(command -v xml.exe) ; then
        # let's try winget installation folder, in case.
        # not sure if the identifier can vary
        if [[ -n "${XMLSTARLET_HOME}" ]]; then
          XMLSTARLET_EXECUTABLE="${XMLSTARLET_HOME}/xml.exe"
          [[ -f "${XMLSTARLET_EXECUTABLE}" ]] && return 0
        fi
        eecho "missing ${ZETA_BASH_COLOR_HL}XMLSTARLET_EXECUTABLE${ZETA_BASH_COLOR_NONE} env"
        wecho "please download xmlstarlet from ${ZETA_BASH_COLOR_HL}https://xmlstar.sourceforge.net/download.php${ZETA_BASH_COLOR_NONE}"
        wecho "and configure XMLSTARLET_HOME"
        return 1
      fi
    fi
  fi

  if [[ ! -x "$XMLSTARLET_EXECUTABLE" ]]; then
    eecho "${ZETA_BASH_COLOR_HL}XMLSTARLET_EXECUTABLE${ZETA_BASH_COLOR_NONE}=${ZETA_BASH_COLOR_HL}${XMLSTARLET_EXECUTABLE}${ZETA_BASH_COLOR_NONE} is not executable."
    return 1
  fi
  export XMLSTARLET_EXECUTABLE
  return 0
} # }}} [_zb_jq_init]

#
# will reparse command line on msys, to avoid XPATH issue (because // is replaced by C:/)
#
_zb_xmlstarlet() {
  _zb_xmlstarlet_init || return 1
  "${XMLSTARLET_EXECUTABLE}" "$@"
}
