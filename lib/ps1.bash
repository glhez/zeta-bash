# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

#
# check if the prompt info was udpated
#
# @param prefix variable prefix
# @param path path without realpath
#
_zb_prompt_updated() {
  local prefix="$1"
  local actual_executable="$2"

  local -n __exr="${prefix}_EXR"
  local -n __ver="${prefix}_VER"

  [[ -z "${__ver:-}" || -z "${__exr:-}" || ! "${__exr:-}" -ef "${actual_executable}" ]]
}

#
# save prompt information into variable [prefix]_...
#
# @param -u force update
# @param prefix variable prefix
# @param path path without realpath
# @param version version to save
#
_zb_prompt_save() {
  local update=auto
  if [[ "$#" != 0 && "${1:-}" == '-u' ]]; then
    update=yes
    shift
  fi
  if [[ $# -lt 3 ]]; then
    eecho "missing parameters <prefix> <path> <version>"
    return 1
  fi

  local prefix="$1"
  local path="$2"
  local version="$3"
  local realpath=""

  local -n __exe="${prefix}_EXE"
  local -n __exr="${prefix}_EXR"
  local -n __ver="${prefix}_VER"

  if [[ "$update" == auto ]]; then
    if [[ -z "${realpath}" ]] && ! realpath="$(realpath "$path" 2> /dev/null )" ; then
      realpath="$path"
    fi
    if [[ -z "${__exr:-}" || ! "${__exr:-}" -ef "${realpath}" ]]; then
      update=yes
    fi
  fi

  if [[ "$update" == yes ]]; then
    if [[ -z "${realpath}" ]] && ! realpath="$(realpath "$path" 2> /dev/null )" ; then
      realpath="$path"
    fi
    __exe="${path}"
    __exr="${realpath}"
    __ver="${version}"

    export "${prefix}_EXE"
    export "${prefix}_EXR"
    export "${prefix}_VER"
    return 0
  fi
  return 1
}

#
# Build the prompt
#
# Don't use directly, use _zb_ps1.
#
# The command use ZETA_BASH_PROMPT_DYNAMIC to determine if we should rewrite the PS1 based on content of ZETA_BASH_PROMPT_TOKENS.
# The purpose is to allow _zb_java_version, _zb_mvn_version, and _zb_env_store to preserve their environment variables, thus leading to optimisations.
#
# @param force_update force update of PS1 even if it is not needed.
#
_zb_prompt_builder() {
  local force_update="${1:-no}"

  # we still want the window title to be useful to user
  case $TERM in
    xterm*|rxvt|Eterm|eterm) echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007" ;;
    screen)                  echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033" ;;
  esac

  if [[ "${force_update}" != yes && "${force_update}" != 1 && "${ZETA_BASH_PROMPT_DYNAMIC:-no}" == no ]]; then
    return 0 # nothing to do, PS1 is good to go.
  fi

  local ps1_bits=''
  local i=0
  local space=''
  local cspace=''
  local handler=''

  local -A dev_prompt_handler=(
    ['maven']=_zb_mvn_version
    ['node']=_zb_node_version
    ['java']=_zb_java_version
  )
  local -A dev_prompt_prefix=(
    ['maven']='mvn: '
    ['node']='node: '
    ['java']='java: '
  )

  declare host_was_before=no
  declare __cached_value=''
  for token in "${ZETA_BASH_PROMPT_TOKENS_ARRAY[@]}" ; do
    if [[ $i -ne 0 ]]; then space=' '; fi
    case "$token" in
      time) ps1_bits+="${space}\e[0;36m[\e[1;36m\\t\e[0m\e[0;36m]\e[0m"                       ;;
      host)
        host_was_before=yes
        ps1_bits+="${space}\e[0;32m\\u@\\h\e[0m"
      ;;
      pwd)
        # facilitate the scp command: the user would just have to copy the host+pwd that way.
        if [[ "$host_was_before" == yes ]]; then
          ps1_bits+=":"
        else
          ps1_bits+="${space}"
        fi
        ps1_bits+="\e[0;33m\\w\e[0m"
      ;;
      git)    ps1_bits+="\`__git_ps1 '${space}\e[0;31m(\e[1;31m%s\e[0m\e[0;31m)\e[0m'\`"        ;;
      dev:*)
        local j=0
        token="${token:4}"
        value=''
        cspace=''
        for child in ${token//,/ }; do
          if [[ $j -ne 0 ]]; then cspace=' '; fi
          handler="${dev_prompt_handler[$child]}"
          if [[ -z "${handler}" ]]; then
            eecho "no handler for token ${child}"
          else
            # don't care about errors
            "${handler}" __cached_value
            if [[ -n "${__cached_value}" ]] ; then
              local prefix="${dev_prompt_prefix[$child]}"
              value+="${cspace}"
              # see https://scriptim.github.io/bash-prompt-generator/
              [[ -n "${prefix}" ]] && value+="\[\e[0;2;38;5;249m\]${prefix}\e[0m"
              value+="\[\e[0;1;38;5;38m\]${__cached_value}\[\e[0m\]"
              (( ++j ))
            fi
          fi
        done
        [[ -n "${value}" ]] && ps1_bits+="${space}\[\e[0;2;38;5;38m\][\[\e[0m\]${value}\[\e[0;2;38;5;38m\]]\[\e[0m\]"
      ;;
      zb_env)
        __cached_value=''
        _zb_env_store __cached_value
        [[ -n "${__cached_value}" ]] && ps1_bits+="${space}\e[0;35m[\e[1;35m${__cached_value}\e[0m\e[0;35m]\e[0m"
      ;;
    esac
    if [[ "$token" != host ]]; then
      host_was_before=0
    fi
    (( ++i ))
  done
  ps1_bits+="\\n\$ "
  export PS1="${ps1_bits}"
}

#
# Prepare a nice PS1.
#
# @param token... a list of token, which can be
#
#   time: print out the current time
#   host: the current host
#   pwd: the current directory
#   git: the current git branch
#   java: the current JVM version
#   mvn, maven: the current maven version.
#   zb_env: the current Zeta Bash env.
#
# java and maven are transformed into special tokens: dev:jdk,mvn in order to join them (this lead to shorter
# prompt).
#
# each token is preceded by a space, unless it is the first. git, java, maven and zb_env can be empty. They should not be at
# beginning of PS1 since there would be a space.
#
_zb_ps1() {
  local dynamic=0
  local -a new_tokens=()
  local dev_token=''
  local force_prompt_command=no
  local init_phase=no

  # resplit the token array (legacy vs new)
  local -a tokens=()
  for arg; do
    case "$arg" in
      --force|-f) force_prompt_command=yes ;;
      --init|-i)  init_phase=yes ;;
      -*) eecho "invalid option $arg" ; return 2 ;;
      # no token will starts with '-'
      *)
        local -a args=()
        read -r -a args <<< "$arg"
        tokens+=( "${args[@]}" )
      ;;
    esac
  done

  for token in "${tokens[@]}"; do
    case "$token" in
      git|time|host|pwd)
        if [[ -n "${dev_token}" ]]; then
          new_tokens+=( "dev:${dev_token:1}" )
          dev_token=''
        fi
        new_tokens+=( "$token" )
      ;; # this is only to allow token check.
      java|maven|mvn|nodejs|node)
        case "${token}" in
          mvn)    token=maven  ;;
          nodejs) token=node ;;
        esac
        dev_token+=",$token"
        (( ++dynamic ))
      ;;
      zb_env)
        if [[ -n "${dev_token}" ]]; then
          new_tokens+=( "dev:${dev_token:1}" )
          dev_token=''
        fi
        new_tokens+=( "$token" )
        (( ++dynamic ))
      ;;
      *)
        wecho "unsupported token ${ZETA_BASH_COLOR_HL}${token}${ZETA_BASH_COLOR_NONE}."
      ;;
    esac
  done
  [[ -n "${dev_token}" ]] && new_tokens+=( "dev:${dev_token:1}" )

  local our_prompt_command='declare -f -F _zb_prompt_builder > /dev/null && _zb_prompt_builder'
  # do not warn user when its session was restored
  if [[ "${init_phase}" == no && "${ZETA_BASH_SESSION_RESTORED}" != yes && -n "${PROMPT_COMMAND:-}" && "${PROMPT_COMMAND:-}" != "${our_prompt_command}" ]]; then
    wecho "${ZETA_BASH_COLOR_HL}PROMPT_COMMAND${ZETA_BASH_COLOR_NONE} is already set to ${ZETA_BASH_COLOR_HL}$(printf "%q" "${PROMPT_COMMAND}")${ZETA_BASH_COLOR_NONE}."
    if [[ $force_prompt_command == no ]]; then
      wecho "your nice PS1 won't be built! You can force it using ${ZETA_BASH_COLOR_HL}--force${ZETA_BASH_COLOR_NONE} or ${ZETA_BASH_COLOR_HL}-f${ZETA_BASH_COLOR_NONE}"
      return 1
    else
      wecho "forcing ${ZETA_BASH_COLOR_HL}PROMPT_COMMAND${ZETA_BASH_COLOR_NONE} in accordance to user request (${ZETA_BASH_COLOR_HL}--force${ZETA_BASH_COLOR_NONE})"
    fi
  fi

  declare -g -x  ZETA_BASH_PROMPT_DYNAMIC=no
  declare -g -x  ZETA_BASH_PROMPT_INSTALLED=yes
  declare -g -xa ZETA_BASH_PROMPT_TOKENS_ARRAY=( "${new_tokens[@]}" )

  [[ "$dynamic" != 0 ]] && ZETA_BASH_PROMPT_DYNAMIC=yes
  PROMPT_COMMAND="${our_prompt_command}"

  _zb_prompt_builder yes
}
