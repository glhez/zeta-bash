# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

alias tsvn="\${ZETA_BASH_HOME}/share/tortoise/tortoise-scm.bash svn"
alias tgit="\${ZETA_BASH_HOME}/share/tortoise/tortoise-scm.bash git"
alias tortoise="\${ZETA_BASH_HOME}/share/tortoise/tortoise-scm.bash auto"
alias t="\${ZETA_BASH_HOME}/share/tortoise/tortoise-scm.bash auto"

