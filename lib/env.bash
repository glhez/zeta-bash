# :mode=shellscript:folding=explicit:
#
# Part of Zeta Bash
#

#
# Flag this session as "restored"
#
# For the moment, this is only to fix a behavior with VSCode where the session is not fully restored (no NODE_HOME).
#
# We assume that some environment variables are kept, while other are not.
#
_zb_env_session_is_restored() {
  export ZETA_BASH_SESSION_RESTORED=no
  if [[ -n "${VSCODE_GIT_IPC_HANDLE}" && "${ZETA_BASH_PREVIOUS_VSCODE_GIT_IPC_HANDLE}" != "${VSCODE_GIT_IPC_HANDLE}" ]]; then
    export ZETA_BASH_PREVIOUS_VSCODE_GIT_IPC_HANDLE="${VSCODE_GIT_IPC_HANDLE}"
    export ZETA_BASH_SESSION_RESTORED=yes
  fi
  _zb_env_session_load
}

_zb_env_session_load() {
  # load session cache (to avoid stuff like reloading mvn, node, java information in prompt builder)
  local env_session_file="${ZETA_BASH_USER_CACHE}/.session"
  if [[ -f "${env_session_file}" ]] ; then
    #decho "reloading session..."
    #shellcheck source=shellcheck/session.cache.bash
    source "${env_session_file}"
  fi
}

_zb_env_session_save() {
  [[ "${ZETA_BASH_SESSION_LOCK_SAVE:-no}" == yes ]] && return 0
  local env_session_dir="${ZETA_BASH_USER_CACHE}"
  local env_session_file="${env_session_dir}/.session"

  local -a context=()
  context+=( ZETA_BASH_PS_JAVA_EXE ZETA_BASH_PS_JAVA_EXR ZETA_BASH_PS_JAVA_VER )
  context+=( ZETA_BASH_PS_MVN_EXE  ZETA_BASH_PS_MVN_EXR  ZETA_BASH_PS_MVN_VER  )
  context+=( ZETA_BASH_PS_NODE_EXE ZETA_BASH_PS_NODE_EXR ZETA_BASH_PS_NODE_VER )

  if [[ -d "${env_session_dir}" ]] || mkdir -p "${env_session_dir}" ; then
    #decho "saving session..."
    {
      echo '#!/bin/bash'
      declare -p "${context[@]}"  2> /dev/null  | sed -E -e 's@^declare +@declare -g @g'
    } > "${env_session_file}"
  fi
  return 0
}


#
# get path to env file
#
# @param --no-prefix remove prefix $ZETA_BASH_USER_HOME
# @param env_name the current env name, defaulted to default
#
_zb_env_file() { # {{{
  local noprefix=no
  if [[ "$1" == '--no-prefix' ]]; then
    shift
    noprefix=yes
  fi
  local env_name="${1:-default}"
  local env_file="env/${ZETA_BASH_OS}.${env_name}.env"

  if [[ "$noprefix" == yes ]]; then
    echo "$env_file"
  else
    echo "${ZETA_BASH_USER_HOME}/${env_file}"
  fi
} # }}} [_zb_env_file]

#
# try to source the env file
#
# @param env_name the current env name, mandatory
#
_zb_env_source() { # {{{
  local env_name="${1}"

  if [[ -z "${env_name}" ]]; then
    eecho "${ZETA_BASH_COLOR_HL}_zb_env_source${ZETA_BASH_COLOR_NONE} was called without an enviroment file"
    _zb_utils_print_stacktrace
    return 1
  fi

  # shellcheck disable=SC2155
  local env_file="$(_zb_env_file "${env_name}")"

  if [[ "${env_name}" != default ]]; then
    if [[ ! -e "${env_file}" ]]; then
      eecho "${ZETA_BASH_COLOR_HL}_zb_env_source${ZETA_BASH_COLOR_NONE} was called with an unknown environment <${ZETA_BASH_COLOR_HL}${env_name}${ZETA_BASH_COLOR_NONE}>: <${ZETA_BASH_COLOR_HL}${env_file}${ZETA_BASH_COLOR_NONE}> does not exists."
      _zb_utils_print_stacktrace "could not initialize env $env_name"
      return 2
    else
      # shellcheck disable=SC1090
      source "${env_file}"
      return $?
    fi
  elif [[ -e "${env_file}" ]]; then
    # shellcheck disable=SC1090
    source "${env_file}"
    return $?
  fi

  return 2
} # }}}

#
# initialize our own enviroment.
#
# @param env_name the env name, which default to "default"
_zb_env_init() { # {{{
  local OLD_MAVEN_DEFAULT="${MAVEN_DEFAULT:-}"
  local OLD_NODE_DEFAULT="${NODE_DEFAULT:-}"
  local OLD_JAVA_DEFAULT="${JAVA_DEFAULT:-}"
  local OLD_PROMPT_TOKENS="${ZETA_BASH_PROMPT_TOKENS:-}"

  local env_name=${1:-default}
  local init_phase="${2:-no}"
  if ! _zb_env_source "${env_name}" ; then
    return 1
  fi

  # we now invoke the PATH, which will do whatever is needed
  _zb_path_init --clean --remove-duplicates

  ZETA_BASH_SESSION_LOCK_SAVE=yes
  [[ -n "${JAVA_DEFAULT}"  && ( "${ZETA_BASH_SESSION_RESTORED}" == yes    || "${OLD_JAVA_DEFAULT}"   != "${JAVA_DEFAULT}"  ) ]] && _zb_java_switch --quiet
  [[ -n "${MAVEN_DEFAULT}" && ( "${ZETA_BASH_SESSION_RESTORED}" == yes    || "${OLD_MAVEN_DEFAULT}"  != "${MAVEN_DEFAULT}" ) ]] && _zb_mvn_switch  --quiet
  [[ -n "${NODE_DEFAULT}"  && ( "${ZETA_BASH_SESSION_RESTORED}" == yes    || "${OLD_NODE_DEFAULT}"   != "${NODE_DEFAULT}"  ) ]] && _zb_node_switch --quiet
  if [[ -n "${ZETA_BASH_PROMPT_TOKENS}" && ( "${init_phase}" == yes || "${ZETA_BASH_SESSION_RESTORED}" == yes || "${OLD_PROMPT_TOKENS}"  != "${ZETA_BASH_PROMPT_TOKENS}" || "${ZETA_BASH_PROMPT_INSTALLED:-no}" == no )]] ; then
    local -a options=()
    [[ "${init_phase}" == yes ]] && options+=( '--init' )
    _zb_ps1 "${options[@]}" "${ZETA_BASH_PROMPT_TOKENS}"
  fi
  ZETA_BASH_SESSION_LOCK_SAVE=no
  _zb_env_session_save

  # set up the current env
  ZETA_BASH_ENV_ACTUAL="${env_name}"
} # }}} [_zb_env_init]

#
# Restore the current PATH (imply that _zb_path_init was called)
# And reinit env.
#
# @param ... same as _zb_env_init
_zb_env_reload() { # {{{
  # the PATH was updated, we need to restore it to initial state.
  _zb_path_restore

  if [[ $# -eq 0 ]]; then
    if [[ -n "${ZETA_BASH_ENV_ACTUAL}" ]]; then
      _zb_env_init "${ZETA_BASH_ENV_ACTUAL}"
    else
      _zb_env_init "${ZETA_BASH_ENV}"
    fi
  else
    _zb_env_init "$@"
  fi
} # }}} [_zb_env_reload]

#
# Load the aliases.
#
_zb_env_load_aliases() {
  local aliases="${ZETA_BASH_ENV_LOAD_ALIASES}"

  # we don't use ${x:-y} because there will be more alias than that
  [[ -z "${aliases}" ]] && aliases="default git tortoise-scm gitex"

  #local IFS=' '
  for alias in $aliases; do
    local path="$ZETA_BASH_HOME/aliases/${alias}.aliases.bash"
    if [[ -e "${path}" ]]; then
      # shellcheck disable=SC1090
      source "${path}"
    else
      eecho "could not load aliases ${ZETA_BASH_COLOR_HL}${alias}${ZETA_BASH_COLOR_NONE}"
    fi
  done
  if [[ -d "${ZETA_BASH_USER_HOME}/aliases" ]]; then
    for alias in "${ZETA_BASH_USER_HOME}/aliases/"*'.aliases.bash' ; do
      if [[ -f "${alias}" ]] ; then
        # shellcheck disable=SC1090
        source "$alias"
      fi
    done
  fi

  #unset IFS
}

#
# Provide the current env name, for use in $PS1
#
# @param variable a variable name to be used as storage.
#
_zb_env_store() {
  declare -gn storage="${1}"
  local env_name="${ZETA_BASH_ENV_ACTUAL:-default}"
  local value
  if [[ "${env_name}" != 'default' && "${env_name}" != "${ZETA_BASH_ENV}" ]]; then
    value="${env_name}"
  else
    value=''
  fi
  # shellcheck disable=SC2034  # this is a reference
  storage="$value"
}


#
# Update env to new_value
#
# Warn the user when the value is updated.
#
# @param env_name name of env, such as M2_HOME
# @param new_value value to set
# @param quiet be quiet
# @return returns 0 if updated, 1 otherwise
#
_zb_env_update_var() {
  local env_name="$1"
  local old_value=''
  local new_value="$2"
  local quiet="${3:-no}"

  eval "old_value=\"\${${env_name}}\""

  if [[ "${old_value}" == "${new_value}" ]]; then
    return 1
  fi
  [[ "$quiet" == no ]] && iecho "setting ${ZETA_BASH_COLOR_PURPLE}${env_name}${ZETA_BASH_COLOR_NONE} to ${ZETA_BASH_COLOR_HL}${new_value}${ZETA_BASH_COLOR_NONE}"
  eval "export ${env_name}=${new_value@Q}"
  return 0
}

