  goto :end

:symbolic
  if exist %1 rmdir %1
  mklink /D %1 %2
  goto :end

:junction
  if exist %1 rmdir %1
  mklink /J %1 %2
  goto :end

:end

