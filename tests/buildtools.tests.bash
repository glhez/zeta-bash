#!/bin/bash -eu

declare SCRIPT_FILE
if ! SCRIPT_FILE=$(realpath "$0"); then
  echo "realpath is skrewed !"
  exit 1
fi
declare SCRIPT_DIR="${SCRIPT_FILE%/*}"

export ZETA_BASH_HOME="${SCRIPT_DIR%/*}"
export ZETA_BASH_USER_HOME="${ZETA_BASH_HOME%/*}/zeta-bash-user"

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/path.bash
source "${ZETA_BASH_HOME}/lib/path.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/buildtools.bash
source "${ZETA_BASH_HOME}/lib/buildtools.bash"
# shellcheck source=lib/java.bash
source "${ZETA_BASH_HOME}/lib/java.bash"
# shellcheck source=lib/maven.bash
source "${ZETA_BASH_HOME}/lib/maven.bash"
# shellcheck source=lib/node.bash
source "${ZETA_BASH_HOME}/lib/node.bash"
# shellcheck source=tests/assertions.bash
source "${ZETA_BASH_HOME}/tests/assertions.bash"

export TEST_TEMP_DIR=''

setUp() {
  local temp_dir=''
  if ! temp_dir=$(mktemp -d --suffix=_buildtools ); then
    report_failure "could not create a temporary directory"
  fi
  export TEST_TEMP_DIR="$temp_dir"

  pushd "${TEST_TEMP_DIR}" > /dev/null
    # real example of java
    mkdir -p multi/foobar-17.0.7.7-hotspot
    mkdir -p multi/foobar-17.0.6.7-hotspot
    mkdir -p multi/foobar-11.0.0-hotspot
    mkdir -p multi/foobar-8.0.372.7-hotspot
    mkdir -p multi/foobar-8.0.362.7-hotspot
    # real example of node
    mkdir -p single
    echo 'v20.21.22' > single/foobar
    # real example of java
    mkdir -p latest/foobar-317-hotspot
    mkdir -p latest/foobar-311-hotspot
    mkdir -p latest/foobar-38-hotspot
    # an arched version
    mkdir -p arch/foobar-v18.16.1-win-x64
  popd
  return 0
}

afterAll() {
  if [[ -n "$TEST_TEMP_DIR" && -d "$TEST_TEMP_DIR" ]]; then
    iecho "cleanup temp directory ${TEST_TEMP_DIR}"
    rm -Rf "$TEST_TEMP_DIR"
  fi
  return 0
}

test_bt_expand_variable() {
  local x=''

  export XYZ_9_HOME="v9"
  #export XYZ_9_1_2_HOME="v9.1.2"
  export XYZv2_9_1_2_HOME="v9.1.2@v2"

  assert_should_success "parse some version 9.1.2" _zt_utils_parse_semver "9.1.2"
  assert_should_success "resolve XYZ_9_HOME"       _zb_bt_expand_variable x 'XYZ@_HOME' 'XYZ@_V2_HOME'
  assert_equals         "result"                   "$XYZ_9_HOME" "$x"

  # with a specific array
  ZETA_BASH_SEMVER=()
  local x=''

  # shellcheck disable=SC2034
  local -A SOME_SEMVER=( [major]=9 [minor]=1 [patch]=2 )

  assert_should_success "resolve XYZ_9_HOME"       _zb_bt_expand_variable -n SOME_SEMVER x 'XYZ@_HOME' 'XYZ@_V2_HOME'
  assert_equals         "result"                   "$XYZ_9_HOME" "$x"
}


_foobar_entries() {
  _zb_bt_analyze_entry environment 'FOOBAR@_HOME'
  _zb_bt_analyze_entry directory "${TEST_TEMP_DIR}/latest/foobar-@VER-hotspot"
  _zb_bt_analyze_entry directory "${TEST_TEMP_DIR}/multi/foobar-@VER-hotspot"
  _zb_bt_analyze_entry directory "${TEST_TEMP_DIR}/arch/foobar-v@VER@ARCH"
  _zb_bt_analyze_entry version   'v([0-9]+)([.]([0-9]+)([.]([0-9]+))?)?' '1 3 5' "${TEST_TEMP_DIR}/single" cat "${TEST_TEMP_DIR}/single/foobar"
}

_foobar_test_entry() {
  return 0
}

test_bt_resolve_container() {
  local foobar_home

  # latest
  {
    foobar_home=''
    assert_should_success "resolving latest" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 317
    assert_equals "foobar_home [latest]" "${TEST_TEMP_DIR}/latest/foobar-317-hotspot" "${foobar_home}"
  }

  # specific
  {
    foobar_home=''
    assert_should_success "resolving specific" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 8 0 362
    assert_equals "foobar_home [specific]" "${TEST_TEMP_DIR}/multi/foobar-8.0.362.7-hotspot" "${foobar_home}"
  }

  # arched with invalid semver
  {
    foobar_home=''
    assert_should_fail "resolving arch with invalid semver" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 18.16.1
    assert_empty "foobar_home [arch with invalid semver]" "${foobar_home}"
  }

  # arched
  {
    foobar_home=''
    assert_should_success "resolving arch" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 18 16 1
    assert_equals "foobar_home [arch]" "${TEST_TEMP_DIR}/arch/foobar-v18.16.1-win-x64" "${foobar_home}"
  }

  # executable directly
  {
    foobar_home=''
    assert_should_success "resolving executable" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 20
    assert_equals "foobar_home [executable]" "${TEST_TEMP_DIR}/single" "${foobar_home}"
  }

  # most precise
  export FOOBAR_17_5_3_HOME='the_expected_path'

  {
    foobar_home=''
    assert_should_success "resolving an env" _zb_bt_resolve_container foobar_home _foobar_entries _foobar_test_entry 17 5 3
    assert_equals "foobar_home [env]" "the_expected_path" "${foobar_home}"
  }

  unset FOOBAR_17_5_3_HOME
}

test_with_time() {
  export TIMEFORMAT=$'\nreal %3lR\tuser %3lU\tsys %3lS'

  iecho "${ZETA_BASH_COLOR_ORANGE}JAVA${ZETA_BASH_COLOR_NONE}"
  time _zb_bt_resolve_container java_home   _zb_java_path_entries _zb_java_validate  17
  iecho "${ZETA_BASH_COLOR_ORANGE}NODE${ZETA_BASH_COLOR_NONE}"
  time _zb_bt_resolve_container node_home _zb_node_path_entries _zt_node_validate  18
  iecho "${ZETA_BASH_COLOR_ORANGE}MAVEN${ZETA_BASH_COLOR_NONE}"
  time _zb_bt_resolve_container mvn_home    _zb_mvn_path_entries  _zt_mvn_validate   3 9

  unset ZETA_BASH_RESOLVED_CONTAINER_ASSUME_CACHE_DIRTY
  return 0
}


run_test_suite "${SCRIPT_FILE##*/}" "$@"



