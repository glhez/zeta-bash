#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# ZETA_BASH_HOME must be defined, if not, then we compute it based on this script.
#
[[ -z "${ZETA_BASH_HOME:-}" ]] && export ZETA_BASH_HOME="${BASH_SOURCE[0]%/*}"

#
# do some magic in case of someone do . init.bash: compute ZETA_BASH_HOME
# from this script rather than from some random value.
#
if [[ "${#BASH_SOURCE[@]}" == 1 ]]; then
  declare bash_source_realpath=''
  if bash_source_realpath=$(realpath "${BASH_SOURCE[0]}" 2> /dev/null ); then
    export ZETA_BASH_HOME="${bash_source_realpath%/*}"
  fi
fi

#
# when run from codium, then ZETA_BASH_HOME may be using windows based path which
# we don't want.
#
if [[ "${OS,,}" == *windows* && "${ZETA_BASH_HOME:0:2}" == [A-Za-z]: && -n "$(command -v cygpath)" ]]; then
  ZETA_BASH_HOME=$(cygpath -u "${ZETA_BASH_HOME}")
fi

# we want absolute path.
if [[ -z "$(command -v realpath)" ]]; then
  ZETA_BASH_HOME=$(cd "${ZETA_BASH_HOME}" > /dev/null 1>&2 && pwd || echo "${ZETA_BASH_HOME}")
else
  ZETA_BASH_HOME=$(realpath "$ZETA_BASH_HOME")
fi
export ZETA_BASH_HOME

export -A ZETA_BASH_LIB_TIME=()
export -a ZETA_BASH_LIB_ORDER=()

#
# This function will load a library; if that library is not found (or if source fails), we will
# display an error message.
#
# We don't scan the directory to ensure the correct initialization order.
#
_zb_lib() {
  local mtime=0
  local cyan='\e[1;36m'
  local red="\e[1;31m"
  local hl="\e[1m"
  local none="\e[0m"
  local lib=''

  local global_mtime=''

  for lib; do
    local p="${ZETA_BASH_HOME}/lib/$lib.bash"
    # shellcheck disable=SC1090
    if ! source "$p" ; then
      echo -e "${red}error|${none} could not load library ${hl}${lib}${none} from path ${hl}${p}${none}"
      return 1
    fi

    if [[ -z "${ZETA_BASH_LIB_TIME["$lib"]}" ]]; then
      # do not use the _actual time_: this will avoid calls to date. We only want to be able to update file that were
      # updated since we started.
      if [[ -z "${global_mtime}" ]]; then
        # because we use '-n', this
        case "${BASH_VERSION}" in
          0.*|1.*|2.*|3.*|4.0.*|4.1.*) global_mtime=0                 ;;
          4.*)               printf -v global_mtime '%(%s)T' -1       ;;
          5.*)                         global_mtime="${EPOCHSECONDS}" ;;
        esac
      fi
      ZETA_BASH_LIB_TIME["$lib"]="${global_mtime}"
      ZETA_BASH_LIB_ORDER+=( "$lib" )
    fi
  done
  if [[ "$#" == 0 ]]; then
    for lib in "${ZETA_BASH_LIB_ORDER[@]}"; do
      local p="${ZETA_BASH_HOME}/lib/$lib.bash"
      ! mtime=$(date -r "$p" '+%s') && mtime=0
      local c="${ZETA_BASH_LIB_TIME["$lib"]:-0}"
      if [[ $mtime -gt $c ]]; then
        echo -e "${cyan}info|${none} reloading library ${hl}${lib}${none} from path ${hl}${p}${none}"
        # shellcheck disable=SC1090
        if ! source "$p" ; then
          echo -e "${red}error|${none} could not load library ${hl}${lib}${none} from path ${hl}${p}${none}"
          return 1
        fi
        ZETA_BASH_LIB_TIME["$lib"]=$mtime
      fi
    done
  fi
  return 0
}

_zb_lib utils      \
        pathutils  \
        os         \
        path       \
        env        \
        buildtools \
        java       \
        maven      \
        node       \
        completion \
        gitex      \
        ps1

_zb_color_init "$@" || :

#
# ZETA_BASH_USER_HOME must also be defined.
#
if [[ -n "$1" ]]; then
  ZETA_BASH_USER_HOME=$(_zb_get_absolute_path "$1" )
  export ZETA_BASH_USER_HOME
elif [[ -z "${ZETA_BASH_USER_HOME}" ]]; then
   ZETA_BASH_USER_HOME=$(_zb_get_absolute_path ../zeta-bash-user "${ZETA_BASH_HOME}")
  export ZETA_BASH_USER_HOME
fi

if [[ ! -d "${ZETA_BASH_USER_HOME}" ]]; then
  eecho "${ZETA_BASH_COLOR_HL}ZETA_BASH_USER_HOME${ZETA_BASH_COLOR_NONE} point to non existent directory ${ZETA_BASH_COLOR_HL}${ZETA_BASH_USER_HOME}${ZETA_BASH_COLOR_NONE}."
  eecho "Please update your .bashrc file by either setting ${ZETA_BASH_COLOR_HL}ZETA_BASH_USER_HOME${ZETA_BASH_COLOR_NONE} or passing an arg while sourcing ${ZETA_BASH_COLOR_HL}${BASH_SOURCE[0]}${ZETA_BASH_COLOR_NONE}."
else
  [[ -z "${ZETA_BASH_USER_CONFIG}" ]] && export ZETA_BASH_USER_CONFIG="${ZETA_BASH_USER_HOME}/.config"
  [[ -z "${ZETA_BASH_USER_CACHE}" ]]  && export ZETA_BASH_USER_CACHE="${ZETA_BASH_USER_HOME}/.cache"
fi

#
# This will not change today.
#
_zb_determine_os
_zb_determine_arch
_zb_path_backup

export ZETA_BASH_BIN_WINVOKE="${ZETA_BASH_HOME}/bin/winvoke"

# post init
_zb_env_session_is_restored
_zb_completion_init
_zb_env_init "${ZETA_BASH_ENV}" yes
_zb_env_load_aliases

# allow (to some extends) the user to override this
[[ -z "${ZETA_BASH_GITEX_CONF}" ]] && export ZETA_BASH_GITEX_CONF="${ZETA_BASH_USER_CONFIG}/gitex.v2.bash"
