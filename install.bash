#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#
declare -r ZETA_BASH_INSTALL_SCRIPT=$0

declare zb_home_dir=""

if ! zb_home_dir=$(realpath "$0" ) ; then
  echo "could not determine current path for $0"
  exit 2
fi

zb_home_dir="${zb_home_dir%/*}"

usage() {
  echo "${ZETA_BASH_INSTALL_SCRIPT}

--help, -h

  Show this help

--user-home <path>, --user-home=<path>, -u<path> :

  Set up ZETA_BASH_USER_HOME to this path.

--update

  Try to update the user home directory if it exists.

Note: calling this several times will skip the part already done.

"

}

#
# check what we will do
#
declare do_user_home=''

declare capture_next_arg=0
declare previous_arg=''
declare do_update=0
for arg; do
  case "$arg" in
    --help|-h)
      usage
      exit 1
    ;;
    --user-home|-u)
      capture_next_arg=1
      previous_arg="$arg"
    ;;
    --update)
      do_update=1
    ;;
    --user-home=*)
      do_user_home="${arg:12}"
    ;;
    *)
      if [[ $capture_next_arg -ne 0 ]]; then
        capture_next_arg=0
        case "$previous_arg" in
          --user-home|-u) do_user_home="$arg"
        esac
      fi
      echo "invalid option: ${arg}"
      echo "usage: ${ZETA_BASH_INSTALL_SCRIPT} --help ..."
      exit 2
    ;;
  esac
done

header() {
  echo -e "${C_YELLOW}${*}${C_NONE}"
}

item() {
  echo -e ":: ${*}"
}


# color constants to be used
declare -r C_RED="\e[1;31m"    #red
declare -r C_GREEN="\e[1;32m"  #green
declare -r C_NONE="\e[0m"      #none
declare -r C_CYAN="\e[1;36m"   #cyan
declare -r C_YELLOW="\e[1;33m" #yellow
declare -r C_HL="\e[1m"        #highlight

check_required_programs() {
  local error=0
  local programs=()
  programs+=( ls dirname basename )
  programs+=( find xargs )
  programs+=( grep sed )
  programs+=( uniq sort cut )
  programs+=( :du :df )
  programs+=( tar :gzip :gunzip )
  programs+=( curl git )
  programs+=( realpath :cygpath :wslpath )

  header 'Checking required programs'
  local optional=no
  for exec in "${programs[@]}" ; do
    if [[ "${exec:0:1}" == ':' ]]; then
      optional=yes
      exec="${exec:1}"
    else
      optional=no
    fi

    declare wexec
    if ! wexec=$(command -v "$exec" 2> /dev/null) || [[ -z "$wexec" ]]; then
      if [[ $optional == no ]]; then
        item "[${C_RED}KO${C_NONE}] ${C_HL}${exec}${C_NONE}"
        (( ++error ))
      else
        item "[${C_YELLOW}KO${C_NONE}] ${C_HL}${exec}${C_NONE} (optional)"
      fi
    else
      item "[${C_GREEN}OK${C_NONE}] ${C_HL}${exec}${C_NONE} -> ${C_HL}${wexec}${C_NONE}"
    fi
  done

  if [[ "${error}" -gt 0 ]]; then
    item "${C_RED}error:${C_NONE} could not install, missing ${C_HL}${error}${C_NONE} required programs."
  fi

  if ! "${zb_home_dir}/share/bash-compatibility-test.bash" ; then
    (( ++error ))
  fi
  return "${error}"
}

setup_env() {
  if [[ ! -d "${zb_home_dir}" ||  ! -f "${zb_home_dir}/init.bash" ]]; then
    echo -e "${C_RED}error:${C_NONE} could not install, ${C_HL}${zb_home_dir}${C_NONE} points to nothing."
    return 1
  fi

  # shellcheck source=lib/utils.bash
  source "${zb_home_dir}/lib/utils.bash"
  # shellcheck source=lib/pathutils.bash
  source "${zb_home_dir}/lib/pathutils.bash"
  # shellcheck source=lib/os.bash
  source "${zb_home_dir}/lib/os.bash"
  # shellcheck source=lib/env.bash
  source "${zb_home_dir}/lib/env.bash"

  # shellcheck disable=SC2155
  export ZETA_BASH_HOME=$(_zb_get_absolute_path "${zb_home_dir}")

  # shellcheck disable=SC2155
  if [[ -z "${ZETA_BASH_USER_HOME}" && -z "$do_user_home" ]]; then
    export ZETA_BASH_USER_HOME="$(_zb_get_absolute_path ../zeta-bash-user "${ZETA_BASH_HOME}")"
  elif [[ -n "$do_user_home" ]]; then
    export ZETA_BASH_USER_HOME="$(_zb_get_absolute_path "${do_user_home}")"
  else
    export ZETA_BASH_USER_HOME="$(_zb_get_absolute_path "${ZETA_BASH_USER_HOME}")"
  fi

  _zb_determine_os

  header 'Installer configuration'
  item "${C_CYAN}ZETA_BASH_HOME     ${C_NONE}: ${ZETA_BASH_HOME}"
  item "${C_CYAN}ZETA_BASH_USER_HOME${C_NONE}: ${ZETA_BASH_USER_HOME}"
}

initialize_user_home() {
  header 'Initializing user home'
  if [[ -d "${ZETA_BASH_USER_HOME}" && $do_update -eq 0 ]]; then
    item "nothing to initalize, directory exists; use ${C_RED}--update${C_NONE} to try to update."
    return 0
  fi

  if [[ ! -d "${ZETA_BASH_USER_HOME}" ]]; then
    do_update=0
  fi

  if [[ $do_update -eq 1 ]]; then
    item "trying to update ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE}."
  fi

  # shellcheck disable=SC2155
  local share_directory="${ZETA_BASH_HOME}/share/zeta-bash-user"
  # shellcheck disable=SC2155
  local default_env_file="$(_zb_env_file --no-prefix)"
  local fileset_to_create=( "${default_env_file}" )

  # shellcheck disable=SC2155
  local parent_dir="${ZETA_BASH_USER_HOME%/*}"
  if [[ ! -d "${parent_dir}" ]]; then
    item "creating ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE} intermediate directories..."
    mkdir -p "${parent_dir}"
  fi

  # see how to *correctly* hook a check on result.
  if [[ $do_update -eq 0 ]]; then
    item "copying ${C_HL}${share_directory}${C_NONE} to ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE}..."
    cp -r "${share_directory}" "${ZETA_BASH_USER_HOME}"
    if [[ $? -ne 0 ]]; then
      item "${C_RED}error:${C_NONE} could not copy ${C_HL}${share_directory}${C_NONE}"
      return 1
    fi
  else
    item "update files from ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE} using ${C_HL}${share_directory}${C_NONE} to ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE}..."
    # don't fail in update mode: the user will have to work!
    local skipped=0
    while read -r file; do
      local left_file="${share_directory}/${file#./}"
      local right_file="${ZETA_BASH_USER_HOME}/${file#./}"
      local right_dir="${right_file%/*}"

      if [[ ! -f "$right_file" ]]; then
        item "copying ${C_HL}${left_file}${C_NONE} to ${C_HL}${right_dir}${C_NONE}..."
        mkdir -p "$right_dir" && cp "${left_file}" "${right_dir}"
        if ! mkdir -p "$right_dir" && cp "${left_file}" "${right_dir}" ; then
          item "${C_RED}error:${C_NONE} copy failed!${C_NONE}"
        fi
      else
        item "skipping copy of ${C_HL}${left_file}${C_NONE} to ${C_HL}${right_dir}${C_NONE}..."
        (( ++skipped ))
      fi
    done < <(cd "${share_directory}" && find . -type f)

    if [[ $skipped -gt 0 ]]; then
      item "${C_YELLOW}${skipped} files${C_NONE} were skipped; you might wish to compare them with ${C_HL}${share_directory}${C_NONE}."
    fi
  fi

  if [[ $do_update -eq 0 && -d .git ]]; then # we test .git to avoid messing the Zeta Bash git dir.
    item "${C_RED}error:${C_NONE} folder already exists and update flag not set.${C_HL}${ZETA_BASH_USER_HOME}${C_NONE}"
    return 1
  fi
  if ! pushd "${ZETA_BASH_USER_HOME}" > /dev/null ; then
    item "${C_RED}error:${C_NONE} could not create ${C_HL}${ZETA_BASH_USER_HOME}${C_NONE}"
    return 1
  fi


  local do_default_env_file=0
  if [[ ! -f "$default_env_file" ]]; then
    do_default_env_file=1
  fi

  for (( i = 0; i < ${#fileset_to_create[@]}; ++i )); do
    local file="${fileset_to_create[$i]}"
    item "creating ${C_HL}${file}${C_NONE}..."
    mkdir -p "${file%/*}" && touch "$file" > "${file}"
  done

  if [[ $do_default_env_file -eq 1 ]]; then
    item "writing default env file ${C_HL}${default_env_file}${C_NONE}..."
    {
      echo '# :mode=shellscript:'
      echo "ZETA_BASH_GITEX_REPOPATH=''"
      echo "ZETA_BASH_GITEX_REPOPATH+=':${ZETA_BASH_HOME}=0'"
      echo "ZETA_BASH_GITEX_REPOPATH+=':${ZETA_BASH_USER_HOME}=0'"
      # shellcheck disable=SC2016
      echo 'ZETA_BASH_GITEX_REPOPATH="${ZETA_BASH_GITEX_REPOPATH:1}"'
      echo 'export ZETA_BASH_GITEX_REPOPATH'
    } > "$default_env_file"
  fi

  if [[ $do_update -eq 1 ]]; then
    if [[ ! -d .git ]]; then
      git init
    fi
    item "[${C_GREEN}SUCCESS${C_NONE}] update was done, you can now browse your user repository to commit changes."
    item "[${C_GREEN}SUCCESS${C_NONE}] try this command to check: ${C_YELLOW}g zeta-bash-user${C_NONE}"
  else
    item "initializing new git directory"
    git init && git add --all . && git commit -m 'initial commit'
  fi

  if ! popd > /dev/null ; then
    item "${C_YELLOW}could not go back to previous directory (popd failed).${C_NONE}."
  fi
}

download_maven_completion() {
  local url='https://raw.githubusercontent.com/glhez/maven-bash-completion/master/bash_completion.bash'
  local dest="${ZETA_BASH_HOME}/completions/maven.completion.bash"

  header "Downloading maven completion file to ${C_HL}${ZETA_BASH_HOME}${C_NONE}"

  local prompt
  local default
  if [[ -f "${dest}" ]]; then
    default=N
    prompt='Would you like to redownload Maven completion file ? [y/N]'
  else
    default=Y
    prompt='Would you like to download Maven completion file ? [Y/n]'
  fi

  read -r -p "${prompt} " download
  case "${download:-${default}}" in
    no|NO|n|N|No)
      return 0
    ;;
  esac

  item "downloading ${C_HL}${url}${C_NONE} to ${C_HL}${dest}${C_NONE}"
  if ! command curl "${url}" -o "${dest}" ; then
    item "${C_RED}error:${C_NONE} could not download maven completion."
    return 1
  fi
  item "download maven completion."
}


copy_in_bashrc() {
  local bashrc_file="${HOME:-~}/.bashrc"
  local line="source '${ZETA_BASH_HOME}/init.bash'"

  header "Updating ${C_HL}${bashrc_file}${C_NONE}"
  if ! command grep -qF "${line}" "${bashrc_file}" ; then
    item "${C_HL}${line}${C_NONE} line not found, adding it..."
    echo ""        >> "${bashrc_file}"
    echo "#shellcheck disable=SC1091"
    echo "${line}" "${ZETA_BASH_USER_HOME@Q}" >> "${bashrc_file}"
  else
    item "${C_HL}${line}${C_NONE} line found, seems good!"
  fi
  return 0
}

check_required_programs   &&
              setup_env   &&
   initialize_user_home   &&
download_maven_completion &&
          copy_in_bashrc



