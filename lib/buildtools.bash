# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#
export ZETA_BASH_BT_DEBUG=0
export ZETA_BASH_ARCH_LIST_INITIALIZED=no

_zb_bt_debug() {
  [[ "${ZETA_BASH_BT_DEBUG:-0}" != 0 ]]
}

#
# resolve a variable against ZETA_BASH_SEMVER or set of variables
#
# @param [-n ARRAY] use array instead of ZETA_BASH_SEMVER
# @param home_target local variable target to the value
# @param variables... a pattern containing @ (which is replaced by the version)
# @return 0 if variable was found, and value stored in $target
#
_zb_bt_expand_variable() {
  local bash_array='ZETA_BASH_SEMVER'
  if [[ "${1:-}" == '-n' ]]; then
    bash_array="${2}"
    shift 2
  fi
  local home_target="$1"
  shift 1
  local -a replacements=()

  local -n _____semver_bash_array="${bash_array:-ZETA_BASH_SEMVER}"

  local major="${_____semver_bash_array[major]:-}"
  local minor="${_____semver_bash_array[minor]:-}"
  local patch="${_____semver_bash_array[patch]:-}"
  if [[ -n "${patch}" ]]; then replacements+=( "_${major}_${minor}_${patch}" "${major}_${minor}_${patch}" ) ; fi
  if [[ -n "${minor}" ]]; then replacements+=( "_${major}_${minor}"          "${major}_${minor}"          ) ; fi
                               replacements+=( "_${major}"                   "${major}"                   )

  for pattern; do
    for replacement in "${replacements[@]}"; do
      local name="${pattern/@/"${replacement}"}"
      if [[ -v "$name" ]]; then
        local -n _____name="${home_target}"
        local -n _____value="${name}"
        _____name="${_____value}"
        return 0
      fi
    done
  done
  return 1
}

_zb_bt_expand_all() {
  local re='^_?([0-9]+)$'
  local -A process_variables=()
  for pattern; do
    local prefix="${pattern%'@'*}"
    local suffix="${pattern##*'@'}"
    while read -r var_name ; do
      [[ -n "${process_variables["${var_name}"]:-}" ]] && continue
      if [[ "${var_name}" == *"${suffix}" ]]; then
        local version="${var_name#"${prefix}"}"
        version="${version%"${suffix}"}"
        if [[ "${version}" =~ $re ]]; then
          local major="${BASH_REMATCH[1]}"
          # same reason than with dir: assume minor/patch/etc is highest
          _zb_bt_append_entry environment "$major" 99999 99999 99999 "${!var_name}"
        fi
      fi
      process_variables["${var_name}"]=yes
    done < <(compgen -v "${prefix}")
  done
}

_zb_bt_expand_arch_init() {
  if [[ "${ZETA_BASH_ARCH_LIST_INITIALIZED:-no}" == no ]]; then
    export -a ZETA_BASH_ARCH_LIST=()
    case "${ZETA_BASH_ARCH}" in
      x86_64)
        [[ "${ZETA_BASH_OS}" == windows ]] && ZETA_BASH_ARCH_LIST=('-win-x64' )
        [[ "${ZETA_BASH_OS}" != windows ]] && ZETA_BASH_ARCH_LIST=('-amd64'   )
      ;;
      *)
        [[ "${ZETA_BASH_OS}" == windows ]] && ZETA_BASH_ARCH_LIST=('-win-x86' )
      ;;
    esac
    # always add the empty arch
    ZETA_BASH_ARCH_LIST+=( '' )
    export ZETA_BASH_ARCH_LIST_INITIALIZED=yes
  fi
}

#
# Append an entry without testing
#
# @param source source (used for caching, eg: directory, env, version)
# @param major
# @param minor
# @param patch
# @param buildno
# @param path
#
_zb_bt_append_entry() {
  local index="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN}"

   ZETA_BASH_RESOLVED_CONTAINER_ARRAY_SOURCES[index]="$1"
    ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[index]="$2"
    ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[index]="$3"
   ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[index]="$4"
   ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[index]="$5"
     ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[index]="$6"

  (( ++ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN ))
}

#
# Append an entry
#
# @param source source (used for caching, eg: directory, env, version)
# @param major
# @param minor
# @param patch
# @param buildno
# @param path
#
_zb_bt_append_entry_if() {
  local source="$1"
  local major="$2"
  local minor="$3"
  local patch="$4"
  local buildno="$5"
  local entry="$6"

  local    validator="${ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR}"
  local target_major="${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[major]:-}"
  local target_minor="${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[minor]:-}"
  local target_patch="${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[patch]:-}"

  if [[ (   ( -z "$target_major" || "$target_major" == "$major" ) &&
            ( -z "$target_minor" || "$target_minor" == "$minor" ) &&
            ( -z "$target_patch" || "$target_patch" == "$patch" )  ) ]] &&
          ( [[ "${source}" == version || -z "${validator}" ]] ||
          "${validator}" "$entry" ) ; then
    _zb_bt_append_entry "${source}" "${major}" "${minor}" "${patch}" "${buildno}" "${entry}"
  fi
}

#
# Analyze list of environment variables
#
# Result of this function may be cached
#
# @input ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER
# @input ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR
# @param env... variables name to analyze
#
_zb_bt_analyze_env_entry() {
  local -a replacements=()
  case "${ZETA_BASH_RESOLVED_CONTAINER_OPT_ENV_ALL}" in
    major) _zb_bt_expand_all "$@" ;;
    *)
      local directory=''
      if _zb_bt_expand_variable -n ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER directory "$@" && ( [[ -z "${ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR}" ]] || "${ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR}" "$directory" ) ; then
        _zb_bt_append_entry environment "${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[major]:-}" "${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[minor]:-}" "${ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[patch]:-}" 0 "${directory}"
      fi
      return 0
    ;;
  esac
}

#
# Analyze one directory entry
#
# @param directory a directory to analyse
#
_zb_bt_analyze_dir_entry() {
  local directories=()
  local directory_expr="$1"
  if [[ "$directory_expr" == *@ARCH* ]]; then
    _zb_bt_expand_arch_init
    for arch in "${ZETA_BASH_ARCH_LIST[@]}"; do
      directories+=( "${directory_expr//'@ARCH'/"$arch"}" )
    done
  else
    directories+=( "$directory_expr" )
  fi

  if _zb_bt_debug; then
    iecho "_zb_bt_analyze_dir_entry:"
    for (( i = 0, n = ${#directories[@]}; i < n; ++i )); do
      iecho "  directories[$i] : ${directories[i]}"
    done
  fi

  local directory=''
  local -A registered_homes=()
  for directory in "${directories[@]}"; do
    local filename="${directory##*/}"
    local dirname="${directory%/*}"

    local ver_before="${filename%%'@VER'*}"
    local ver_after="${filename##*'@VER'}"

    local n_before=$(( ${#dirname} + 1 + ${#ver_before}  )) # don't forget the '/'
    local n_after=$((  ${#ver_after}  ))
    local entry=''

    if _zb_bt_debug; then
      iecho "reading directory ... : $directory"
      iecho "  ver_before ........ : $ver_before"
      iecho "  ver_after ......... : $ver_after"
    fi

    for entry in "${dirname}/${ver_before}"*"${ver_after}"; do
      if [[ -d "$entry" && -z "${registered_homes["$entry"]:-}" ]] ; then
        local version="${entry}"
        [[ $n_after != 0 ]] && version="${version:0:-$n_after}"
        version="${version:$n_before}"

        #
        # in some case (jdk on windows), the version may use '+' or '.', and have 4 numbers.
        # this handle such case and ignore duplicate not matching the versions.
        #
        # Also, if the version in complete (no min/patch) assume a value of 99999 so that sort
        # will put the version first (it is probably a symlink)
        #
        if [[ "${version}" =~ ^([0-9]+)([.+][0-9]+)?([.+][0-9]+)?([.+][0-9]+)?([.+][0-9]+)? ]]; then
          local a="${BASH_REMATCH[1]:-}"            ; a="${a:-99999}"
          local b="${BASH_REMATCH[2]:-}"; b="${b:1}"; b="${b:-99999}"
          local c="${BASH_REMATCH[3]:-}"; c="${c:1}"; c="${c:-99999}"
          local d="${BASH_REMATCH[4]:-}"; d="${d:1}"; d="${d:-99999}"

          _zb_bt_append_entry_if directory "${a}" "${b}" "${c}" "${d}" "${entry}"
          # avoid duplicates, especially with arch.
          registered_homes["$entry"]=yes
        else
          wecho "ignoring ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE} found in ${ZETA_BASH_COLOR_HL}${entry}${ZETA_BASH_COLOR_NONE}."
        fi
      fi
    done
  done
}

#
# Analyse a version produced by executable --version
#
# The home will be the parent directory of executable
#
# @param regexp a regexp to parse the produced version using =~
# @param index an array of indexes, with 0=major, 1=minor, 2=patch, 3=buildno
# @param directory directory to append; if empty or '-', use executable basedir
# @param executable (an absolute path is required here)
# @param parameters...
#
_zb_bt_analyze_version() {
  local regexp="$1"
  #shellcheck disable=SC2206
  local -a indexes=( $2 )
  local directory="${3:-}"
  shift 3
  if ver=$( "$@" ) && [[ "$ver" =~ $regexp ]]; then
    local   major_index="${indexes[0]:-}"
    local   minor_index="${indexes[1]:-}"
    local   patch_index="${indexes[2]:-}"
    local buildno_index="${indexes[3]:-}"

    local major=''
    local minor=''
    local patch=''
    local buildno=''

    [[ -n "${major_index}"   ]] &&   major="${BASH_REMATCH[$major_index]}"
    [[ -n "${minor_index}"   ]] &&   minor="${BASH_REMATCH[$minor_index]}"
    [[ -n "${patch_index}"   ]] &&   patch="${BASH_REMATCH[$patch_index]}"
    [[ -n "${buildno_index}" ]] && buildno="${BASH_REMATCH[$buildno_index]}"
    [[ -z "${directory}" || "${directory}" == '-' ]] && directory="${1%/*}"
    _zb_bt_append_entry_if version "$major" "$minor" "$patch" "$buildno" "${directory}"
  fi
}

#
# Analyze an entry
#
# @param type type of entry
#             e | environment | env : remaning parameters are env variable with @ replaced by the semver
#             d | directory   | dir : single parameter is a directory, which may contains @ARCH for arch and @VER for version guess
#             x | execute           : invoke "$@"; the handler will call _zb_bt_append_entry
#
_zb_bt_analyze_entry() {
  local type="${1:-}"
  shift
  case "$type" in
    e|environment|env) _zb_bt_analyze_env_entry "$@" ;;
    d|directory|dir)   _zb_bt_analyze_dir_entry "$1" ;;
    v|version|ver)     _zb_bt_analyze_version   "$@" ;;
    *)
      eecho "invalid type: $type"
      return 1
    ;;
  esac
  return 0
}

#
# Initialize a resolver context
#
_zb_bt_resolve_find() {
  # mandatory
  local entry_provider=''
  # optional
  local validator=''
  local major=''
  local minor=''
  local patch=''

  local prev_opt=''
  local arg=''
  local arg_index=0
  local skip_opt=no
  local list_all_env=no
  for arg; do
    if [[ -n "${prev_opt}" ]]; then
      case "$prev_opt" in
        --provider)  entry_provider="${arg}" ;;
        --validator) validator="${arg}"      ;;
        *) eecho "invalid ${prev_opt} with mandatory arg." ;;
      esac
      prev_opt=''
      continue
    fi
    if [[ "${skip_opt}" == no ]]; then
      local positional_arg=no
      case "$arg" in
        --provider)  prev_opt="$arg"  ;;
        --validator) prev_opt="$arg"  ;;
        --all-major) list_all_env=major ;;
        --)          skip_opt=yes  ;;
        -*)          eecho "invalid option ${prev_opt}" ; return 1 ;;
        *)           positional_arg=yes ;;
      esac
      [[ "${positional_arg}" == no ]] && continue
    fi
    case "$arg_index" in
      0) major="$arg" ;;
      1) minor="$arg" ;;
      2) patch="$arg" ;;
      *) eecho "unsupported positionned parameter: ${arg_index} (value: <$arg>)" ; return 1 ;;
    esac
    (( ++arg_index ))
  done

  if [[ -z "${entry_provider}" ]]; then
    _zb_utils_print_stacktrace2 "no entry provider."
    return 2
  fi

  #
  declare -xg  ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR="$validator"
  declare -xgA ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER=()
  declare -xgA ZETA_BASH_RESOLVED_CONTAINER_OPT_ENV_ALL="$list_all_env"
  declare -xg  ZETA_BASH_ARCH_LIST_INITIALIZED=no
  [[ -n "$major" ]] && ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[major]="$major"
  [[ -n "$minor" ]] && ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[minor]="$minor"
  [[ -n "$patch" ]] && ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER[patch]="$patch"

  _zb_bt_resolve_create
  "${entry_provider}"
}

_zb_bt_resolve_create() {
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_SOURCES=()
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS=()
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS=()
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES=()
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO=()
  declare -xga ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES=()
  declare -xg  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN=0
  # this need to be done in two step, otherwise bash won't reinit if coming from cache
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_SOURCES=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES=()
  ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN=0
}

_zb_bt_resolve_destroy() {
  unset ZETA_BASH_RESOLVED_CONTAINER_OPT_VALIDATOR
  unset ZETA_BASH_RESOLVED_CONTAINER_OPT_SEMVER
  unset ZETA_BASH_RESOLVED_CONTAINER_OPT_ENV_ALL

  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_SOURCES
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES
  unset ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN
}

#
# Resolve the "HOME" directory of an executable, such as Java, Maven or Node
#
# First, entry_provider is called producing lines of path entries with _zb_bt_analyze_entry.
#
# @param target local variable target to the value
# @param entry_provider a function producing a list of path to process
# @param test_command a function testing for the home after resolution
# @param major,minor,patch override ZETA_BASH_SEMVER
# @return 0 if variable was found, and value stored in $target
#
_zb_bt_resolve_container() { # {{{
  local target="$1"
  local entry_provider="$2"
  local test_command="$3"
  shift 3
  if [[ $# != 0 ]]; then
    local major="${1:-}"
    local minor="${2:-}"
    local patch="${3:-}"
  else
    local major="${ZETA_BASH_SEMVER[major]:-}"
    local minor="${ZETA_BASH_SEMVER[minor]:-}"
    local patch="${ZETA_BASH_SEMVER[patch]:-}"
  fi

  _zb_bt_resolve_find --provider "${entry_provider}" --validator "${test_command}" "${major}" "${minor}" "${patch}"
  #_zb_utils_is_debug_enabled && {
  #  echo "--- RESULT [$entry_provider] ---"
  #  _zb_bt_expand_sort --debug
  #  echo "--- RESULT [$entry_provider] ---"
  #}
  local found_home=''
  if _zb_bt_expand_sort --find-first ; then
    found_home="${ZETA_BASH_RESOLVED_CONTAINER_FIND_FIRST_REPLY}"
  fi
  if [[ -n "${ZETA_BASH_RESOLVED_CONTAINER_FIND_FIRST_REPLY}" ]]; then
    local -n _____zb_bt_resolve_container_out0="$target"
    _____zb_bt_resolve_container_out0="${found_home}"
    return 0
  fi
  return 1
} # }}} [_zb_bt_resolve_container]


#
# Handle result of _zb_bt_resolve_find
#
# @param mode a work mode
#   --debug       output information
#   --find-first  store first path in ZETA_BASH_RESOLVED_CONTAINER_FIND_FIRST_REPLY (string)
#   --path        store all path in ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY (array)
#   --completion  store semver of each version in ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY
#
_zb_bt_expand_sort() { # {{{
  local mode="$1"

  #
  #
  # this use a buble sort because sadly, 'sort' + pipe takes too many time during prompt startup (100ms * {nodejs,maven,java})
  #
  # and the multi array stuff makes it worst
  #
  # To make it simplier, we keep the sorted index
  #
  #
  local -a sorted_indexes=()
  local   i=0
  local   k=0
  local  j1=0
  local  j2=0
  local j1i=0
  local j2i=0

  for (( i = 0; i < ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN; ++i )); do
    sorted_indexes+=( "$i" )
  done

  for (( i = 0; i < ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN; ++i )); do
    local swap_done=no
    for (( j1i = 0, j2i = 1, k = ZETA_BASH_RESOLVED_CONTAINER_ARRAY_LEN - i - 1; j1i < k; ++j1i, ++j2i )); do
      local cmp=1

      local j1="${sorted_indexes[j1i]}"
      local j2="${sorted_indexes[j2i]}"

      # major,minor,patches,buildno are reverse sorted, entries is not
      if [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[j1]}"          == "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[j2]}" ]]; then
        if [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[j1]}"        == "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[j2]}" ]]; then
          if [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[j1]}"     == "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[j2]}" ]]; then
            if [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[j1]}"   == "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[j2]}" ]]; then
                if [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[j1]}" == "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[j2]}" ]]; then cmp=0
              elif [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[j1]}" > "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[j2]}" ]]; then  cmp=1
              else cmp=-1
                fi # entries
            elif [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[j1]}" > "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[j2]}" ]]; then  cmp=-1
            else cmp=1
              fi # buildno
          elif [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[j1]}" > "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[j2]}" ]]; then  cmp=-1
          else cmp=1
            fi # patches
        elif [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[j1]}" > "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[j2]}" ]]; then  cmp=-1
        else cmp=1
          fi # minors
      elif [[ "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[j1]}" > "${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[j2]}" ]]; then  cmp=-1
      else cmp=1
        fi # majors

      if [[ "$cmp" == 1 ]]; then
        swap_done=yes
        sorted_indexes[j1i]="$j2"
        sorted_indexes[j2i]="$j1"
      fi
    done
    [[ "$swap_done" == no ]] && break
  done

  case "$mode" in
    --debug)
      for index in "${sorted_indexes[@]}"; do
        local major="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[index]:-}"
        local minor="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[index]:-}"
        local patch="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[index]:-}"
      local buildno="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[index]:-}"
        local entry="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[index]:-}"

        echo "${major};${minor};${patch};${buildno};${entry}"
      done | sort --unique
    ;;
    --find-first)
      export ZETA_BASH_RESOLVED_CONTAINER_FIND_FIRST_REPLY=''
      [[ "${#sorted_indexes[@]}" == 0 ]] && return 1
      export ZETA_BASH_RESOLVED_CONTAINER_FIND_FIRST_REPLY="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[sorted_indexes[0]]}"
      return 0
    ;;
    --path)
      [[ "${#sorted_indexes[@]}" == 0 ]] && return 1
      local -A replied=()
      declare -g -a ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY
      ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY=()
      for index in "${sorted_indexes[@]}"; do
        local path="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_ENTRIES[index]}"
        [[ -n "${replied[$path]:-}" ]] && continue
        ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY+=( "${path}" )
        replied["${path}"]=yes
      done
      export ZETA_BASH_RESOLVED_CONTAINER_PATH_REPLY
      return 0
    ;;
    --completion)
      declare -g -a ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY
      ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY=()
      for index in "${sorted_indexes[@]}"; do
        local major="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MAJORS[index]:-}"
        local minor="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_MINORS[index]:-}"
        local patch="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_PATCHES[index]:-}"
        local buildno="${ZETA_BASH_RESOLVED_CONTAINER_ARRAY_BUILDNO[index]:-}"
        local semver="$major"
        if [[ -n "$minor" && $minor != 99999 ]] ; then
          semver+=".$minor"
          if [[ -n "$patch" && "$patch" != 99999 ]]; then
            semver+=".$patch"
            if [[ -n "$buildno" && "$buildno" != 99999 ]]; then
              semver+=".$buildno"
            fi
          fi
        fi
        [[ -n "$semver" ]] && ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY+=( "$semver" )
      done
      export ZETA_BASH_RESOLVED_CONTAINER_COMPLETION_REPLY
    ;;
  esac
  return 0
} # }}} [_zb_bt_expand_sort]


#
# lookup for a command, store it in exported variable
#
# If variable is defined, fail if the path it point to is not executable
#
# If variable is NOT defined, will look pathlike:
#
# - `alias:env`: use another env as alias
# - `command:[os:][word]`: use command -v [word]
# - `exec:[os:]path`: lookup for an executable at path
# - `c` and `x` are alias for `command` and `exec`.
#
# If present, supported os are win or !win;
#
# @param variable name of variable to export to
# @param pathlike... path like item, indicating how to search
#
_zb_bt_command() {
  local variable="$1"
  shift 1

  local -n value="${variable}"
  if [[ -n "${value:-}" ]]; then
    if [[ ! -x "$value" ]]; then
      eecho "environment ${ZETA_BASH_COLOR_HL}${variable}${ZETA_BASH_COLOR_NONE} contains non executable value: ${ZETA_BASH_COLOR_HL}${value}${ZETA_BASH_COLOR_NONE}"
      return 1
    fi
    return 0
  fi

  for arg; do
    local type="${arg%%:*}"
    arg="${arg#"${type}":}"
    local os=''
    case "$arg" in
      *:*)
        os="${arg%%:*}"
        arg="${arg#"${os}":}"
      ;;
    esac
    local path="$arg"

    case "$os" in
      '!win') if [[ "${ZETA_BASH_OS}" == windows ]]; then continue ; fi ;;
      'win')  if [[ "${ZETA_BASH_OS}" != windows ]]; then continue ; fi ;;
    esac

    local rv=''
    case "$type" in
      alias|a)
        local -n alias_value="${path}"
        if [[ -n "${alias_value:-}" ]]; then
          if [[ ! -x "$alias_value" ]]; then
            eecho "environment alias ${ZETA_BASH_COLOR_HL}${path}${ZETA_BASH_COLOR_NONE} contains non executable value: ${ZETA_BASH_COLOR_HL}${alias_value}${ZETA_BASH_COLOR_NONE}"
            return 1
          fi
          value="${alias_value}"
          return 0
        fi
      ;;
      command|c)
        if rv=$(command -v "$path" 2> /dev/null ); then
          value="$rv"
          return 0
        fi
      ;;
      exec|x)
        if [[ -x "$path" ]]; then
          value="$path"
          return 0
        fi
        if [[ "${ZETA_BASH_OS}" == windows ]]; then
          IFS=';'; for ext in ${PATHEXT} ; do
            rv="$path$ext"
            if [[ -x "$rv" ]]; then
              value="$rv"
              return 0
            fi
          done
        fi
      ;;
    esac
  done
  return 1
}
