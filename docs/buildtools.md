# Build tools

The buildtools library provides helper function to determine:

- current version of a SDK (Java, ...) to use
- resolve environment variable against a semver
- provide completion

## switch-java, switch-maven, switch-node

### How switch function works

Whatever the SDK, the switch function work more or less the same:

- the user "switch" the SDK using a version (`switch-java 8`) or the default (eg: `switch-java`)
- the version must match a semver (more or less `major.minor.patch.buildno`)
- `_zb_bt_resolve_container` is used to select the corresponding version: each entries (see next section specific to
  SDK) is analyzed
  - versions are matched against semver (ignored missing fields)
  - path entry is validated using a specific handler (by default, the path entry must be a directory)
  - for directories, version is determined based on the filename of each pattern (eg: an entry
    in `apache-maven-4.0.0` will have version `4.0.0` even if that's not the actual version)
  - the result is sorted by descending semver (highest first)
  - the first entry is the winning path
- the appropriate SDK home (eg: `JAVA_HOME`, `M2_HOME`, ...) is updated
- the `PATH` is rebuilt to introduce the new SDK home
- the prompt for the SDK is cleared and session saved.
- (maven only) `MAVEN_OPTS`, `M2_SETTINGS` and `M2_PROFILES` are updated with content of `MAVEN@_OPTS`,
  `MAVEN@_SETTINGS` and `MAVEN@_PROFILES`. This allow for example enabling TLS1.3 on Maven 2.2.1 with Java
  7 (...).

### Java

The following directories are supported as java homes (see `_zb_java_path_entries`)

- explicit path defined in `$JAVA@_HOME`, `$JAVA@_ORACLE_HOME` or `$JAVA@_OPENJ9_HOME` (where `@` is replaced by requested
  version, if any)
- all path matching `latest/openjdk-@VER-hotspot` in `$ZETA_BASH_JAVA_CONTAINER_PATH` (if you use openjdk-downloader)
- all path matching `temurin-@VER-jdk@ARCH` in `/usr/lib/jvm` (linux)
- all path matching `jdk-@VER-hotspot` in `C:\Program Files\Eclipse Adoptium` (windows)

### Maven

The following directories are supported as maven homes (see `_zb_mvn_path_entries`)

- explicit path defined in `$MVN@_HOME` or `$MAVEN@_HOME` (where `@` is replaced by requested version, if any)
- all path matching `apache-maven-@VER` in `$ZETA_BASH_MAVEN_CONTAINER_PATH` (if you use openjdk-downloader)
- all path matching `apache-maven-@VER` in `$MAVEN_INSTALL_HOME`

### Node

The following directories are supported as NodeJS homes (see `_zb_node_path_entries`)

- explicit path defined in `$NODE@_HOME`, `$NODEJS@_ORACLE_HOME` or `$JAVA@_OPENJ9_HOME` (where `@` is replaced by requested
  version, if any)
- all path matching `latest/node-v@VER` in `$ZETA_BASH_NODEJS_CONTAINER_PATH` (if you use nodejs-downloader)
- all path matching `latest/node-v@VER` in `$NODEJS_INSTALL_HOME` (if you use nodejs-downloader)
- all path matching `v@VER` in `$HOME/.nvm/versions/node` (if `nvm-sh` is used)
- if `C:\Program Files\nodejs\node.exe` (windows) is an executable file
- if `/usr/bin/node` (linux) is an executable file

When the path is an executable path, the version is resolved with `node --version` (which produce entries like `v18.17.1`).

### How this work for PS1 (`_zb_java_version`, `_zb_node_version`, `_zb_mvn_version`)

By default, `env-reload` initalize the PS1 with `_zb_ps1` and a list of tokens.

If tokens contains `java`, `mvn` or `node` then version is evaluated using appropriate function.

The three function work more or less the same, with a subtle variation for Java due to `Oracle/Java/javapath/java`:

- `command -v` is used to resolved the actual path
- if cached version is empty or if cached path is not same as actual path
  - the actual version is evaluated using the actual path (for example, `node --version` or `mvn -B --version`)
  - `ZETA_BASH_PS_<SDK>_EXE` and `ZETA_BASH_PS_<SDK>_VER` are updated
  - session is saved
- version is returned with a hash (`#`) which indicate a change or without

The `_zb_SDK_version` tries hard to minimize invoking the command from which version can be found: this is because (at
least on Windows) creating a subprocess is slow for a lot of reasons.

## System Releases

Some system SDK are supported, otherwise only local installations are supported.

- See [Install Eclipse Temurin™](https://adoptium.net/installation/) for installations using `winget`, `apt get`, ...
- You may also use `openjdk-downloader`: read the help using `openjdk-downloader --help` or directly from here: [usage](../share/openjdk/usage.txt).
- You may also use `nodejs-downloader`: read the help using `nodejs-downloader --help` or directly from here: [usage](../share/nodejs/usage.txt).
