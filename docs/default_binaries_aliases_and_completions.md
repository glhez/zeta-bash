# Default aliases and completions

## Default aliases

### Miscellaneous

- `ll`: display content of a directory using long list `ls` format. Also display dates in long ISO (because stuff like
  *Jan 9 2015* is less readable than `2015-01-09 21:48`).
- `empty`: find empty directories not being inside a git repository.
- `clean-empty-directories`: clean empty directories not inside a git repository.
- `ex`: open an explorer. On Windows, this is particulary useful as it will open the Explorer in the current directory
  or open the file or directory pointed by the first parameter using the default action.
- `cntlm-start` and `cntlm-stop` require [CNTLM][1]. This proxy is very useful even on Windows as it allows Java application
 (such as Eclipse, Nexus, ...) to work correctly when being a NTLM proxy.

### Zeta Bash

- `env-reload`: reload your environment. This can take an optional environment name.
- `env-load-aliases`: load or reload the aliases. This can be useful if you added a new alias file, or changed one already
  existing.
- `env-load-completions`: load or reload the Bash completions.
- `path-print`: pretty print the path.
- `switch-java`: switch to some Java version (8, 11, ...) or path (`JAVA_HOME`).
- `switch-maven`: switch to some maven version (3.6.3, 2.2.0, ...)
- `switch-node`: switch to some maven version (3.6.3, 2.2.0, ...)

### Git

- `bb`: display all branches.
- `ss`: display the status (note: this is used by another Linux application. Either unset it, either use \ss to use that
  particular Linux application).
- `tt`: display tags.

### Git (Advanced)

- `git-assume-unchanged`: assume that a file displayed as changed by `git status` was not changed.
- `git-assume-changed`: assume that a file was changed. This should be applied on file marked as unchanged by `git-assume-unchanged`.
- `git-show-unchanged`: show the file that are flagged as *unchanged*.

### Tortoise SVN / GIT

The aliases currently support only SVN and GIT, but then can be easily updated to add other Tortoise handler such as Tortoise
Mercurial.

- `tgit`, `tsvn`: invoke the corresponding TortoiseProc handler.
- `tortoise`, `t`: invoke TortoiseProc for the current SCM type.

The completion provide a list of available commands (which are mainly `log`, `commit`, `status`).

## Default completions

Zeta Bash provide completions for its own functionalities (Git Extension, etc).

It also provide completion for the Nexus launcher (found in `/bin/nexus`) and for Maven if it was downloaded during the
installation.

## Tools added in PATH

These tools are added to the PATH for faster use:

### Miscellaneous

- `notepadplusplus` starts [Notepad++](https://notepad-plus-plus.org/) from the command line.
- `vscode` starts [VSCodium](https://vscodium.com/) or [VSCode](https://code.visualstudio.com/) from the command line
- `jedit` starts jEdit. It will also translate any parameters to Windows Path if you are on Windows. By that it means
  that path like `/e/git` will get translated to `E:\git`. This makes it the perfect tool for launching jedit from the terminal!
- `find-and-grep` find ... and then grep, and eventually open that in editor

### Git

GIT script are prefixed by gitex to avoid confusion with legit GIT tools.

- `gitex-empty-dir`: create an empty `.gitkeep` file to version an empty directory.
- `gitex-remove-backup-ref`: remove backup reference created by `git filter-branch`.
- `gitex-clean-m2e`: clean file using `git clean -nxd` but handle m2e (Maven for Eclipse) files such as `.project` that
  are typically ignored in SCM and therefore subject to deletion by `git clean`.
- `gitex-gc`: invoke `git reflog expire` and `git gc` to clean your repositories.

### Git (Advanced)

- `bin/git/trim-spaces-at-eol.bash`: rewrite the history of some rev list to remove space at end of line.

### Maven

- `maven-generate-toolchains`: generate a toolchain file based on JDK found in default location or in env.
- `maven-test-report`: look for maven surefire/failsafe reports containing test execution failure, and store them into a
  specific directory.
- `maven-move-resources`: move resources found in `src/main/java` or `src/test/java` to the appropirate resources directory;
  this won't fix the `pom.xml` in case of includes/excludes or filtering.

## Other tools

### Git

- `shared/git/configure-kdiff3-as-difftool.bash` set up KDiff3 for use with Git.
- `shared/git/trim-spaces-at-eol-in-revisions.bash` a tool to trim space at end of line in GIT history.

[1]: http://cntlm.sourceforge.net/
