. | {
  lts: @sh "\(.available_lts_releases)",
  all: @sh "\(.available_releases)",
  latest: "\(.available_releases | max)",
} | "#!/bin/bash
\($jq_lts_releases)=( \(.lts) )
\($jq_all_releases)=( \(.all) )
\($jq_latest_release)=\(.latest)
"
