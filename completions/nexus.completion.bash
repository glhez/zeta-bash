# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# on windows, nexus require some admin right, so we only let the console
case "${ZETA_BASH_OS}" in
  windows) complete -o nospace -W 'console' nexus;;
  *)       complete -o nospace -W 'console start stop restart install uninstall' nexus
  ;;
esac
