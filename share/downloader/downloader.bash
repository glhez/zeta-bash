# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
#
# This library is explicitely used by openjdk-downloader and is not meant to be
# used directly.
#
#

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/os.bash
source "${ZETA_BASH_HOME}/lib/os.bash"
# shellcheck source=lib/cliparser.bash
source "${ZETA_BASH_HOME}/lib/cliparser.bash"
# shellcheck source=lib/cliparser.bash
source "${ZETA_BASH_HOME}/lib/jq.bash"

export ZETA_BASH_WRAPPED=0
_zb_determine_os
_zb_determine_arch

# header/footer file for link creation (extension is completed based on os)
declare -r SYMLINK_SHARE_DIR="${ZETA_BASH_HOME}/share/create-link"
declare -r SYMLINK_HEADER_FILE="${SYMLINK_SHARE_DIR}/create-link.header"
declare -r SYMLINK_FOOTER_FILE="${SYMLINK_SHARE_DIR}/create-link.footer"

#
# default configuration
#

# where we store downloaded files
declare -r DEFAULT_CONFIG_ARCHIVES_DIR='/.archives'
# name of link to be created (warning: maven-generate-toolchains must be fixed as well)
declare -r DEFAULT_CONFIG_SYMLINK_NAME=latest
# where the symlink is to be created
declare -r DEFAULT_CONFIG_SYMLINK_DIR="/${DEFAULT_CONFIG_SYMLINK_NAME}"
# where we download json files
declare -r DEFAULT_CONFIG_CACHE_DIR='/.cache'
# configuration to be used
declare -r DEFAULT_CONFIG_CONFIG_FILE="/${DOWNLOADER_NAME}.conf"
# where to install new jdk
declare -r DEFAULT_CONFIG_INSTALL_DIR=''

# see _zb_downloader_initialize_paths
declare       CONFIG_CONTAINER_VARNAME=''
declare       CONFIG_CONTAINER_PATH=''

# actual values

# where we store archive
declare CONFIG_ARCHIVES_DIR=".${DEFAULT_CONFIG_ARCHIVES_DIR}"
# where we store "latest" symlink
declare CONFIG_SYMLINK_DIR=".${DEFAULT_CONFIG_SYMLINK_DIR}"
# where we store cache data
declare CONFIG_CACHE_DIR=".${DEFAULT_CONFIG_CACHE_DIR}"
# where we store eventual configuration file
declare CONFIG_CONFIG_FILE=".${DEFAULT_CONFIG_CONFIG_FILE}"
# where we do install
declare CONFIG_INSTALL_DIR=".${DEFAULT_CONFIG_INSTALL_DIR}"

#
# path to the usage file, used by _zb_cliparser_parse_options
#
# @type path
#
export CONFIG_USAGE_FILE=''

# do we save configuration?
# @type bool
declare -x OPTIONS_SAVE_CONFIG=no

# do we refresh downloaded json?
# @type bool
declare -x OPTIONS_REFRESH=no

# regenerate jq file based on json
# @type bool
declare -x OPTIONS_JQ_REFRESH=no

# create symlink, junction (windows specific, synonym to symlink otherwise) or do nothing
# @type enum{no,symbolic,junction}
declare OPTIONS_GENERATE_SYMBOLIC_LINKS=no

# windows only: use powershell to ask for elevated prompt
# this is a hack again symbolic link requiring user to elevate
# @type bool
declare OPTIONS_REQUIRE_ELEVATED_PROMPT=yes

# wait some time after file extraction
# default to yes on Windows, due to antivirus locking files.
# @type bool
declare OPTIONS_WAIT_AFTER_FILE_EXTRACTION=no
# os specific values
[[ "${ZETA_BASH_OS}" == windows ]] && OPTIONS_WAIT_AFTER_FILE_EXTRACTION=yes

# how to handle archive: no (do nothing), yes (clean archive not being the last)
# @type enum {no, archive, dir, all}
declare -x OPTIONS_CLEAN=no


# see _zb_downloader_validate_version + _zb_downloader_install_versions + _zb_downloader_generate_symbolic_links
# [downloader specifics] {{{

# if a version is validated
# @type bool[key]
declare -A downloader_validated_versions=()

# friendly name of a version
# @type string[key]
declare -A downloader_validated_names=()

# path to archive for version
# @type path[key]
declare -A downloader_validated_archives=()

# name of install directories for version (default to archive filename without extension)
# @type {filename}[key]
declare -A downloader_validated_dirnames=()

# name of symlink to generate (can be empty, in which case link is disabled for version)
# @type {filename}[key]
declare -A downloader_validated_symlink=()

# full path of install directories for version (default to archive filename without extension)
# @type path[key]
declare -A downloader_validated_install_directories=()

# full path to symlink given key, only if downloader_validated_symlink is not empty
# @type path[key]
declare -A downloader_validated_symlink_path=()

# name of environment variable (eg: JAVA_HOME, NODE_HOME, ...)
# @type varname[key]
declare -A downloader_validated_env_names=()

# }}} [downloader specifics]

#
# initialize default options.
#
_zb_downloader_init_default_options() {
  _zb_cliparser_register_option_variable     OPTIONS_GENERATE_SYMBOLIC_LINKS    enum junction symlink no
  _zb_cliparser_describe                     OPTIONS_GENERATE_SYMBOLIC_LINKS    "generate symbolic link to major version: value is no, junction, (sym)link"
  _zb_cliparser_register_option_map_singular OPTIONS_GENERATE_SYMBOLIC_LINKS    'J:junction' 'L:symlink' ':no-link:no'

  _zb_cliparser_register_option_variable     OPTIONS_WAIT_AFTER_FILE_EXTRACTION bool
  _zb_cliparser_describe                     OPTIONS_WAIT_AFTER_FILE_EXTRACTION "wait some time after extraction is done (windows specific)"
  _zb_cliparser_register_option_map_singular OPTIONS_WAIT_AFTER_FILE_EXTRACTION 'W:wait'

  _zb_cliparser_register_option_variable     OPTIONS_REQUIRE_ELEVATED_PROMPT    bool
  _zb_cliparser_describe                     OPTIONS_REQUIRE_ELEVATED_PROMPT    "require elevated prompt ? (windows specific for links)"
  _zb_cliparser_register_option_map_singular OPTIONS_REQUIRE_ELEVATED_PROMPT    'e:elevated'

  _zb_cliparser_register_option_variable     OPTIONS_CLEAN enum archive dir all no
  _zb_cliparser_describe                     OPTIONS_CLEAN "clean directory, archive, all or none"
  _zb_cliparser_register_option_map_singular OPTIONS_CLEAN ':clean-archive:archive' ':clean-dir:dir' ':clean:all' ':no-clean:no'
  _zb_cliparser_register_option_map          OPTIONS_CLEAN --clean optional all

  _zb_cliparser_register_option_variable     OPTIONS_REFRESH bool
  _zb_cliparser_register_option_map_singular OPTIONS_REFRESH ':refresh'

  _zb_cliparser_register_option_variable     OPTIONS_JQ_REFRESH bool
  _zb_cliparser_register_option_map_singular OPTIONS_JQ_REFRESH ':jq-refresh'

  _zb_cliparser_register_option_variable     OPTIONS_SAVE_CONFIG bool
  _zb_cliparser_register_option_map_singular OPTIONS_SAVE_CONFIG ':save-config' 's:save'
}

#
# Initialize {@code CONFIG_CONTAINER_VARNAME} and {@code CONFIG_CONTAINER}.
#
_zb_downloader_initialize_paths() { # {{{
  local valid_path=''
  local valid_varname=''
  for name ; do
    if [[ -v "$name" ]]; then
      local -n path="$name"
      local p=''
      if ! p="$(realpath "$path")" || [[ -z "$p" || ! -d "$p" ]]; then
        eecho "invalid ${ZETA_BASH_COLOR_HL}${name}${ZETA_BASH_COLOR_NONE}, not a directory: ${ZETA_BASH_COLOR_HL}${path}${ZETA_BASH_COLOR_NONE}."
        return 1
      else
        valid_path="$p"
        valid_varname="$name"
        break
      fi
    fi
  done

  if [[ -z "${valid_varname}" || -z "${valid_path}" ]]; then
    eecho "could not find a valid environment variable containing an ${ZETA_BASH_COLOR_RED}existing${ZETA_BASH_COLOR_NONE} directory".
    eecho "tested variables: ${*}"
    return 1
  fi

  export CONFIG_CONTAINER_VARNAME="${valid_varname}"
  export CONFIG_CONTAINER_PATH="${valid_path}"

  export CONFIG_INSTALL_DIR="${valid_path}${DEFAULT_CONFIG_INSTALL_DIR}"
  export CONFIG_ARCHIVES_DIR="${valid_path}${DEFAULT_CONFIG_ARCHIVES_DIR}"
  export CONFIG_SYMLINK_DIR="${valid_path}${DEFAULT_CONFIG_SYMLINK_DIR}"
  export CONFIG_CACHE_DIR="${valid_path}${DEFAULT_CONFIG_CACHE_DIR}"
  export CONFIG_CONFIG_FILE="${valid_path}${DEFAULT_CONFIG_CONFIG_FILE}"
  return 0
} # }}} [_zb_downloader_initialize_paths]

#
# Create a parent directory if needed.
#
# This will mainly create the parent directory for said path.
#
# @param path some path
# @return 1 if mkdir failed.
#
_zb_downloader_prepare_dir() { # {{{
  local path="$1"
  local parent="${path%/*}"
  if [[ "${parent}" != "${path}" && -n "${parent}" ]]; then
    if [[ ! -d "$parent" ]] && ! mkdir -p "$parent" ; then
      eecho "could not create parent directories of ${ZETA_BASH_COLOR_HL}${path}${ZETA_BASH_COLOR_NONE}"
      return 1
    fi
  fi
  return 0
} # }}} [_zb_downloader_prepare_dir]

#
# Convert a downloaded JSON into a Bash file using JQ
#
# @param url endpoint to invoke
# @param filter_file jq filter file to use
# @param bash_file a bash file
# @param jq_options... other jq options
#
_downloader_json_to_bash() {
  local url="$1"
  local jq_filter_file="$2"
  local bash_file="$3"
  shift 3

  local json_file="${bash_file%.bash}.json"
  local header_file="${json_file%.bash}.header"

  local json_file_failed="${json_file}.failed"
  local bash_file_failed="${bash_file}.failed"

  decho "about to download ${url} to ${json_file} (OPTIONS_REFRESH=${OPTIONS_REFRESH})"

  _zb_downloader_prepare_dir "${bash_file}" || return 1

  if [[ "$OPTIONS_REFRESH" == yes || ! -f "${json_file}" || -f "${json_file_failed}" ]]; then
    local -a curl_options=()
    curl_options+=( curl )
    curl_options+=( --fail --silent --show-error -L --compressed -H  "accept: application/json" )
    curl_options+=( --dump-header "${header_file}" )
    curl_options+=( --output "${json_file}" )
    curl_options+=( "${url}" )
    if ! "${curl_options[@]}" ; then
      eecho "unable to invoke JSON web service"
      eecho " - endpoint: [${ZETA_BASH_COLOR_HL}${url}${ZETA_BASH_COLOR_NONE}]"
      eecho " - curl output: [${ZETA_BASH_COLOR_HL}${json_file}${ZETA_BASH_COLOR_NONE}]"
      eecho " - curl header: [${ZETA_BASH_COLOR_HL}${header_file}${ZETA_BASH_COLOR_NONE}]"
      echo "${curl_options[*]}" > "${json_file_failed}"
      return 1
    else
      rm -f "${json_file_failed}" "${bash_file}"
    fi
  fi

  decho "about to generate jq ${bash_file} (OPTIONS_JQ_REFRESH=${OPTIONS_JQ_REFRESH})"

  if [[ "${OPTIONS_JQ_REFRESH}" == yes  || -f "${bash_file_failed}" || "${jq_filter_file}" -nt "${bash_file}" ]]; then
    local jq_opts=()
    jq_opts+=( "${JQ_EXECUTABLE}" )
    jq_opts+=( "$@" )
    jq_opts+=( --from-file "${jq_filter_file}" --raw-output "${json_file}" )
    decho "generating jq ${bash_file} (${jq_opts[*]})"
    if ! "${jq_opts[@]}" > "${bash_file}" || [[ ! -f "${bash_file}" ]] ; then
      eecho "jq failed to generate ${ZETA_BASH_COLOR_HL}${bash_file}${ZETA_BASH_COLOR_NONE} file to be sourced."
      echo "${jq_opts[*]}" > "${bash_file_failed}"
      rm -f "${bash_file}"
      return 1
    else
      [[ "${ZETA_BASH_OS}" == windows ]] && dos2unix --quiet "${bash_file}"
      rm -f "${bash_file_failed}"
    fi
  fi
  return 0
}

#
# Extract file to directory (it may be created if needed but its parent should exists)
#
# @param file the archive file
# @param target the target directory
# @return if if tar/unzip failed, or if archive type is not supported
#
_zb_downloader_extract_file() { # {{{
  local file="$1"
  local target # this fix shellcheck error on target being array, then string
  target="$2"

  case "$file" in
    *.zip)
      if ! unzip "${file}" -d "${target}" ; then
        eecho "could not unzip file ${ZETA_BASH_COLOR_HL}${file}${ZETA_BASH_COLOR_NONE}."
        rm -Rf "${target}"
        return 1
      fi
    ;;
    *.tar.xz|*.tar.gz)
      if [[ ! -d "$target" ]] && ! mkdir -p "${target}" ; then
        eecho "could not create directory ${ZETA_BASH_COLOR_HL}${target}${ZETA_BASH_COLOR_NONE}."
        return 1
      fi
      if ! tar -axvf "${file}" --directory "${target}" ; then
        eecho "could not untar file ${ZETA_BASH_COLOR_HL}${file}${ZETA_BASH_COLOR_NONE}."
        rm -Rf "${target}"
        return 1
      fi
    ;;
    *)
      eecho "unsupported kind of archive: ${ZETA_BASH_COLOR_HL}${file}${ZETA_BASH_COLOR_NONE}."
      return 1
    ;;
  esac
  return 0
} # }}} [_zb_downloader_extract_file]

#
# Attempt to move a file to target, and it fails, retry after 10 seconds.
#
# @param source
# @param target
#
_zb_downloader_move_retry() { # {{{
  local source="$1"
  local target="$2"
  if mv "${source}" "${target}" ; then
    return 0
  fi
  # let's wait, especially on windows, then retry
  if [[ "${OPTIONS_WAIT_AFTER_FILE_EXTRACTION}" == yes ]]; then
    local message_suffix=''
    if [[ "${ZETA_BASH_OS}" == windows ]]; then
      message_suffix=", because you are on Windows and someone will lock the file..."
    fi
    declare delay=10
    while [[ $delay -gt 0 ]]; do
      wwrap "sleeping ${delay} seconds${message_suffix}"
      sleep 1
      (( --delay ))
    done
    wwrap "lock delay now expired, resuming flat or move operation"
    if mv "${source}" "${target}" ; then
      return 0
    fi
  fi
  return 1
} # }}} [_zb_downloader_move_retry() { # {{{

#
# Will move the top directory of extracted directory to the expected target_dir
# If there are multiple directory, move directory_to_flatten to target_dir
#
# @param path directory_to_flatten a directory to flatten
# @param path target_dir target directory
#
_zb_downloader_flatten_dir() { # {{{
local directory_to_flatten="$1"
  local target_dir="$2"

  _zb_downloader_prepare_dir "${target_dir}" || return 1

  local topdir_path=
  local topdir_count=0
  while read -r dir; do
    if [[ -z "$topdir_path" ]]; then
      topdir_path="$dir"
    fi
    (( ++topdir_count ))
  done < <(find "${directory_to_flatten}" -mindepth 1 -maxdepth 1 | head -n 2)

  local source=''
  if [[ -n "${topdir_path}" && "$topdir_count" -eq 1 ]]; then
    source="${topdir_path}"
  else
    source="${directory_to_flatten}"
  fi
  if ! _zb_downloader_move_retry "${source}" "${target_dir}" ; then
    eecho "could not flatten directory ${ZETA_BASH_COLOR_HL}${directory_to_flatten}${ZETA_BASH_COLOR_NONE}."
    rm -Rf "${target_dir}" "${directory_to_flatten}"
    return 1
  fi
  if [[ -d "${directory_to_flatten}" ]] && ! rmdir "${directory_to_flatten}" ; then
    eecho "could not remove directory ${ZETA_BASH_COLOR_HL}${directory_to_flatten}${ZETA_BASH_COLOR_NONE}."
    rm -Rf "${target_dir}" "${directory_to_flatten}"
    return 1
  fi
  return 0
} # }}} [_zb_downloader_flatten_dir]

#
# Create a "create symbolic link" command (windows) or shellscript (other)
#
# The command will then be executed (as elevated prompt or not) to create relevant symbolic link.
#
# @windows on windows, junction and elevated prompt are both supported.
# @param symlink_file name of symbolic link file to be generated; name will be suffixed by platform extension.
# @param (target, link)* the couple of target file/dir and its symbolic link
#
_zb_downloader_create_symbolic_link_file() { # {{{
  local extension=''
  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    extension='.cmd'
  else
    extension='.bash'
  fi

  local symlink_file="${1}${extension}"
  shift

  local -a args=( "$@" )
  local    index=0
  local -a targets=()
  local -a links=()

  # revalidate
  for (( i = 0, n=${#args[@]}; i < n; i += 2, ++index )); do
    local target="${args[i]}"
    local link="${args[i+1]}"
    if [[ -z "${target}" ]]; then
      wecho "won't try to create link to empty target (index: $i)"
      continue
    fi
    if [[ -z "${link}" ]]; then
      wecho "won't try to create link to empty link (target: $target)"
      continue
    fi
    targets[index]="${target}"
    links[index]="${link}"
  done

  if [[ "${index}" == 0 ]]; then
    wecho "no link were created."
    return 0
  fi

  _zb_downloader_prepare_dir "${symlink_file}" || return 1

  # first generate file
  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    local label='symbolic'
    [[ "${OPTIONS_GENERATE_SYMBOLIC_LINKS}" == junction ]] && label=junction

    {
      cat "${SYMLINK_HEADER_FILE}${extension}"
      for (( i = 0; i < index; ++i )); do
        local target="${targets[i]}"
        local link="${links[i]}"
        local cygpath_link='' cygpath_target=''
        if ! cygpath_link="$(cygpath -w "${link}")"  ; then cygpath_link="${link}"  ; fi
        if ! cygpath_target="$(cygpath -w "${target}")" ; then cygpath_target="${target}" ; fi
        echo "  call :${label} \"${cygpath_link}\" \"${cygpath_target}\" "
      done
      cat "${SYMLINK_FOOTER_FILE}${extension}"
    } > "${symlink_file}"
  else
    {
      cat "${SYMLINK_HEADER_FILE}${extension}"
      for (( i = 0; i < index; ++i )); do
        local target="${targets[i]}"
        local link="${links[i]}"
        printf 'ln -f -v -T -s "%q" "%q"\n' "${target}" "${link}"
      done
      cat "${SYMLINK_FOOTER_FILE}${extension}"
    } > "${symlink_file}"
  fi

  local should_execute=no
  for (( i = 0; i < index; ++i )); do
    local target="${targets[i]}"
    local link="${links[i]}"

    if [[ ! -L "${link}" ]]; then should_execute=yes ; break ; fi

    local actual_target=""
    if ! actual_target="$(readlink "${link}")" ; then should_execute=yes ; break ; fi
    if [[ "${actual_target}" != "${target}" ]] ; then should_execute=yes ; break ; fi
  done

  # then execute it
  if [[ "${should_execute}" == no ]]; then
    iecho "not regenerating symbolic links, all seems good."
    iecho "${ZETA_BASH_COLOR_PURPLE}note:${ZETA_BASH_COLOR_NONE} you may use ${ZETA_BASH_COLOR_HL}${symlink_file}${ZETA_BASH_COLOR_NONE} to regenerate them."
    return 0
  fi

  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    unix2dos -q "${symlink_file}"

    local cygpath_symlink_file=''
    if ! cygpath_symlink_file=$(cygpath -w "${symlink_file}") ; then
      cygpath_symlink_file="${symlink_file}"
    fi

    wecho ""
    wecho "The symbolic link are about to be generated."
    wecho ""
    wecho "If the creation fail, you may execute it again in a ${ZETA_BASH_COLOR_PURPLE}DOS Elevated Prompt${ZETA_BASH_COLOR_NONE} "
    wecho "using the following command"
    wecho ""
    wecho "  ${cygpath_symlink_file//\\/\\\\}"
    wecho ""
    if [[ "${OPTIONS_GENERATE_SYMBOLIC_LINKS}" != junction ]]; then
      wecho "You may also try to use NTFS junction instead."
    else
      wecho "If it failed because of NTFS junction not supported, you may try normal symbolic link"
    fi
    wecho ""
    wecho "Note: create a symbolic link requires some specific permissions in Group Strategy: "
    wecho "  https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/create-symbolic-links "
    wecho ""
    local command_runner
    local -a command_opts=()
    if [[ "${OPTIONS_REQUIRE_ELEVATED_PROMPT}" == yes ]]; then
      command_runner=powershell
      command_opts=( "Start-Process -Verb RunAs cmd.exe '/c ${cygpath_symlink_file}'" )
    else
      command_runner=cmd
      command_opts=( '//c' "${cygpath_symlink_file}" )
    fi
    if ! "${command_runner}" "${command_opts[@]}" ; then
      eecho ""
      eecho "you may retry manually using the command below, with appropriate privilege/permissions:"
      eecho ""
      eecho "  ${cygpath_symlink_file//\\/\\\\}"
      eecho ""
    fi
  else
    chmod u+x "${symlink_file}"
    wecho "generating symbolic link for latest"
    if ! "${symlink_file}" ; then
      eecho ""
      eecho "Could not generate symbolic link"
      eecho ""
      eecho "You may retry manually using the command below, with appropriate privilege/permissions:"
      eecho ""
      eecho "  ${symlink_file//\\/\\\\}"
      eecho ""
    fi
  fi
} # }}} [_zb_downloader_create_symbolic_link_file]

#
# Register a version (its archive was downloaded and validated).
#
# @param [ -k key ] enforce key to be something
# @param name friendly name for user, when action are displayed
# @param archive path to archive
# @param symlink name of symlink, can be empty (but must be provided)
# @param dirname name of install directory
# @param homevarname name of XXX_HOME variable (will be uppercased)
#
_zb_downloader_validate_version() { # {{{
  local         key="$1"
  local        name="$2"
  local     archive="$3"
  local     symlink="${4:-}"
  local     dirname="${5:-}"
  local homevarname="${6:-}"

  if [[ -z "$dirname" ]]; then
    dirname="${archive##*/}"
    local re="^(.+)(\.(zip|tar\.[bgx]z|7z))\$"
    [[ "${dirname}" =~ $re ]] && dirname="${BASH_REMATCH[1]}"
  fi

  if [[ -n "${homevarname}" ]]; then
    homevarname="${homevarname//./_}"
    homevarname="${homevarname^^}"
  fi

  downloader_validated_versions["$key"]=yes
  downloader_validated_names["$key"]="${name}"
  downloader_validated_archives["${key}"]="$archive"
  downloader_validated_symlink["${key}"]="$symlink"
  downloader_validated_dirnames["${key}"]="$dirname"
  downloader_validated_env_names["${key}"]="$homevarname"
  return 0
} # }}} [_zb_downloader_validate_version]

_zb_downloader_install_versions() { # {{{
  local key=''
  for key in "${!downloader_validated_versions[@]}"; do
    local friendly_name="${downloader_validated_names[$key]}"

    local target_dirname="${downloader_validated_dirnames[$key]}"
    local install_dir="${CONFIG_INSTALL_DIR}/${target_dirname}"

    local archive_path="${downloader_validated_archives[$key]}"

    if [[ ! -d "${install_dir}" ]]; then
      # this one should already exists
      _zb_downloader_prepare_dir "$install_dir" || return 1

      local install_dir_tmp="${install_dir}.tmp"
      iecho "extracting archive for ${ZETA_BASH_COLOR_HL}${friendly_name}${ZETA_BASH_COLOR_NONE}"
      if ! _zb_downloader_extract_file "${archive_path}" "${install_dir_tmp}" ; then
        eecho "extraction failed."
        continue
      fi
      if ! _zb_downloader_flatten_dir "${install_dir_tmp}" "${install_dir}" ; then
        eecho "could not flatten directory ${ZETA_BASH_COLOR_HL}${install_dir_tmp}${ZETA_BASH_COLOR_NONE}."
        continue
      fi
    fi
    downloader_validated_install_directories[$key]="${install_dir}"
  done
  return 0
} # }}} [_zb_downloader_install_versions]

_zb_downloader_generate_symbolic_links() { # {{{
  for key in "${!downloader_validated_versions[@]}"; do
    local friendly_name="${downloader_validated_names[$key]}"
    local symlink_name="${downloader_validated_symlink[$key]:-}"
    local install_dir="${downloader_validated_install_directories[$key]:-}"

    [[ -z "${symlink_name}" ]] && continue

    if [[ -z "${install_dir}" ]]; then
      wecho "won't create links ${ZETA_BASH_COLOR_HL}${friendly_name}${ZETA_BASH_COLOR_NONE}: previous step failed!"
      continue
    fi

    local symlink_path="${CONFIG_SYMLINK_DIR}/${symlink_name}"

    downloader_validated_symlink_path[$key]="${symlink_path}"

    args+=( "${install_dir}" "${symlink_path}" )
  done

  _zb_downloader_create_symbolic_link_file "${CONFIG_SYMLINK_DIR}/create-link" "${args[@]}"

  return 0
} # }}} [_zb_downloader_generate_symbolic_links]

_downloader_clean() { # {{{
  local -a to_deletes=()

  # we can't use path as key, because they may contains subscript (and escaping is another problem)
  # so, create a find expr instead
  if [[ "${OPTIONS_CLEAN}" == dir || "${OPTIONS_CLEAN}" == all ]]; then
    local -a find_options=()
    find_options+=( "${CONFIG_INSTALL_DIR}" -mindepth 1 -maxdepth 1 )
    find_options+=( -not -path "${CONFIG_ARCHIVES_DIR}/*" )
    find_options+=( -not -path "${CONFIG_SYMLINK_DIR}/*" )
    for path in "${downloader_validated_install_directories[@]}"; do
      find_options+=( -not -path "${path}" )
    done
    find_options+=( -type d )
    if [[ "$#" != 0 ]]; then
      find_options+=( '(' '-false' )
      for name; do
        find_options+=( '-or' '-name' "${name}" )
      done
      find_options+=( ')' )
    fi

    while read -r dir; do
      local dirname="${dir##*/}"
      wecho "will delete install dir: ${ZETA_BASH_COLOR_HL}${dirname}${ZETA_BASH_COLOR_NONE}."
      to_deletes+=( "$dir" )
    done < <(find "${find_options[@]}")
  fi

  if [[ "${OPTIONS_CLEAN}" == archive || "${OPTIONS_CLEAN}" == all ]]; then
    local -a find_options=()
    find_options+=( "${CONFIG_ARCHIVES_DIR}" -mindepth 1 -maxdepth 1 )
    for path in "${downloader_validated_archives[@]}"; do
      find_options+=( -not -path "${path}" )
      find_options+=( -not -path "${path}.sha*" )
    done
    find_options+=( -type f )

    while read -r file; do
      local filename="${file##*/}"
      wecho "will delete file: ${ZETA_BASH_COLOR_HL}${filename}${ZETA_BASH_COLOR_NONE}."
      to_deletes+=( "$file" )
    done < <(find "${find_options[@]}")
  fi

  if [[ "${#to_deletes[@]}" == 0 ]]; then
    iecho "nothing to delete."
    return 0
  fi

  iecho "deleting ${#to_deletes[@]} files"
  if ! rm -Rf "${to_deletes[@]}" ; then
    eecho "unable to clean some file..."
    return 1
  fi

  if [[ "${OPTIONS_CLEAN}" == dir || "${OPTIONS_CLEAN}" == all ]]; then
    local -a broken_links=()
    iecho "cleaning broken links..."
    if ! mapfile -t broken_links < <(find "${CONFIG_SYMLINK_DIR}" -xtype l) ; then
      eecho "could not determine broken links."
      return 1
    else
      iecho "will remove ${#broken_links[@]} broken links."
      if [[ "${#broken_links[@]}" != 0 ]] &&  ! rm "${broken_links[@]}" ; then
        eecho "unable to remove some or all broken links."
        return 1
      fi
    fi
  fi

} # }}} [_downloader_clean]

_downloader_print_export() { # {{{
  iecho 'use the export below to save path to downloaded artifacts'

  echo "# export JQ_EXECUTABLE='${JQ_EXECUTABLE}'"
  echo "# export ${CONFIG_CONTAINER_VARNAME}='${CONFIG_CONTAINER_PATH}'"

  for key in "${!downloader_validated_versions[@]}"; do
    local install_dir="${downloader_validated_install_directories["${key}"]:-}"
    local symlink_path="${downloader_validated_symlink_path["${key}"]:-}"
    local varname="${downloader_validated_env_names["${key}"]:-}"

    [[ -z "${install_dir}" || -z "${varname}" ]] && continue

    local home_value=''
    if [[ -n "${symlink_path}" && -d "${symlink_path}" ]]; then
      home_value="${symlink_path}"
    else
      home_value="${install_dir}"
    fi
    local home_value="\${${CONFIG_CONTAINER_VARNAME}}/${home_value#"${CONFIG_INSTALL_DIR}"/}"
    echo "export ${varname}=\"${home_value}\""
  done
} # }}} [_downloader_print_export]
