# Building your prompt made easy

Zeta Bash provide  a `_zb_ps1` function to build and set up a PS1 prompt: to change your PS1, you must write this in
your environment file:

```bash
export ZETA_BASH_PROMPT_TOKENS='time pwd git java zb_env'
```

This will build PS1 with the following token:

- `time`: time of day.
- `host`: user and host (user@host).
- `pwd`: current working directory.
- `git`: git branch
- `java`: current Java version, using `_zb_java_version`. See [Getting java version from java](java.md).
- `maven` or `mvn`: current Maven version, using `_zb_mvn_version`.
- `node` or `nodejs`: current Maven version, using `_zb_node_version`.
- `zb_env`: current Zeta Bash env.

The following tokens are merged if they are one after another:

- `java`
- `maven`
- `node`

For example, using this:

```bash
# with this tokens, produce:
export ZETA_BASH_PROMPT_TOKENS='time pwd git java maven zb_env'
env-reload

[21:58:27] /e/git/zb/zeta-bash (develop) [1.8.0_131 3.5.0]

# with this tokens, produce:
export ZETA_BASH_PROMPT_TOKENS='time pwd git java zb_env maven'
[21:58:27] /e/git/zb/zeta-bash (develop) [1.8.0_131] [3.5.0]
```

In the second example, the maven version is displayed with a space before the JDK version: this is because the `zb_env`
was set to default.

If we later change env:

```bash
env-reload test
[22:00:47] /e/git/zb/zeta-bash (develop) [1.8.0_131] [test] [3.5.0]
```

**Note:**  it  is  important  to  remember  the primary prompt command (PS1) should not delay **too much** the
readyness  of said prompt. This is the reason that the `java` and `zb_env` tokens are calculed only once (they
change rarely) while the `git` token **may** cost depending on the speed of `__git_ps1`.
