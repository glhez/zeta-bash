# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# usage file
declare CLI_PARSER_USAGE_FILE=''

# saved config header/footer
declare CLI_PARSER_CONFIG_HEADER=''
declare CLI_PARSER_CONFIG_FOOTER=''
declare CLI_PARSER_CONFIG_VARPREFIX='USER_'

# store user args
declare CLI_PARSER_USER_ARGS=''
# if true, consume invalid short arg in CLI_PARSER_USER_ARGS.
declare CLI_PARSER_CONSUME_INVALID_SHORT_ARGS=no

# CLI_PARSER_VARIABLES[variable]
#   variable and its types
# CLI_PARSER_VARIABLES_DESCRIPTION[variable] -> some text
#   a text to describe the variable
# CLI_PARSER_VARIABLES_ENUM_VALUES[variable] -> variable
#   is the associative array variables containing the valid values
# CLI_PARSER_VARIABLES_KEY_TYPES[variable] -> none string enum
#   type of key (none if not an indexed/associative array)
# CLI_PARSER_VARIABLES_VALUE_TYPES[variable] -> string enum bool
#   type of key (none if not an indexed/associative array)
# CLI_PARSER_VARIABLES_SET[variable] -> bool
#   did we set variable?
declare -A CLI_PARSER_VARIABLES=()
declare -A CLI_PARSER_VARIABLES_DESCRIPTION=()
declare -A CLI_PARSER_VARIABLES_ENUM_VALUES=()
declare -A CLI_PARSER_VARIABLES_KEY_TYPES=()
declare -A CLI_PARSER_VARIABLES_VALUE_TYPES=()
declare -A CLI_PARSER_VARIABLES_SET=()

# CLI_PARSER_OPTIONS
#   key is the short/long option with -/--', value is target variable
# CLI_PARSER_OPTIONS_VALUE
# CLI_PARSER_OPTIONS_KEY
#   for an option, its value and it possible key
# CLI_PARSER_OPTIONS_HAS_ARG
#   option requires an arg (no, optional, required)
declare -A CLI_PARSER_OPTIONS=()
declare -A CLI_PARSER_OPTIONS_KEY=()
declare -A CLI_PARSER_OPTIONS_VALUE=()
declare -A CLI_PARSER_OPTIONS_HAS_ARG=()

#
# where to look for usage file
#
# @param usage_file
#
_zb_cliparser_set_usage_file() {
  CLI_PARSER_USAGE_FILE="${1}"
} # }}} [_zb_cliparser_set_usage_file]

#
# header/footer for writing configuration
#
# @param usage_file
#
_zb_cliparser_write_config_header_footer() {
  CLI_PARSER_CONFIG_HEADER="${1:-}"
  CLI_PARSER_CONFIG_FOOTER="${2:-}"
} # }}} [_zb_cliparser_write_config_header_footer]

#
# where to store user args
#
# @param usage_file
#
_zb_cliparser_set_user_args() {
  CLI_PARSER_USER_ARGS="${1}"
  CLI_PARSER_CONSUME_INVALID_SHORT_ARGS="${2:-no}"
  _zb_cliparser_register_option_variable "${1}" "string[]"
}

#
# Register a variable and its possible values
#
# @param variable target variable, where to save
# @param key_type (int, enum or empty) type of key if variable is array or associative array
# @param value_type type of value (string, enum, bool)
# @param enum_values... enum values if key_type is enum or value_type is enum
#
_zb_cliparser_register_option_variable() { # {{{
  local variable="$1"
  local type="$2"

  local key_type='none'
  local value_type=''
  case "$type" in
    'enum[]')         key_type=int    value_type=enum   ;;
    'enum')                           value_type=enum   ;;

    'bool[enum]')     key_type=enum   value_type=bool   ;;
    'bool')                           value_type=bool   ;;

    # value string
    'string[]')       key_type=int    value_type=string ;;
    'string[string]') key_type=string value_type=string ;;
    'string[enum]')   key_type=enum   value_type=string ;;
    'string')                         value_type=string ;;
    *)
      eecho "invalid type ${type}"
      return 1
    ;;
  esac

  CLI_PARSER_VARIABLES[${variable}]="${type}"
  CLI_PARSER_VARIABLES_ENUM_VALUES[${variable}]=""
  CLI_PARSER_VARIABLES_KEY_TYPES[${variable}]="${key_type}"
  CLI_PARSER_VARIABLES_VALUE_TYPES[${variable}]="${value_type}"

  if [[ "${key_type}" == enum || "${value_type}" == enum ]]; then
    shift 2
    local enum_name="${variable}_ENUM_T"
    ! declare -p "${enum_name}" > /dev/null 2> /dev/null && eval "declare -x -g -A ${enum_name}=()"
    CLI_PARSER_VARIABLES_ENUM_VALUES[$variable]="${enum_name}"
    for enum_value; do
      if [[ -n "${enum_value}" ]] ; then
        eval "${enum_name}[\"\${enum_value}\"]=1"
      fi
    done
  fi
} # }}} [_zb_cliparser_register_option_variable]

_zb_cliparser_describe() { # {{{
  local variable="$1"
  local description="$2"
  CLI_PARSER_VARIABLES_DESCRIPTION[${variable}]="${description}"
  return 0
} # }}} [_zb_cliparser_describe]

_zb_cliparser_register_option_map() {
  local variable="$1"
  local option="$2"
  local has_arg="${3}"
  local value="${4:-}"
  local key="${5:-}"

  CLI_PARSER_OPTIONS["${option}"]="${variable}"
  [[ -n "${key}"   ]] && CLI_PARSER_OPTIONS_KEY["${option}"]="${key}"
  [[ -n "${value}" ]] && CLI_PARSER_OPTIONS_VALUE["${option}"]="${value}"
  CLI_PARSER_OPTIONS_HAS_ARG["${option}"]="${has_arg}"
} # [_zb_cliparser_register_option_map]

#
# Map a registered variable (see _zb_cliparser_register_option_variable) with a singular (no arg) option
#
# The encoded option contains both short and long flag:
#
#   short:long:value
#
# if key/value type of variable is enum, then value default to long option
# if key type is enum and value type is bool, then '--no-' variations are generated as well
# if key type is none and value type is bool, then '--no-' variations are generated as well
#
# if value type is bool and value is missing, the value is set to "yes" or "no" depending on flag
# if short is present, it must be one character
#
# @param variable
# @param flag_option...
#
_zb_cliparser_register_option_map_singular() { # {{{
  local variable="$1"
  local type="${CLI_PARSER_VARIABLES[$variable]:-}"

  if [[ -z "${type}" ]] ; then
    eecho "variable ${variable} is not registered. use _zb_cliparser_register_option_variable first."
    return 1
  fi

  local key_type="${CLI_PARSER_VARIABLES_KEY_TYPES[${variable}]}"
  local value_type="${CLI_PARSER_VARIABLES_VALUE_TYPES[${variable}]}"

  shift 1

  for pattern; do
    local opt_short=''
    local opt_long=''
    local opt_value=''
    local opt_neg_value=''
    local opt_key=''

    if ! IFS=':' read -r opt_short opt_long opt_key opt_value <<< "${pattern}"  ; then
      eecho "read failed on pattern ${pattern}"
      return 1
    fi

    if [[ "${#opt_short}" -gt 1 ]]; then
      eecho "short option must be one character wide (variable: $variable)"
      return 1
    fi

    if [[ "${key_type}" == none ]]; then
      [[ -z "${opt_value}" ]] && opt_value="${opt_key}"
      [[ -z "${opt_value}" && "${value_type}" == 'enum' && -n "${opt_long}" ]] && opt_value="${opt_long}"
      [[ -z "${opt_value}" && "${value_type}" == 'bool' ]] && opt_value="yes"

      if [[ -z "${opt_value}" ]]; then
        eecho "could not determine a value to use for $variable and pattern [${pattern}])"
        return 1
      fi

      [[ -n "${opt_short}" ]] && _zb_cliparser_register_option_map "${variable}" "-${opt_short}" no "${opt_value}"
      if [[ -n "${opt_long}" ]]; then
        _zb_cliparser_register_option_map "$variable" "--${opt_long}" no     "${opt_value}"
        if [[ "${value_type}" == bool ]]; then
          if [[ "${opt_value}" == yes ]]; then opt_neg_value=no ; else opt_neg_value=yes ; fi
          _zb_cliparser_register_option_map "$variable" "--no-${opt_long}" no "${opt_neg_value}"
        fi
      fi
    elif [[ "${value_type}" == bool && "${key_type}" == enum ]]; then
      [[ -z "${opt_key}" && -n "${opt_long}" ]] && opt_key="${opt_long}"
      [[ -z "${opt_value}" ]] && opt_value=yes

      if [[ -z "${opt_key}" ]]; then
        eecho "could not determine a key to use for $variable and pattern [${pattern}])"
        return 1
      fi
      if [[ -z "${opt_value}" ]]; then
        eecho "could not determine a value to use for $variable and pattern [${pattern}])"
        return 1
      fi

      [[ -n "${opt_short}" ]] && _zb_cliparser_register_option_map "${variable}" "-${opt_short}" no "${opt_value}" "${opt_key}"
      if [[ -n "${opt_long}" ]]; then
        _zb_cliparser_register_option_map "${variable}" "--${opt_long}"    no "${opt_value}" "${opt_key}"
        if [[ "${opt_value}" == yes ]]; then opt_neg_value=no ; else opt_neg_value=yes ; fi
        _zb_cliparser_register_option_map "${variable}" "--no-${opt_long}" no "${opt_neg_value}" "${opt_key}"
      fi
    elif [[ "${value_type}" == enum && "${key_type}" == int ]]; then
      [[ -z "${opt_value}" && -n "${opt_long}" ]] && opt_value="${opt_long}"
      if [[ -z "${opt_value}" ]]; then
        eecho "could not determine a value to use for $variable and pattern [${pattern}])"
        return 1
      fi

      [[ -n "${opt_short}" ]] && _zb_cliparser_register_option_map "${variable}" "-${opt_short}" no "${opt_value}"
      [[ -n "${opt_long}"  ]] && _zb_cliparser_register_option_map "${variable}" "--${opt_long}" no "${opt_value}"
    else
      eecho "$variable type must be enum[] or enum[bool]"
      return 1
    fi
  done
} # }}} [_zb_cliparser_register_option_map_singular]

_zb_cliparser_validate_enum() {
  local opt_name="$1"
  local enum_type="$2"
  local value="$3"
  local allow_none_all="${4:-no}"

  local -n enum_values="${enum_type}"

  [[ -n "${enum_values[${value}]:-}" ]] && return 0

  if [[ "${allow_none_all}" == no ]]; then
    eecho "unsupported enum value: ${ZETA_BASH_COLOR_RED}${value}${ZETA_BASH_COLOR_NONE} for option ${ZETA_BASH_COLOR_HL}${opt_name}${ZETA_BASH_COLOR_NONE}, expected ${ZETA_BASH_COLOR_HL}${!enum_values[*]}${ZETA_BASH_COLOR_NONE}"
  else
    [[ "${value}" == all || "${value}" == none ]] && return 0
    eecho "unsupported enum value: ${ZETA_BASH_COLOR_RED}${value}${ZETA_BASH_COLOR_NONE} for option ${ZETA_BASH_COLOR_HL}${opt_name}${ZETA_BASH_COLOR_NONE}, expected ${ZETA_BASH_COLOR_HL}${!enum_values[*]}${ZETA_BASH_COLOR_NONE} or none/all"
  fi
  return 1
}

_zb_cliparser_validate_user_enum() {
  local user_variable="$1"
  local enum_type="$2"
  local value="$3"
  local -n enum_values="${enum_type}"

  [[ -n "${enum_values[${value}]:-}" ]] && return 0
  eecho "user configuration ${ZETA_BASH_COLOR_HL}${user_variable}${ZETA_BASH_COLOR_NONE}: unsupported enum value: ${ZETA_BASH_COLOR_RED}${value}${ZETA_BASH_COLOR_NONE}, expected ${ZETA_BASH_COLOR_HL}${!enum_values[*]}${ZETA_BASH_COLOR_NONE} -> ignoring it"
  return 1
}


#
# Set option variable
#
# @param name name of option to set
# @param has_arg (bool) indicate if there was a value to set (or let default)
# @param value a default value
#
_zb_cliparser_set_option() {
  local opt_name="$1"
  local opt_has_arg="$2"
  local opt_value="${3:-}"

  local variable="${CLI_PARSER_OPTIONS[${opt_name}]}"
  local key="${CLI_PARSER_OPTIONS_KEY[${opt_name}]:-}"
  local value="${CLI_PARSER_OPTIONS_VALUE[${opt_name}]:-}"
  local has_arg="${CLI_PARSER_OPTIONS_HAS_ARG[${opt_name}]:-no}"

  local enum_t="${CLI_PARSER_VARIABLES_ENUM_VALUES[${variable}]:-}"
  local key_type="${CLI_PARSER_VARIABLES_KEY_TYPES[${variable}]:-}"
  local value_type="${CLI_PARSER_VARIABLES_VALUE_TYPES[${variable}]:-}"

  if [[ "${key_type}" == none ]]; then
    [[ "${opt_has_arg}" == no ]] && opt_value="${value}"
    if [[ "${value_type}" == enum ]]; then
      ! _zb_cliparser_validate_enum "${opt_name}" "${enum_t}" "${opt_value}" && return 1
    elif [[ "${value_type}" == bool && "${opt_has_arg}" != no ]]; then
      eecho "configuration issue: ${ZETA_BASH_COLOR_RED}${opt_name}${ZETA_BASH_COLOR_NONE} is a bool, it should not have a value."
      return 1
    fi
    local -n target="$variable"
    # shellcheck disable=SC2034
    target="${opt_value}"
  elif [[ "${key_type}" == enum && "${value_type}" == bool ]]; then
    local -n enum_values="${enum_t}"
    local opt_bool_value=yes
    if [[ "${opt_has_arg}" == no ]]; then # option is of form --enum_value or --no-enum_value
      opt_value="${key}"
      opt_bool_value="${value}"
    elif [[ "${opt_value:0:1}" == '-' ]]; then # option is of form --option enum_value
      opt_value="${opt_value:1}"
      opt_bool_value=no
    fi
    ! _zb_cliparser_validate_enum "${opt_name}" "${enum_t}" "${opt_value}" yes && return 1
    local -n target="$variable"
    # if value is all/none, ensure we don't accept it if found in enum_values
    if [[ "${opt_value}" == none && -z "${enum_values[${opt_value}]:-}" || "${opt_value}" == '@none' ]]; then
      for enu in "${!enum_values[@]}"; do target[enu]=no ; done
    elif [[ "${opt_value}" == all && -z "${enum_values[${opt_value}]:-}" || "${opt_value}" == '@all' ]]; then
      for enu in "${!enum_values[@]}"; do target[enu]=yes ; done
    else
      target[opt_value]="${opt_bool_value}"
    fi
  elif [[ "${key_type}" == int  ]]; then
    [[ "${opt_has_arg}" == no ]] && opt_value="${value}"
    if [[ "${value_type}" == enum ]] ; then ! _zb_cliparser_validate_enum "${opt_name}" "${enum_t}" "${opt_value}" && return 1 ; fi
    # shellcheck disable=SC2178
    local -n target="$variable"
    target+=( "${opt_value}" )
  elif [[ ( "${key_type}" == enum || "${key_type}" == string ) && "${value_type}" == string ]]; then
    # reparsing key, eg: -Dfoobar=X and --foobar=aaa=bbb
    if [[ ! "${opt_value}" =~ ^([^=]+)=(.*)$ ]]; then
      eecho "invalid value for option ${ZETA_BASH_COLOR_HL}${opt_name}${ZETA_BASH_COLOR_NONE}, expected key=value"
      return 1
    fi
    local target_key="${BASH_REMATCH[1]}"
    local target_value="${BASH_REMATCH[2]}"

    if [[ "${key_type}" == enum ]] ; then ! _zb_cliparser_validate_enum "${opt_name}" "${enum_t}" "${target_key}" && return 1 ; fi
    # shellcheck disable=SC2178
    local -n target="$variable"
    target["${target_key}"]="${target_value}"
  fi
  CLI_PARSER_VARIABLES_SET["${variable}"]='yes'
}


#
# parse the arg (from command line)
#
_zb_cliparser_parse_options() { # {{{
  local show_usage=no

  # be nice to the user: if there is --help or -h, exit nicely.
  for arg; do
    if [[ "${arg}" == --help || "${arg}" == -h ]]; then
      show_usage=yes
      break
    fi
  done

  if [[ "${show_usage}" == yes ]]; then
    if [[ -n "${CLI_PARSER_USAGE_FILE}" && -f "${CLI_PARSER_USAGE_FILE}" ]]; then
      cat "${CLI_PARSER_USAGE_FILE}"
    else
      eecho "no usage file."
    fi
    return 1
  fi

  local skip_options=no
  local opt_name=''
  local opt_value=''
  local opt_has_arg=no
  local opt_skip_to_next=no

  [[ -n "${CLI_PARSER_USER_ARGS}" ]] && local -n user_args_target="${CLI_PARSER_USER_ARGS}"

  for arg; do
    if [[ "${skip_options}" == yes ]]; then
      [[ -n "${CLI_PARSER_USER_ARGS}" ]] && user_args_target+=( "$arg" )
    elif [[ "${arg}" == -- ]]; then
      skip_options=yes
    else
      if [[ -n "${opt_name}" ]]; then
        opt_skip_to_next=yes
        if [[ "${opt_has_arg}" == required ]]; then
          _zb_cliparser_set_option "${opt_name}" yes "${arg}"
        else
          case "$arg" in
            --*|-?*) _zb_cliparser_set_option "${opt_name}" no ''        ; opt_skip_to_next=no ;;
            *)       _zb_cliparser_set_option "${opt_name}" yes "${arg}" ;;
          esac
        fi
        opt_name=''
        [[ "${opt_skip_to_next}" == yes ]] && continue
      fi

      opt_name=''
      opt_value=''
      opt_has_arg=no

      case "$arg" in
        --*=*)
          opt_name="${arg%%=*}"
          opt_value="${arg#--*=}"
          opt_has_arg=yes
        ;;
        --*)
          opt_name="${arg}"
          opt_value=''
          opt_has_arg=no
        ;;
        -?*)
          opt_name="${arg:0:2}"
          opt_value="${arg:3}"
          [[ "${#arg}" -gt 2 ]] && opt_has_arg=yes
        ;;
        *) [[ -n "${CLI_PARSER_USER_ARGS}" ]] && user_args_target+=( "$arg" ) ;;
      esac

      if [[ -n "${opt_name}" ]]; then
        if [[ -z "${CLI_PARSER_OPTIONS[${opt_name}]:-}" ]]; then
          if [[ -n "${CLI_PARSER_USER_ARGS}" && "${CLI_PARSER_CONSUME_INVALID_SHORT_ARGS}" == yes  ]]; then
            user_args_target+=( "$arg" )
            opt_name=''
            continue
          fi
          eecho "unknown option: ${ZETA_BASH_COLOR_RED}${opt_name}${ZETA_BASH_COLOR_NONE} ; accepted: ${ZETA_BASH_COLOR_HL}${!CLI_PARSER_OPTIONS[*]}${ZETA_BASH_COLOR_NONE}."
          return 1
        fi

        if [[ "${opt_has_arg}" == yes && "${CLI_PARSER_OPTIONS_HAS_ARG[$opt_name]}" == no ]]; then
          eecho "option ${ZETA_BASH_COLOR_HL}${opt_name}${ZETA_BASH_COLOR_NONE} does not accept parameter."
          return 1
        fi
        # wait till next arg to get value
        if [[ "${opt_has_arg}" == no && "${CLI_PARSER_OPTIONS_HAS_ARG[$opt_name]}" != no ]]; then
          opt_has_arg="${CLI_PARSER_OPTIONS_HAS_ARG[$opt_name]}"
          continue
        fi
        _zb_cliparser_set_option "${opt_name}" "${opt_has_arg}" "${opt_value}"
        opt_name=''
      fi
    fi
  done

  if [[ -n "${opt_name}" ]]; then
    if [[ "${opt_has_arg}" == required ]]; then
      eecho "parameter ${ZETA_BASH_COLOR_HL}${opt_name}${ZETA_BASH_COLOR_NONE} required a value."
      return 1
    fi
    _zb_cliparser_set_option "${opt_name}" no
  fi
  return 0
} # }}} [_zb_cliparser_parse_options]


_zb_cliparser_display_options() { # {{{
  echo "configured variables:"
  for variable in "${!CLI_PARSER_VARIABLES[@]}" ; do
    local type="${CLI_PARSER_VARIABLES[${variable}]}"
    local key_type="${CLI_PARSER_VARIABLES_KEY_TYPES[${variable}]}"
    local value_type="${CLI_PARSER_VARIABLES_VALUE_TYPES[${variable}]}"
    local is_set="${CLI_PARSER_VARIABLES_SET[$variable]:-no}"

    if [[ "${key_type}" == enum || "${value_type}" == enum ]]; then
      local -n values="${CLI_PARSER_VARIABLES_ENUM_VALUES[$variable]}"
      echo "${type};${variable};${is_set};${!values[*]}"
    else
      echo "${type};${variable};${is_set}"
    fi
  done | sort -k2 '-t;' | column -t -s ';' --table-columns 'Type,Name,User defined?,Values (Enum)'

  echo "configured options:"
  for option in "${!CLI_PARSER_OPTIONS[@]}" ; do
    local variable="${CLI_PARSER_OPTIONS[$option]}"
    local key="${CLI_PARSER_OPTIONS_KEY[$option]:-}"
    local value="${CLI_PARSER_OPTIONS_VALUE[$option]:-}"
    local has_arg="${CLI_PARSER_OPTIONS_HAS_ARG[$option]}"
    echo "$option;$variable;$has_arg;$key;$value"
  done | sort -k2 -k3 '-t;' | column -t -s ';' --table-columns 'Option,Name,Has arg,Key,Value'
  echo "__ END selected options __"
} # }}} [_zb_cliparser_display_options]

_zb_cliparser_write_config() { # {{{
  local config_file="$1"
  local config_dir="${config_file%/*}"

  if [[ "${config_dir}" != "${config_file}" && -n "${config_dir}" ]]; then
    if [[ ! -d "${config_dir}" ]] && ! mkdir -p "${config_dir}" ; then
      eecho "could not create parent directories of ${ZETA_BASH_COLOR_HL}${config_dir}${ZETA_BASH_COLOR_NONE}"
      return 1
    fi
  fi

  local -a variables=()
  local    variable=''
  while read -r variable; do
    variables+=( "$variable" )
  done < <(for variable in "${!CLI_PARSER_VARIABLES_DESCRIPTION[@]}"; do echo "${variable}" ; done | sort )

  iecho "saving configuration to ${ZETA_BASH_COLOR_HL}${config_file}${ZETA_BASH_COLOR_NONE}."
  {
    if [[ -n "${CLI_PARSER_CONFIG_HEADER}" && -f "${CLI_PARSER_CONFIG_HEADER}" ]] ; then
      cat "${CLI_PARSER_CONFIG_HEADER}"
    else
      echo '#!/bin/bash'
      echo ''
      echo '#'
      echo "# This script contains options for ${0##*/} utility"
      echo '#'
      echo '# You may edit it, however calling --save-config will overwrite'
      echo '# this file'
      echo '#'
      echo ''
    fi

    for variable in "${variables[@]}"; do
      local user_variable="${CLI_PARSER_CONFIG_VARPREFIX}${variable}"
      local value=''
      [[ "${CLI_PARSER_VARIABLES_SET[$variable]:-no}" == no && ( -z "${CLI_PARSER_USER_ARGS}" || "${variable}" != "${CLI_PARSER_USER_ARGS}" ) ]] && continue

      local description="${CLI_PARSER_VARIABLES_DESCRIPTION[$variable]:-}"
      local enum_type="${CLI_PARSER_VARIABLES_ENUM_VALUES[$variable]:-}"
      local key_type="${CLI_PARSER_VARIABLES_KEY_TYPES[$variable]:-}"
      local value_type="${CLI_PARSER_VARIABLES_VALUE_TYPES[$variable]:-}"

      echo '#'
      echo "# ${user_variable}"
      echo '#'

      if [[ -n "${description}" ]]; then
        while read -r line; do
          echo "# ${line}"
        done <<< "${description}"
      fi

      if [[ -n "${value_type}" ]]; then
        echo '#'
        echo "# @type ${value_type}[${key_type}]"
        if [[ "${key_type}" == enum || "${value_type}" == enum ]]; then
          local -n enum_values="${enum_type}"
          echo "# @values {${!enum_values[*]}}"
        fi
      fi
      echo '#'
      if value=$(declare -p "${variable}" 2> /dev/null); then
        echo "$value" | sed -E -e "s@^(declare.+)(${variable})@\1${CLI_PARSER_CONFIG_VARPREFIX}\2@g" -e 's@^declare@declare -g @g'
      else
        echo "# declare ${user_variable}"
      fi
      echo ''
    done
    [[ -n "${CLI_PARSER_CONFIG_FOOTER}" && -f "${CLI_PARSER_CONFIG_FOOTER}" ]] && cat "${CLI_PARSER_CONFIG_FOOTER}"
  } > "${config_file}"
  return 0
} # }}} [_zb_cliparser_write_config]

_zb_cliparser_read_config() { # {{{
  local config_file="$1"
  shift

  [[ ! -f "${config_file}" ]] && return 0

  iecho "reading configuration ${ZETA_BASH_COLOR_HL}${config_file}${ZETA_BASH_COLOR_NONE}"

  # shellcheck disable=SC1090
  source "${config_file}"

  for variable in "${!CLI_PARSER_VARIABLES_DESCRIPTION[@]}"; do
    local name="${CLI_PARSER_CONFIG_VARPREFIX}${variable}"

    # note: we could use ${!name+x} ... but it seems to fails with array/map
    if declare -p "${name}" 2> /dev/null 1>&2 ; then
      _zb_cliparser_read_config_set "${variable}" "${name}"
      CLI_PARSER_VARIABLES_SET["${variable}"]='yes'
    fi
  done

  return 0
} # }}} [_zb_cliparser_read_config]


# shellcheck disable=SC2178
_zb_cliparser_read_config_set() { # {{{
  local variable="$1"
  local user_variable="$2"

  local -n user_value_content="${user_variable}"

  local    enum_t="${CLI_PARSER_VARIABLES_ENUM_VALUES[${variable}]:-}"
  local    key_type="${CLI_PARSER_VARIABLES_KEY_TYPES[${variable}]:-}"
  local    value_type="${CLI_PARSER_VARIABLES_VALUE_TYPES[${variable}]:-}"

  if [[ "${key_type}" == none ]]; then
    if [[ "${value_type}" == enum ]]; then
      # ignore error
      ! _zb_cliparser_validate_user_enum "${user_variable}" "${enum_t}" "${user_value_content}" && return 0
    fi
    local -n target="${variable}"
    target="${user_value_content}"
  elif [[ "${key_type}" == enum && "${value_type}" == bool ]]; then
    local -n enum_values="${enum_t}"

    local -n target="${variable}"
    target=()
    for user_key in "${!user_value_content[@]}"; do
      if _zb_cliparser_validate_user_enum "${user_variable}" "${enum_t}" "${user_key}" ; then
        if [[ "${user_value_content[${user_key}]:-no}" == no ]]; then
          target["${user_key}"]="no"
        else
          target["${user_key}"]="yes"
        fi
      fi
    done
  elif [[ "${key_type}" == int && "${value_type}" == enum ]]; then
    local -n enum_values="${enum_t}"
    local -n target="${variable}"
    target=()
    for user_value in "${user_value_content[@]}"; do
      if _zb_cliparser_validate_user_enum "${user_variable}" "${enum_t}" "${user_value}" ; then
        target+=( "${user_value}" )
      fi
    done
  elif [[ "${key_type}" == int ]]; then
    local -n target="${variable}"
    target=( "${user_value_content[@]}" )
  elif [[ "${key_type}" == enum && "${value_type}" == string ]]; then
    local -n enum_values="${enum_t}"

    local -n target="${variable}"
    target=()
    for user_key in "${!user_value_content[@]}"; do
      if _zb_cliparser_validate_user_enum "${user_variable}" "${enum_t}" "${user_key}" ; then
        target["${user_key}"]="${user_value_content[${user_key}]}"
      fi
    done
  elif [[ "${key_type}" == string ]]; then
    local -n target="${variable}"
    target=()
    for user_key in "${!user_value_content[@]}"; do
      target["${user_key}"]="${user_value_content[${user_key}]}"
    done
  else
    eecho "unsupported key/value type : ${key_type}[${value_type}]"
    return 1
  fi
  return 0
} # }}} [_zb_cliparser_read_config_set]


