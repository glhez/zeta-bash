#!/bin/bash

declare SCRIPT_FILE
if ! SCRIPT_FILE=$(realpath "$0"); then
  echo "realpath is skrewed !"
  exit 1
fi
declare SCRIPT_DIR="${SCRIPT_FILE%/*}"

export ZETA_BASH_HOME="${SCRIPT_DIR%/*}"
export ZETA_BASH_USER_HOME="${ZETA_BASH_HOME%/*}/zeta-bash-user"

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/completion.bash
source "${ZETA_BASH_HOME}/lib/completion.bash"
# shellcheck source=tests/assertions.bash
source "${ZETA_BASH_HOME}/tests/assertions.bash"

if ! _zb_utils_function_exists _get_comp_words_by_ref ; then
  _get_comp_words_by_ref() {
    : # do nothing, because we are mocking it
  }
fi


declare -a -g COMPREPLY=()
_zb_complete_match_prefix val value values valery realpath
assert_array_equals "COMPREPLY should have 3 values" COMPREPLY value values valery

COMPREPLY=()
_zb_complete_match_prefix2 -p --application -- --application a b
assert_array_equals "COMPREPLY should have 2 prefixed entries" COMPREPLY --application=a --application=b

COMPREPLY=()
_zb_complete_match_prefix2 -p --application -s: -- --application a b
assert_array_equals "COMPREPLY should have 3 prefixed entries" COMPREPLY --application:a --application:b


declare temp_dir
if ! temp_dir=$(mktemp --directory); then
  eecho "could not create a temp directory for this test."
fi
temp_dir+="/"
touch "${temp_dir}"{file1,file2,foobar1,acme}.bash

iecho "using ${ZETA_BASH_COLOR_HL}${temp_dir}${ZETA_BASH_COLOR_NONE} as directory"

COMPREPLY=()
_zb_complete_filelist col "${temp_dir}" ".bash"
assert_array_equals "COMPREPLY should have 0 entry" COMPREPLY

COMPREPLY=()
_zb_complete_filelist file "${temp_dir}" ".bash"
assert_array_equals "COMPREPLY should have 2 entries" COMPREPLY file1 file2

COMPREPLY=()
_zb_complete_filelist acme "${temp_dir}" ".bash"
assert_array_equals "COMPREPLY should have 0 entry (exact match)" COMPREPLY

COMPREPLY=()
_zb_complete_filelist acm "${temp_dir}" ".bash"
assert_array_equals "COMPREPLY should have 1 entry" COMPREPLY acme

iecho "checking completions files..."
declare completion='' status=0
while read -r completion; do
  _zb_completion_loader "$completion"
  status=$?
  if [[ $status == 124 ]] ; then
    report_success "status: ${ZETA_BASH_COLOR_GREEN}$status${ZETA_BASH_COLOR_NONE} cmdspec: ${ZETA_BASH_COLOR_HL}${ZETA_BASH_COMPLETION_LAST_CMDSPEC}${ZETA_BASH_COLOR_NONE} file: ${ZETA_BASH_COLOR_HL}${ZETA_BASH_COMPLETION_LAST_FILE}${ZETA_BASH_COLOR_NONE} dynamic: ${ZETA_BASH_COMPLETION_LAST_DYNAMIC}"
  else
    report_failure    "status: ${ZETA_BASH_COLOR_RED}$status${ZETA_BASH_COLOR_NONE} cmdspec: ${ZETA_BASH_COLOR_HL}${ZETA_BASH_COMPLETION_LAST_CMDSPEC}${ZETA_BASH_COLOR_NONE} file: ${ZETA_BASH_COLOR_HL}${ZETA_BASH_COMPLETION_LAST_FILE}${ZETA_BASH_COLOR_NONE} dynamic: ${ZETA_BASH_COMPLETION_LAST_DYNAMIC}"
  fi
done < <(
  find "${ZETA_BASH_HOME}/completions" -type f -name '*.completion.bash' -print0 | \
    xargs -r0 grep -hP '^\s*complete\s+' | \
    sed -E -e 's@#.+$@@g' -e 's@^\s*complete\s+.+\s+(\S+)$@\1@g' | \
    sort --unique
  )


#ZETA_BASH_COMPLETION_LAST_CMDSPEC
#ZETA_BASH_COMPLETION_LAST_FILE
#ZETA_BASH_COMPLETION_LAST_DYNAMIC
