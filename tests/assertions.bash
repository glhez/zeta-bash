# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

declare -x ZETA_BASH_ASSERTIONS_LOG_PREFIX=''

aecho() {
  _zb_log 0 error 'test ' "${ZETA_BASH_COLOR_PURPLE}" "${ZETA_BASH_ASSERTIONS_LOG_PREFIX}$*" 2>&1 | tee -a "${SUITE_LOG_FILE}"
}

handler_equals()     { local actual="$1" expected="$2" ; [[ "$actual" ==  "$expected" ]]; }
handler_not_equals() { local actual="$1" expected="$2" ; [[ "$actual" !=  "$expected" ]]; }
handler_eq()         { local actual="$1" expected="$2" ; [[ "$actual" -eq "$expected" ]]; }
handler_gt()         { local actual="$1" expected="$2" ; [[ "$actual" -gt "$expected" ]]; }
handler_lt()         { local actual="$1" expected="$2" ; [[ "$actual" -lt "$expected" ]]; }
handler_ge()         { local actual="$1" expected="$2" ; [[ "$actual" -ge "$expected" ]]; }
handler_le()         { local actual="$1" expected="$2" ; [[ "$actual" -le "$expected" ]]; }
handler_ne()         { local actual="$1" expected="$2" ; [[ "$actual" -ne "$expected" ]]; }
handler_empty()      { local actual="$1"  ; [[ -z "$actual"       ]]; }
handler_not_empty()  { local actual="$1"  ; [[ -n "$actual"       ]]; }
# we use true/false rather than 0 and !0
handler_true()       { local actual="$1"  ; [[ "$actual" == yes  ]]; }
handler_false()      { local actual="$1"  ; [[ "$actual" == no   ]]; }

report_success() {
  [[ "${SILENT:-0}" != 1 ]] && aecho "${ZETA_BASH_COLOR_GREEN}[OK]${ZETA_BASH_COLOR_NONE} $*"
}
report_failure() {
  [[ "${SILENT:-0}" != 1 ]] && aecho "${ZETA_BASH_COLOR_RED}[KO]${ZETA_BASH_COLOR_NONE} $*"
  _zb_utils_print_stacktrace2 aecho "$*"
  exit 1
}

assert0_format() {
  local -n target="$1"
  local message="$2"
  local expected="$3"
  local actual="$4"

  target=""
  for word in ${message} ; do
    [[ -n "${target}" ]] && target+=' '
    case "$word" in
      *'%actual'*)
        target+="${ZETA_BASH_COLOR_PURPLE}${actual}${ZETA_BASH_COLOR_NONE}"
      ;;
      *'%expected'*)
        target+="${ZETA_BASH_COLOR_PURPLE}${expected}${ZETA_BASH_COLOR_NONE}"
      ;;
      *)           target+="$word"
    esac
  done
}

#
# Test some value against another, using an operator like -eq
#
# @param message message to be added, describing the test
# @param expected expected value
# @param actual actual value
# @param op operator to be used, must be a valid 'if' binary operator (see man bash)
#
assert0() {
  local message=$1
  local handler=$2
  local expected=$3
  local actual=$4

  local message_expected=''
  if "$handler" "$actual" "$expected"; then
    if [[ "${SILENT:-0}" != 1 ]]; then
      assert0_format message_expected "${message}" "${expected}" "${actual}"
      report_success "${message_expected}"
    fi
    return 0
  fi

  local -A formats=()
  formats[handler_equals]='%s == %s'
  formats[handler_not_equals]='%s != %s'

  # boolean
  formats[handler_true]='1;%s is true '
  formats[handler_false]='1;%s is false'

  # numeric
  formats[handler_eq]='(int) %s == %s'
  formats[handler_ne]='(int) %s != %s'
  formats[handler_gt]='(int) %s > %s'
  formats[handler_lt]='(int) %s < %s'
  formats[handler_ge]='(int) %s >= %s'
  formats[handler_le]='(int) %s <= %s'

  # empty
  formats[handler_empty]='1;%s must be empty'
  formats[handler_not_empty]='1;%s must not be empty'

  assert0_format message_expected "${message}" "${expected}" "${actual}"

  _zb_utils_print_stacktrace2 aecho "${ZETA_BASH_COLOR_RED}assertion failed:${ZETA_BASH_COLOR_NONE} ${message_expected}"
  aecho "${ZETA_BASH_COLOR_RED}for expression:${ZETA_BASH_COLOR_NONE}"
  if [[ -z "${formats[$handler]}" ]]; then
    aecho "  | operator: <${handler}>"
    aecho "  | expected: <${expected}>"
    aecho "  | actual  : <${actual}>"
  else
    local format="${formats[$handler]}"
    local formatted=''
    if [[ "${format:0:2}" == "1;" ]]; then
      # shellcheck disable=SC2059
      printf -v formatted "${format:2}" "<${actual}>"
    else
      # shellcheck disable=SC2059
      printf -v formatted "${format}" "<${expected}>" "<${actual}>"
    fi
    aecho "  | $formatted"
  fi

  exit 1
}

#                     assert0 message handler            expected    actual
assert_equals()     { assert0 "$1"    handler_equals     "$2"        "$3"   ; }
assert_not_equals() { assert0 "$1"    handler_not_equals "$2"        "$3"   ; }
assert_eq()         { assert0 "$1"    handler_eq         "$2"        "$3"   ; }
assert_gt()         { assert0 "$1"    handler_gt         "$2"        "$3"   ; }
assert_lt()         { assert0 "$1"    handler_lt         "$2"        "$3"   ; }
assert_ge()         { assert0 "$1"    handler_ge         "$2"        "$3"   ; }
assert_le()         { assert0 "$1"    handler_le         "$2"        "$3"   ; }
assert_ne()         { assert0 "$1"    handler_ne         "$2"        "$3"   ; }
assert_empty()      { assert0 "$1"    handler_empty      "empty"     "$2"   ; }
assert_not_empty()  { assert0 "$1"    handler_not_empty  "not empty" "$2"   ; }
assert_true()       { assert0 "$1"    handler_true       "yes"       "$2"   ; }
assert_false()      { assert0 "$1"    handler_false      "no"        "$2"   ; }

assert_not_set() {
  local variable="$1"

  if declare -p "${variable}" 2> /dev/null 1>&2; then
    report_failure "variable ${variable} is set"
  else
    report_success "variable ${variable} is not set"
  fi
}

report_line_diff() {
  local index="$1"
  local expected="$2"
  local actual="$3"
  if [[ "$expected" == "$actual" ]]; then
    return 1
  fi
  local n="${#expected}"
  if [[ "$n" -lt "${#actual}" ]]; then
    n="${#actual}"
  fi

  local diff_line=""
  for (( i = 0; i < n; ++i )); do
    if [[ "${expected:$i:1}" == "${actual:$i:1}" ]]; then
      diff_line+='.'
    else
      diff_line+='#'
    fi
  done
  aecho "  | [$index]:"
  aecho "  |   expected: |${expected}|"
  aecho "  |   diff    : |${diff_line}|"
  aecho "  |   actual  : |${actual}|"

}

#
# test if some array equals another, using = and same order
#
# @param name name of array
# @param expected... expected array
#
assert_array_equals() {
  local name="$1"
  SILENT=1 assert_not_empty "name must be present" "$name"
  shift

  local eval_value
  eval "eval_value=\${#${name}[@]}"

  SILENT=1 assert_equals "$name length must be %expected" "${#}" "${eval_value}"
  local i=0 failed=-1
  for expected; do
    eval "eval_value=\${${name}[$i]}"

    if [[ "$expected" != "${eval_value}" ]]; then
      failed=$i
      break
    fi
    (( ++i ))
  done

  if [[ "${failed}" != -1 ]]; then
    _zb_utils_print_stacktrace2 aecho "${ZETA_BASH_COLOR_RED}assertion failed:${ZETA_BASH_COLOR_NONE} value mismatch at index ${failed}"
    aecho "${ZETA_BASH_COLOR_RED}index below fail assertion:${ZETA_BASH_COLOR_NONE}"
    local i=0
    local failed_count=0
    for expected; do
      eval "eval_value=\${${name}[$i]:-}"
      if report_line_diff "$i" "$expected" "${eval_value}" ; then
        (( ++failed_count ))
        if [[ "$failed_count" -gt 10 ]]; then
          break # avoid big "lines"
        fi
      fi
      (( ++i ))
    done
    exit 1
  fi
  report_success "array ${name}"
}

#
# test if some array equals another, using = and not considering order
#
# @param name name of array
# @param expected... expected array
#
assert_array_equals_unordered() {
  local name="$1"
  SILENT=1 assert_not_empty "name must be present" "$name"
  shift

  local eval_value
  eval "eval_value=\${#${name}[@]}"

  SILENT=1 assert_equals "$name length must be %expected" "${#}" "${eval_value}"
  local i=0
  for expected; do
    eval "eval_value=\${${name}[$i]}"
    SILENT=1 assert_equals "${name}[$i]" "$expected" "${eval_value}"
    (( ++i ))
  done
  report_success "array ${name}"
}

#
# verify if provided associative array equals elements provided
#
# @param name name of associative array
# @param key=value... a couple' of key/value
#
assert_map_equals() {
  local name="$1"
  SILENT=1 assert_not_empty "name must be present" "$name"
  shift

  local eval_value
  eval "eval_value=\${#${name}[@]}"

  local expected_keys=$#

  SILENT=1 assert_equals "$name length must be %expected, was %actual" "${expected_keys}" "${eval_value}"

  for key_value; do
    local key="${key_value%%=*}"
    local value="${key_value#*=}"
    eval "eval_value=\${${name}[\${key}]:-}"
    SILENT=1 assert_equals "${name}[${key@Q}]" "${value}" "${eval_value}"
  done
  report_success "map equals ${name} <$*>"
}

#
# Assert that some command produce the expected result.
#
# @param command... command and its options
# @param -- separate command from expected output; if the '--' is part of command, it may be prefixed by \.
# @param lines... expected lines
#
assert_command_output() {
  local -a command_args=()
  local -a expected_lines=()

  local catch_command=1
  for arg; do
    if [[ "$catch_command" == 1 ]]; then
      if [[ "$arg" != '--' ]]; then
        if [[ "$arg" == '\--' ]]; then
          command_args+=( "--" )
        else
          command_args+=( "$arg" )
        fi
      else
        catch_command=0
      fi
    else
      expected_lines+=( "$arg" )
    fi
  done

  local -a actual_lines=()

  if ! mapfile -t actual_lines < <("${command_args[@]}") ; then
    report_failure "command execution failed: ${ZETA_BASH_COLOR_PURPLE}${command_args[*]}${ZETA_BASH_COLOR_NONE}"
  fi

  SILENT=1 assert_equals "command did not produce %expected lines" "${#expected_lines[@]}" "${#actual_lines[@]}"
  local i=0 failed=-1
  for (( i = 0, n=${#expected_lines[@]}; i < n; ++i )); do
    if [[ "${expected_lines[$i]}" != "${actual_lines[$i]}" ]]; then
      failed=$i
      break
    fi
  done

  if [[ "${failed}" != -1 ]]; then
    _zb_utils_print_stacktrace2 aecho "${ZETA_BASH_COLOR_RED}assertion failed:${ZETA_BASH_COLOR_NONE} command ${ZETA_BASH_COLOR_PURPLE}${command_args[*]}${ZETA_BASH_COLOR_NONE} output mismatch at line ${failed}"
    aecho "${ZETA_BASH_COLOR_RED}lines below also fail assertion:${ZETA_BASH_COLOR_NONE}"

    local failed_count=0
    for (( i = 0, n=${#expected_lines[@]}; i < n; ++i )); do
      if report_line_diff "$i" "${expected_lines[$i]}" "${actual_lines[$i]}" ; then
        (( ++failed_count ))
        if [[ "$failed_count" -gt 10 ]]; then
          break # avoid big "lines"
        fi
      fi
    done
    exit 1
  fi
  report_success "result of command: ${ZETA_BASH_COLOR_PURPLE}${command_args[*]}${ZETA_BASH_COLOR_NONE}"
}

hash_to_array() {
  local -n hash="$1"
  local -n result="$2"
  for key in "${!hash[@]}"; do
    result+=( "$key" "${hash[$key]}" )
  done
}

assert_should_fail() {
  local message="$1"
  shift 1
  if "$@" 1>> "${SUITE_OUT_FILE}" 2>> "${SUITE_ERR_FILE}" ; then
    report_failure "${message} (invocation should have failed)"
  fi
  report_success "${message}"
}

assert_should_success() {
  local message="$1"
  shift 1
  if ! "$@" 1>> "${SUITE_OUT_FILE}" 2>> "${SUITE_ERR_FILE}" ; then
    report_failure "${message} (invocation failed)"
  fi
  report_success "${message}"
}

assert_execute() {
  if ! "$@" 1>> "${SUITE_OUT_FILE}" 2>> "${SUITE_ERR_FILE}" ; then
    report_failure "${*}"
  else
    report_success "${*}"
  fi
}

assert_file_equals() {
  local actual_file="$1"
  local expected_file="$2"
  if ! diff --strip-trailing-cr "${actual_file}" "${expected_file}" ; then
    report_failure "diff --strip-trailing-cr '${actual_file}' '${expected_file}'"
  else
    report_success "diff --strip-trailing-cr '${actual_file}' '${expected_file}'"
  fi
}

# @deprecated
run_tests() {
  local dir="$1"
  shift
  for arg; do
    pushd "$dir" > /dev/null || report_failure "could not cd to ${dir}"
    aecho "running test ${ZETA_BASH_COLOR_PURPLE}${arg}${ZETA_BASH_COLOR_NONE} in ${ZETA_BASH_COLOR_HL}${PWD}${ZETA_BASH_COLOR_NONE}"
    "$arg" || aecho "test ${ZETA_BASH_COLOR_PURPLE}${arg}${ZETA_BASH_COLOR_NONE} failed"
    popd > /dev/null || report_failure "could not cd from ${dir}"
  done
}

invoke_callback() {
  local callback="$1"
  if [[ -n "${callback}" ]] ; then
    aecho ">> ${ZETA_BASH_COLOR_YELLOW}${callback}${ZETA_BASH_COLOR_NONE} "
    if "${callback}" ; then
      aecho "<< ${ZETA_BASH_COLOR_YELLOW}${callback}${ZETA_BASH_COLOR_NONE}"
    else
      report_failure "${callback}"
    fi
  fi
  return 0
}

run_test_suite() {
  local suite="$1"
  shift 1

  suite="${suite%.tests.bash}"

  local -a user_args=()
  local opt_display_list=no
  for arg; do
    case "$arg" in
      --list) opt_display_list=yes  ;;
      --*)    eecho "no an option: ${arg}" ; return 1 ;;
      -?)     eecho "no an option: ${arg}" ; return 1 ;;
      *) user_args+=( "$arg" )
    esac
  done

  local    before_all_callback=''
  local    after_all_callback=''
  local    set_up_callback=''
  local    tear_down_callback=''
  local -a possible_tests_to_run=()

  while read -r name; do
    case "${name,,}" in
      beforeall) before_all_callback="$name" ;;
      afterall)  after_all_callback="$name"  ;;
      setup)     set_up_callback="$name"     ;;
      teardown)  tear_down_callback="$name"  ;;
      *)
        if [[ "${#user_args[@]}" == 0 ]]; then
          possible_tests_to_run+=( "$name" )
        else
          mapfile -t -O "${#possible_tests_to_run[@]}" possible_tests_to_run < <(compgen -W "${user_args[*]}" -P test_ "${name#test_}")
          mapfile -t -O "${#possible_tests_to_run[@]}" possible_tests_to_run < <(compgen -W "${user_args[*]}" "${name}" )
        fi
      ;;
    esac
  done < <( declare -F | grep -P '^declare\s+-f\s+(beforeAll|afterAll|setUp|tearDown|test_\S+)' | sed -E -e 's@^declare\s+-f\s+@@g' )

  local -A tests_to_run=()
  for name in "${possible_tests_to_run[@]}" ; do
    tests_to_run["${name}"]=not_ran
  done

  if [[ "${opt_display_list}" == yes ]]; then
    if [[ "${#user_args[@]}" == 0 ]]; then
      iecho "available test case in ${ZETA_BASH_COLOR_PURPLE}${suite}${ZETA_BASH_COLOR_NONE}"
    else
      iecho "available test case in ${ZETA_BASH_COLOR_PURPLE}${suite}${ZETA_BASH_COLOR_NONE} and matching ${ZETA_BASH_COLOR_HL}${user_args[*]}${ZETA_BASH_COLOR_NONE}"
    fi
    for test_case in "${!tests_to_run[@]}"; do
      iecho " - ${test_case}"
    done
    iecho "run specific test by using theses names (prefix 'test_' can be ignored)"
    return 1
  fi

  if [[ "${#tests_to_run[@]}" == 0 ]]; then
    iecho "no test to run."
    return 1
  fi

  set -eu

  local TEST_TMP_DIR="${ZETA_BASH_HOME}/tmp"
  local TEST_LOG_DIR="${ZETA_BASH_HOME}/logs/tests"
  declare -gx SUITE_LOG_FILE="${TEST_LOG_DIR}/${suite%.tests.bash}.log"
  declare -gx SUITE_OUT_FILE="${TEST_LOG_DIR}/${suite%.tests.bash}.out"
  declare -gx SUITE_ERR_FILE="${TEST_LOG_DIR}/${suite%.tests.bash}.err"

  if [[ ! -d "${TEST_LOG_DIR}" ]] && ! mkdir -p "${TEST_LOG_DIR}" ; then
    eecho "could not create log directory: ${TEST_LOG_DIR}"
    exit 1
  fi
  if [[ ! -d "${TEST_TMP_DIR}" ]] && ! mkdir -p "${TEST_TMP_DIR}" ; then
    eecho "could not create tmp directory: ${TEST_TMP_DIR}"
    exit 1
  fi

  rm -f "${SUITE_LOG_FILE}" "${SUITE_OUT_FILE}" "${SUITE_ERR_FILE}"

  aecho "executing suite: ${ZETA_BASH_COLOR_PURPLE}${suite}${ZETA_BASH_COLOR_NONE}"

  invoke_callback "${before_all_callback}"
  for test_case in "${!tests_to_run[@]}"; do
    local dir="${TEST_TMP_DIR}/${suite}/${test_case}"
    if [[ ! -d "${dir}" ]] && ! mkdir -p "${dir}" ; then
      report_failure "could not create directory [${dir}]."
    fi
    pushd "$dir" > /dev/null || report_failure "could not cd to ${dir}"
      echo ">>> ${test_case}" >> "${SUITE_OUT_FILE}"
      echo ">>> ${test_case}" >> "${SUITE_ERR_FILE}"

      aecho "-- executing ${ZETA_BASH_COLOR_PURPLE}${test_case}${ZETA_BASH_COLOR_NONE} --"
      ZETA_BASH_ASSERTIONS_LOG_PREFIX="${test_case} | "

      {
        invoke_callback "${set_up_callback}"
        invoke_callback "${test_case}"
        invoke_callback "${tear_down_callback}"
      } &
      local test_pid=$!
      if ! wait "${test_pid}" ; then
        tests_to_run[${test_case}]=failed
      else
        tests_to_run[${test_case}]=success
      fi
      ZETA_BASH_ASSERTIONS_LOG_PREFIX=''
    popd > /dev/null || report_failure "could not cd from ${dir}"
  done
  invoke_callback "${after_all_callback}"
  aecho ">> finished!"

  aecho ""
  aecho "--------------------------------------------------------------------------------"
  aecho "                               RESULT                                           "
  aecho "--------------------------------------------------------------------------------"
  aecho ""
  for test_case in "${!tests_to_run[@]}"; do
    local status="${tests_to_run[${test_case}]}"
    case "$status" in
      not_ran) status="${ZETA_BASH_COLOR_YELLOW}DISABLED${ZETA_BASH_COLOR_NONE}" ;;
      failed)     status="${ZETA_BASH_COLOR_RED}FAILED${ZETA_BASH_COLOR_NONE}  " ;;
      success)  status="${ZETA_BASH_COLOR_GREEN}SUCCESS${ZETA_BASH_COLOR_NONE} " ;;
    esac
    aecho " - [${status}] ${ZETA_BASH_COLOR_PURPLE}${test_case}${ZETA_BASH_COLOR_NONE}"
  done

  # for vscode: rewrite file to use Windows path
  local slf="${SUITE_LOG_FILE}"
  local sof="${SUITE_OUT_FILE}"
  local sef="${SUITE_ERR_FILE}"
  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    slf="$(cygpath -w "$slf")"
    sof="$(cygpath -w "$sof")"
    sef="$(cygpath -w "$sef")"
  fi
  slf="${slf//\\/\\\\}"
  sof="${sof//\\/\\\\}"
  sef="${sef//\\/\\\\}"

  aecho ""
  aecho "--------------------------------------------------------------------------------"
  aecho "  logs ..... : ${ZETA_BASH_COLOR_HL}less -R ${slf}${ZETA_BASH_COLOR_NONE}"
  aecho "  stdout ... : ${ZETA_BASH_COLOR_HL}less -R ${sof}${ZETA_BASH_COLOR_NONE}"
  aecho "  stderr ... : ${ZETA_BASH_COLOR_HL}less -R ${sef}${ZETA_BASH_COLOR_NONE}"
  aecho ""
}
