# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# common alias
alias ..='cd ..'
alias ...='cd ../..'
alias ll='ls -lAh --color --time-style=long-iso --group-directories-first'
alias fll='find -type f -print0|xargs -r0 ls -lAh --color --time-style=long-iso -tr'

# find or clean empty directories
alias empty="find . '(' -type d -and -empty -and -not '(' -path '*/.git/*' ')' ')'"
alias clean-empty-directories="find -not '(' -path '*/.git/*' -or -path '*/.svn/*' ')' -type d -empty -print0|xargs -r0 rmdir --ignore-fail-on-non-empty -p -v"

# alias for common function

alias ex='"${ZETA_BASH_HOME}/bin/explorer" select'
alias view='"${ZETA_BASH_HOME}/bin/explorer" view'

alias env-reload=_zb_env_reload
alias env-load-aliases=_zb_env_load_aliases
alias env-load-completions=_zb_env_load_completions

alias ps1-reload='_zb_ps1 "${ZETA_BASH_PROMPT_TOKENS}"'

alias path-init=_zb_path_init
alias path-print=_zb_path_print

alias cntlm-start='"${ZETA_BASH_HOME}/share/cntlm" start'
alias cntlm-stop='"${ZETA_BASH_HOME}/share/cntlm" stop'

alias switch-maven=_zb_mvn_switch
alias switch-java=_zb_java_switch
alias switch-nodejs=_zb_node_switch
alias switch-node=_zb_node_switch
