# sub("-(?<extension>zip|7z|exe|msi|tar([.-](xz|gz))?)$" ; ".\(.extension | tar)"
def fix_filename:
  if test("-(zip|msi|7z|exe|pkg)$") then
    . | sub("-(?<ext>zip|msi|7z|exe|pkg)$" ;".\(.ext)")
  elif endswith("-tar") then
    . + ".gz"
  else
    . + ".tar.gz"
  end
;
def match_arch($actual; $expected):
  $expected == "" or ($actual | ascii_downcase | contains("-\($expected)" | ascii_downcase ))
;
def match_os($actual; $expected):
  $expected == "" or ($actual | ascii_downcase | startswith($expected| ascii_downcase ))
;
def match_ext($actual; $os):
  if $os == "win" then
    endswith("-zip")
  else
    true
  end
;
def semver:
  . [1:]  # remove vXXX
   | split("-")
   | { qualifier: (.[1:] | join("-")), ver: (.[0] | split(".")) }
   | {   major: (.ver[0] | tonumber)
       , minor: (.ver[1] | tonumber)
       , patch: (.ver[2] | tonumber)
       , qualifier
     }
;
[
  .[] | . as $release
      | {   version
          , date
          # a string indicate if it a LTS (that's its name)
          , lts: ( (.lts | type) == "string" )
          , dir: $release.version
          , filename: (
            [ .files[]
              | select( true
                        and . != "src"
                        and match_os(.; $os)
                        and match_arch(.; $arch)
                        and match_ext(.; $os)
                      )
              | fix_filename | "node-\($release.version)-\(.)"
            ] | first
          )
          , semver: .version | semver
      }
      | (. + {
          # let assume that older version are
          lts: (.lts and .semver.major >= 18)
        })
      | select( (.filename | length > 0) )
] | group_by( .semver.major ) # we don't care about minor.
  | map(max_by( [.semver.major, .semver.minor, .semver.patch]))
  | sort_by( [.semver.major, .semver.minor, .semver.patch] )
  | reverse
  | [ "#!/bin/bash",
      "njs_versions_latest=\(first | .semver.major | @sh)",
      # lts are not flagged by boolean, but with a name.
      "njs_versions_lts=(" + (map(select(.lts) | .semver.major | @sh) | join(" ") ) + ")",
      "njs_versions_indexes=(" + (map(.semver.major | @sh) | join(" ")) +")",
      "njs_versions_names=(",
      (map("  [" + (.semver.major | @sh) + "]=" + (.version | @sh))),
      ")",
      "njs_versions_directories=(",
      (map("  [" + (.semver.major | @sh) + "]=" + (.dir | @sh))),
      ")",
      "njs_versions_filenames=(",
      (map("  [" + (.semver.major | @sh) + "]=" + (.filename | @sh))),
      ")"
    ] | flatten | join("\n")

