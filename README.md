# Zeta Bash

## What is Zeta Bash ?

Zeta Bash is a set of bash4 script that I wrote to enhance my bash enviromnent with some feature that I commonly use, like _going to some repository_, _setting the PATH without tempering the Windows PATH_, or even some common script I might need (like removing all white space at end of file, ...).

### Features

- [Environment Switching made easy][INT-ENV]
- [Default aliases and completions][INT-ALIAS]: default alias and completions to work with Git and Bash.
  - `env-reload <env>` select an env
  - `switch-java <version>` switch to some java version
  - `switch-java <path>` switch to some `JAVA_HOME`
  - `switch-maven <version>`: switch to some Maven version
- [Git Extension][INT-GITEX]: the Git Extension of Zeta Bash allows switching from one repository to another by one magic alias: `g`. And it comes with completion: `g zeta-[TAB]` will find `zeta-bash`!.
- [Prompt builder][INT-PROMPT]: build the PS1 prompt variable, to create a nice prompt!
- [Default binaries][INT-ALIAS]: this contains some wrapper to invoke executable in a portable way (like [Sonatype Nexus OSS](http://www.sonatype.org/nexus/go/)).
- Java based script:
  - [Java][INT-ALIAS]: download AdoptOpenJDK releases right out of the box, switch from Java version to another...
  - [Maven][INT-MAVEN]: switch Maven versions easily.

### Tested on

- Windows 10x64 with [git-for-windows](http://git-for-windows.github.io/)
- Gentoo Linux

## Installation

The recommanded process to install is to clone the repository and call the `install.bash` script.

1. Cloning the repository

    To install Zeta Bash, nothing complicated, simply clone the Git repository:

        git clone https://bitbucket.org/glhez/zeta-bash.git

    There are several branch, which you might know from your day to day use of `git`:

    - `master`: current version of Zeta Bash.
    - `develop`: next version of Zeta Bash.

    There are no real differences between the two, except `develop` is generally more up to date.

2. Installing...

   Simply run `./install.bash`. It will also modify you `.bashrc` to add the actual path to
   Zeta Bash. This path is stored in a variable named `ZETA_BASH_HOME`.

   If you want to set you Zeta Bash userhome to some other place, you can do it like that:

       export ZETA_BASH_USER_HOME=/e/git/zb/zeta-bash-user
       /e/git/zb/zeta-bash/install.bash

   Or:

       /e/git/zb/zeta-bash/install.bash --user-home=/e/git/zb/zeta-bash-user

   The script will do the following:

   1. Check required programs (usually, those coming with Git for Windows).
   2. Check `ZETA_BASH_HOME` and `ZETA_BASH_USER_HOME`.
   3. Initialize `ZETA_BASH_USER_HOME`
   4. Download maven completion (optional) from [maven-bash-completion][EXT-MVNBASH].
   5. Source the `init.bash` in your `~/.bashrc`.

## Configuration

You might want to read these:

- [Environment Switching made easy][INT-ENV]: this contains additional information on (alternate) environment loading.
- [Git Extension][INT-GITEX]: to set up your Git Extension.
- [Prompt builder][INT-PROMPT]: to set up your prompt with fancy colors/stuff.
- [Java][INT-JAVA], [Maven][INT-MAVEN]: configuring Java and Maven.

## Layout

The project layout is the following (from `ZETA_BASH_HOME`):

`/aliases/*.aliases.bash`, `/completions/*.completion.bash`

   Contains all the aliases and completions loaded by Zeta Bash.
   See this doc: [Default aliases and completions][INT-ALIAS]

`/lib/*.bash`

  Our library. Implementor can (re)load them using `_zb_lib`.


## Other stuff

All Zeta Bash related variables are prefixed by `ZETA_BASH_`.
Functions are prefixed by `_zb_`.

Among these, there are 3 variables that may be important:

- `ZETA_BASH_USER_CONFIG` point to the user config directory, containing the temporary and cached config file.
- `ZETA_BASH_WRAP` : if set to _off_, disable the line wrapper. This is used in Git Extension when printing the current status
  of some long running function, like `_zb_gitex_list`.
- `ZETA_BASH_DEBUG` : if set to `1`, enable debug mode. This will print some info, prefixed by `debug|` on stderr.

`ZETA_BASH_USER_CONFIG` points to `${ZETA_BASH_USER_HOME}/.config` by default. You are assured that deleting
content in that directory (or the directory itself) will not break Zeta Bash **but** you might have to issue
some commands to restore initial config (like `g --refresh`).

[EXT-MVNBASH]: https://github.com/juven/maven-bash-completion
[INT-ENV]: docs/env.md
[INT-GITEX]: docs/gitex.md
[INT-ALIAS]: docs/default_binaries_aliases_and_completions.md
[INT-PROMPT]: docs/prompt.md
[INT-MAVEN]: docs/maven.md
[INT-JAVA]: docs/java.md
