# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

#
# Dump the JDK version
#
# @param storage where to store the value (a local variable from calling process)
# @return 0 if executable found; 1 otherwise.
_zb_java_version() { # {{{
  local -gn storage="$1"
  local actual_executable=''

  # not found
  if ! actual_executable=$(command -v java 2> /dev/null) || [[ -z "${actual_executable}" ]]; then
    storage=''
    return 1
  fi

  #
  # two cases:
  #  - symbolic links
  #  - Windows Java installation, which can use such links.
  # we resort to dynamic invocation.
  #

  local refresh_indicator=''
  if [[ -L "${actual_executable}" || "${actual_executable}" == */Oracle/Java/javapath/java ]]; then
    refresh_indicator='!'
  elif _zb_prompt_updated ZETA_BASH_PS_JAVA "${actual_executable}" ; then
    refresh_indicator='#'
  fi

  local version=''
  if [[ -z "$refresh_indicator" ]]; then
    version="${ZETA_BASH_PS_JAVA_VER:-}"
  else
    version=$("${actual_executable}" -version 2>&1 | \grep -P '^[ \t]*[^ ]+[ \t]+version' | head -n 1 | sed -r -e 's/^[ \t]*[^ ]+[ \t]+version "(.+)".*$/\1/g')
    if [[ "${refresh_indicator}" == '#' ]]; then
      if _zb_prompt_save -u ZETA_BASH_PS_JAVA "${actual_executable}" "${version}" ; then
        _zb_env_session_save
      fi
    fi
    version="${refresh_indicator} ${version}"
  fi

  # shellcheck disable=SC2034  # this is a reference
  storage="${version}"
  return 0
} # }}} [_zb_java_version]

_zb_java_validate() {
  local java_home="$1"
  local java_exec=java
  [[ "$ZETA_BASH_OS" == windows ]] && java_exec+=.exe
  [[ -x "$java_home/bin/$java_exec" ]]
}

#
# produce a set of path containing JDK
# each entries may end with a filename itself referenceing @VER for version
#
_zb_java_path_entries() {
  _zb_bt_analyze_entry environment 'JAVA@_HOME' 'JAVA@_ORACLE_HOME' 'JAVA@_OPENJ9_HOME'
  if [[ -n "${ZETA_BASH_JAVA_CONTAINER_PATH}" && -d "${ZETA_BASH_JAVA_CONTAINER_PATH}" ]]; then
    _zb_bt_analyze_entry directory "${ZETA_BASH_JAVA_CONTAINER_PATH}/latest/openjdk-@VER-hotspot"
    # we don't really care about minor/patch
    # echo "d:${ZETA_BASH_JAVA_CONTAINER_PATH}/openjdk-@VER-hotspot"
  fi
  # linux (debian)
  if [[ -d '/usr/lib/jvm' ]] ; then
    _zb_bt_analyze_entry directory '/usr/lib/jvm/temurin-@VER-jdk@ARCH'
    _zb_bt_analyze_entry directory '/usr/lib/jvm/zulu@VER-ca@ARCH'
  fi
  # windows (winget)
  [[ -d '/c/Program Files/Eclipse Adoptium' ]] && _zb_bt_analyze_entry directory '/c/Program Files/Eclipse Adoptium/jdk-@VER-hotspot'
}

#
# Switch to JDK found in provided JAVA_HOME
#
# @param java_home where the JDK is installed
# @param --quiet silent non error message
#
_zb_java_switch() { # {{{
  local quiet=no
  local single_arg=''
  for arg; do
    if [[ "${arg}" == --quiet ]]; then
      quiet=yes
    elif [[ -z "$single_arg" ]]; then
      single_arg="${arg}"
    fi
  done

  [[ -z "${single_arg}" && -n "${JAVA_DEFAULT}" ]] && single_arg="${JAVA_DEFAULT}"
  [[ -z "${single_arg}" ]] && return 1

  local java_home=''
  if _zt_utils_parse_semver "$single_arg" ; then
    local version="${single_arg}"
    # use system path / whatever
    [[ "$quiet" == no ]] && iecho "resolving ${ZETA_BASH_COLOR_PURPLE}JAVA_HOME${ZETA_BASH_COLOR_NONE} using containers"
    if ! _zb_bt_resolve_container java_home _zb_java_path_entries _zb_java_validate ; then
      eecho "could not resolve a java home for version ${ZETA_BASH_COLOR_HL}${version}${ZETA_BASH_COLOR_NONE}."
      return 1
    fi
  else
    java_home="${single_arg}"
    # be nice: adjust path
    case "$java_home" in
      *'/bin')          java_home="${java_home%/bin}"      ;;
      *'/bin/java')     java_home="${java_home%/bin/java}" ;;
      *'/bin/java.exe') [[ "$ZETA_BASH_OS" == windows ]] && java_home="${java_home%/bin/java.exe}" ;;
    esac

    if ! _zb_java_validate "$java_home" ; then
      eecho "provided path does not seems to be a valid JAVA_HOME: ${ZETA_BASH_COLOR_HL}${java_home}${ZETA_BASH_COLOR_NONE} (no bin/java)."
      return 1
    fi
  fi

  if ! _zb_env_update_var "JAVA_HOME" "${java_home}" "${quiet}" ; then
    return 0
  fi

  # set up our variables
  local java_bin="${java_home}/bin"
  export ZETA_BASH_JAVA_EXECUTABLE="${java_bin}/java"
  if [[ "${ZETA_BASH_OS}" == windows ]]; then
    export ZETA_BASH_JAVAW_EXECUTABLE="${java_bin}/javaw"
  else
    export ZETA_BASH_JAVAW_EXECUTABLE="${ZETA_BASH_JAVA_EXECUTABLE}"
  fi

  _zb_path_replace_app_home --app-name=java "--quiet=$quiet" "--app-home=$java_home" "--executable=java" --directories=_zb_java_path_entries

  # refresh the prompt, but don't reevaluate the version if empty
  if actual_executable=$(command -v java 2> /dev/null) ; then
    _zb_prompt_save ZETA_BASH_PS_JAVA "${actual_executable}" '' || :
  fi
  _zb_env_session_save
  # reset prompt var
  return 0
} # }}} [_zb_java_switch]
