# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

_zb_env_completion() { # {{{
  _zb_complete_filelist "${ZETA_BASH_USER_HOME}/env/${ZETA_BASH_OS}." ".env"
} # }}} [_zb_env_completion]

_zb_lib_completion() { # {{{
  _zb_complete_filelist "${ZETA_BASH_HOME}/lib/" ".bash"
} # }}} [_zb_lib_completion]

if ! _zb_utils_function_exists _get_comp_words_by_ref ; then
  wecho "missing function ${ZETA_BASH_COLOR_HL}_get_comp_words_by_ref${ZETA_BASH_COLOR_NONE}, gitex completion will not be enabled"
else
  complete -o bashdefault -o nospace -F _zb_env_completion env-reload
  complete -o bashdefault -o nospace -F _zb_env_completion _zb_env_reload
  complete -o bashdefault -o nospace -F _zb_lib_completion _zb_lib
fi
complete -o nospace -W '--clean --remove-duplicates' path-init
