# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

declare ls_options='
  -a  --all -A  --almost-all --author -b  --escape --block-size=SIZE -B
  --ignore-backups -c -C --color --color=always --color=never --color=auto
  -d  --directory -D  --dired -f -F  --classify --file-type --format=WORD
  --full-time -g --group-directories-first -G  --no-group -h  --human-readable
  --si -H  --dereference-command-line --dereference-command-line-symlink-to-dir
  --hide= --hyperlink --hyperlink=always --hyperlink=never --hyperlink=auto
  --indicator-style= -i  --inode -I  --ignore= -k  --kibibytes -l -L  --dereference
  -m -n  --numeric-uid-gid -N  --literal -o -p  --indicator-style=slash -q
  --hide-control-chars --show-control-chars -Q  --quote-name --quoting-style=
  -r  --reverse -R  --recursive -s  --size -S --sort= --time= --time-style=full-iso
  --time-style=long-iso --time-style=iso -t -T  --tabsize= -u -U -v -w  --width=
  -x -X -Z  --context -1 --append-exe --help --version
'

declare xargs_options='
  -0  --null -a  --arg-file= -d  --delimiter= -E -e  --eof --eof= -I -i  --replace
  --replace= -L  --max-lines= -l -n  --max-args= -o  --open-tty -P  --max-procs=
  -p  --interactive --process-slot-var= -r  --no-run-if-empty -s  --max-chars=
  --show-limits -t  --verbose -x  --exit --help --version
'

declare du_options='
  -0  --null -a  --all --apparent-size -B  --block-size= -b  --bytes -c  --total -D
  --dereference-args -d  --max-depth= --files0-from= -H -h  --human-readable --inodes
  -k -L --dereference -l  --count-links -m -P  --no-dereference -S  --separate-dirs
  --si -s  --summarize -t  --threshold= --time --time=
  --time-style=full-iso --time-style=long-iso --time-style=iso -X  --exclude-from=
  --exclude= -x  --one-file-system --help --version
'

complete -o filenames -o default -o nospace -W "${ls_options}" ls ll
complete -o filenames -o default -o nospace -W "${xargs_options}" xargs
complete -o filenames -o default -o nospace -W "${du_options}" du





