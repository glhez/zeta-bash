#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

if [[ -z "${ZETA_BASH_HOME:-}" || ! -d "${ZETA_BASH_HOME}" ]]; then
  if ! ZETA_BASH_HOME="$(realpath "$0")" ; then
    echo "fatal error: could not determine a ZETA_BASH_HOME."
    exit 1
  fi
  ZETA_BASH_HOME="${ZETA_BASH_HOME%/bin/*}"
fi
if [[ ! -f "${ZETA_BASH_HOME}/init.bash" ]]; then
  echo "fatal error: [${ZETA_BASH_HOME}] does not seems to be a valid Zeta Bash install (not init.bash)"
  exit 1
fi

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/os.bash
source "${ZETA_BASH_HOME}/lib/os.bash" && _zb_determine_os


#
# Part of Zeta Bash
#

#
# Invoke explorer (windows + wsl/cygpath)
#
# On WSL, explorer seems to "translate" the WSL path by itself. The / must be replaced by \.
#
# In the path contains a space, then it fails: I don't know which is responsible for _not_ passing
# the path correctly?
#
_zb_explorer_windows() {
  if [[ "$#" == 0 || "$#" == 1 && ( "$1" == 'open' || "$1" == 'view'  || "$1" == 'select' ) ]]; then
    explorer.exe .
  else
    local action="$1"
    local -a options=( '/select,' )
    case "$action" in
      open|view) options=()         ; shift ;;
      select)                         shift ;;
    esac
    # prepare args
    local -a args=()
    if [[ -n "$WSL_DISTRO_NAME" ]]; then
      for arg; do
        args+=( "${arg//\//\\}" )
      done
    elif ! mapfile -t args < <(_zb_win_path2 "$@" ) ; then
      eecho "could not translate path to windows."
      return 1
    fi
    for arg in "${args[@]}"; do
      MSYS_NO_PATHCONV=1 explorer.exe "${options[@]}" "${arg}"
    done
  fi
}

#
# Provide a way to launch an "explorer"
#
# any arg will be passed to the explorer command.
# if no arg is passed, "." (current directory) is passed.
# on linux, user can set ZB_BASH_EXPLORER to override any explorer.
#
main() {
  # invoke the necessary tool
  case "${ZETA_BASH_OS}" in
    windows) _zb_explorer_windows "$@" ;;
    *)
      if [[ -n "$WSL_DISTRO_NAME" ]] ; then
        _zb_explorer_windows "$@"
      else
        eecho "unhandled explorer ${ZETA_BASH_COLOR_HL}:(${ZETA_BASH_COLOR_NONE}"
      fi
    ;;
  esac
}

main "$@"
