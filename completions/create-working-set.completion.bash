# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

_zb_create_working_set_completion() { # {{{
  #local cmd="$1"
  local cur=''
  #local prev=''
  local -a words=()
  #local cword=''

  _get_comp_words_by_ref -n '=' cur prev words cword

  local -a completions=()
  completions+=( -A --all --aggregate -c --per-commit )

  # consider existing repo
  local -A already_completed_words=()
  # shellcheck disable=SC2154
  for (( i = 1, n = ${#words[@]}; i < n; ++i )); do
    local word="${words[$i]}"
    if [[ -n "$word" ]]; then already_completed_words[$word]=yes ; fi
  done

  # rewrite completions
  local -a possible_completions=()
  for completion in "${completions[@]}" ; do
    if [[ -z "${already_completed_words[$completion]}" ]]; then
      possible_completions+=( "$completion" )
    fi
  done

  __git_complete_refs
  _zb_complete_match_prefix "$cur" "${possible_completions[@]}"

} # }}} [_zb_gitex_completion_gitex_main]

# don't add filenames: this make names with '/' fails
# https://stackoverflow.com/questions/55476845/escape-slashes-in-bash-complete
complete -o bashdefault -o nospace -F _zb_create_working_set_completion create-working-set


