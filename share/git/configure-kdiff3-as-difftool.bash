#!/bin/bash
# :mode=shellscript:folding=explicit:

#
# Part of Zeta Bash
#

# shellcheck source=lib/utils.bash
source "${ZETA_BASH_HOME}/lib/utils.bash"
# shellcheck source=lib/pathutils.bash
source "${ZETA_BASH_HOME}/lib/pathutils.bash"
# shellcheck source=lib/os.bash
source "${ZETA_BASH_HOME}/lib/os.bash" && _zb_determine_os

#
# This script will set kdiff3 as the default diff engine on Windows.
#
declare kdiff3_path=''

if [[ "${ZETA_BASH_OS}" == windows || -n "${WSL_DISTRO_NAME}" ]]; then
  declare -a windows_default_lookups=()
  _zb_wsl_prefix windows_default_lookups "/c/Program Files/KDiff3/kdiff3.exe" "/c/Program Files (x86)/KDiff3/kdiff3.exe"
  for lookup in "${windows_default_lookups[@]}"; do
    if [[ -f "${lookup}" ]]; then
      kdiff3_path="${lookup}"
      break
    fi
  done
else
  eecho "unhandled os ${ZETA_BASH_COLOR_HL}${ZETA_BASH_OS}${ZETA_BASH_COLOR_NONE}"
  exit 2
fi

if [[ -z "$kdiff3_path" ]]; then
  eecho "could not find kdiff3 in usual locations"
  exit 2
fi

if [[ "${ZETA_BASH_OS}" == windows ]]; then
  declare translated=''
  if ! translated=$(cygpath -w "${kdiff3_path}") ; then
    eecho "could no translate path with cygpath."
    exit 2
  fi
  kdiff3_path="${translated}"
fi

echo "configuring git to use kdiff3 (exec: ${kdiff3_path})"
git config --global merge.tool kdiff3
git config --global mergetool.kdiff3.path "${kdiff3_path}"
git config --global diff.tool kdiff3
git config --global difftool.kdiff3.path "${kdiff3_path}"
